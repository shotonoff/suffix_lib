<?php

/**
 *
 * @author shotonoff
 */
interface IOperation {

    /**
     * @var mixed 
     */
    public function BuildReasponse($data);

    /**
     * @return IGateResponse 
     */
    public function Execute();
}