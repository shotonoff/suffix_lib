<?php

/**
 * Description of GateResponseBase
 *
 * @author shotonoff
 */

abstract class GateResponseBase {

    /**
     * @var mixed
     */
    protected $responseData;

    /**
     * @var ICrypt
     */
    protected $crypt;
    
    protected $operation;

    public function __construct(ICrypt $crypt = null) {
        (null == $crypt) || $this->crypt = $crypt;
    }

    public function SetData($data) {
        $this->responseData = $data;
    }

    /**
     * @return string
     * @throws Exception 
     */
    protected function prepareSend() {
        $resp = null;
        
        if (is_array($this->responseData) && count($this->responseData) > 0) {
            $resp = implode("", $this->responseData);
        } elseif (is_string($this->responseData)) {
            $resp = $this->responseData;
        } else {
            return "";
        }
        return $resp;
    }

}