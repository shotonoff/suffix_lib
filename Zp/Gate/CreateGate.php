<?php

/**
 * Description of CreateGate
 *
 * @author shotonoff
 */
require_once(DocRoot . '/application/main/models/HttpsProtocol/ZpHttpsProtocol.php');

require_once(DocRoot . '/application/main/models/gate/GateBase.php');
require_once(DocRoot . '/application/main/models/gate/OperationBase.php');

require_once(DocRoot . '/application/main/models/gate/GateProtocolOperation.php');

require_once(DocRoot . '/application/main/models/gate/IGateRequest.php');
require_once(DocRoot . '/application/main/models/gate/GateRequestBase.php');

require_once(DocRoot . '/application/main/models/gate/IGateResponse.php');
require_once(DocRoot . '/application/main/models/gate/GateResponseBase.php');

class GateNotFoundException extends Exception {
    const GateNotFound = 0;
    const TypeNotFound = 1;
}


interface IGateFactory {

    static public function Factory($nameGate);
}

class CreateGate implements IGateFactory {

    /**
     *
     * @return IGateHttpsProtocolAdapter
     */
    static public function Factory($nameGate) {
        

        switch ($nameGate) {
            case GateEnum::TelePay:


                require_once(DocRoot . '/application/main/models/gate/telepay/BalanceOnOperation.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/StatusOnOperation.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/PaynmentOnOperation.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/InvoiceOnOperation.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/TransferOnOperation.php');

                require_once(DocRoot . '/application/main/models/gate/telepay/GateTelepayRequest.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/GateTelepayResponse.php');
                require_once(DocRoot . '/application/main/models/gate/telepay/TelepayGate.php');


                $zpAccount = "ZP09346689";
                $idInterface = 107;
                $password = "dbae2af37c4a73a9914eeaa51eee26a";
                
                $protocol = new GateProtocolOperation($zpAccount, $idInterface, $password);

                /**
                 * Приватный ключ: /cert/telepay/tp_priv.key
                 * @var string
                 */
                $pathToPrivateKey = DocRoot . "/cert/telepay/tp_priv.key";

                /**
                 * Публичный ключ: /cert/telepay/tp_pub.pem
                 * @var string
                 */
                $pathToPublicKey = DocRoot . "/cert/telepay/tp_pub.pem";

                /**
                 * Пароль приватного ключа: d0e2d32dc8
                 * @var string
                 */
                $passPhrase = "d0e2d32dc8";

                $openssl = new Openssl();
                $openssl->SetPrivateKey($pathToPrivateKey, $passPhrase);
                $openssl->SetPublicKey($pathToPublicKey);

                $gateTelepayRequest = new GateTelepayRequest($openssl);
                $gateTelepayResponse = new GateTelepayResponse($openssl);

                $protocol->AddOperation(EnumGateHttpsProtocol::Balance, new BalanceOnOperation($gateTelepayRequest, $gateTelepayResponse));
                $protocol->AddOperation(EnumGateHttpsProtocol::Status, new StatusOnOperation($gateTelepayRequest, $gateTelepayResponse));
                $protocol->AddOperation(EnumGateHttpsProtocol::TransferMoney, new TransferOnOperation($gateTelepayRequest, $gateTelepayResponse));
                $protocol->AddOperation(EnumGateHttpsProtocol::Invoice, new InvoiceOnOperation($gateTelepayRequest, $gateTelepayResponse));

                $gate = new TelepayGate($protocol, $gateTelepayRequest, $gateTelepayResponse);

                break;
            default :
                throw new GateNotFoundException("Шлюз не найден", GateNotFoundException::GateNotFound);
        }
        return $gate;
    }

}
