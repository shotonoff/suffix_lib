<?php

/**
 *
 * @author shotonoff
 */
interface IGateResponse {

    /**
     * @desc отсылаем полученный результат 
     */
    public function send();
}