<?php

/**
 * Description of GateBase
 *
 * @author shotonoff
 */

class EnumGateHttpsProtocol {
    const Balance = 0;
    const TransferMoney = 1;
    const History = 2;
    const SendMessage = 3;
    const GetInformation = 4;
    const Invoice = 5;
    const Status = 6;
}

abstract class GateBase {

    protected $gateOperation;

    /**
     * @var IGateRequest
     */
    protected $request;

    /**
     * @var IGateResponse 
     */
    protected $response;

    /**
     * @return type 
     */
    public function GetRequest() {
        return $this->request;
    }

    /**
     * @return type 
     */
    public function GetResponse() {
        return $this->response;
    }

    public function __construct(GateProtocolOperation $gateOperation, IGateRequest $request, IGateResponse $response) {
        $this->gateOperation = $gateOperation;
        $this->response = $response;
        $this->request = $request;
    }

    abstract public function ChooseOperation();

    abstract public function Execute();
}