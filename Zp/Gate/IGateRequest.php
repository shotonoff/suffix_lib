<?php

/**
 *
 * @author shotonoff
 */
interface IGateRequest {

    /**
     * @desc В методе должна быть описана логика
     * получения данных
     */
    public function Initialize();
}