<?php

require_once 'IOperation.php';

/**
 * Базовый класс
 */
class OperationBase {

    public function __get($name) {
        $method_name = "Get" . ucfirst(strtolower($name));
        if (method_exists($this, $method_name)) {
            return call_user_func(array($this, $method_name));
        }
    }

    public function __set($name, $value) {
        $method_name = "Set" . ucfirst(strtolower($name));
        if (method_exists($this, $method_name)) {
            call_user_func(array($this, $method_name), $value);
        }
    }

    /**
     * @var IHttpsProtocol 
     */
    protected $protocol;

    /**
     * @var IGateResponse
     */
    protected $gateResponse = null;

    /**
     *
     * @var IGateRequest
     */
    protected $gateRequest = null;

    public function __construct(IGateRequest $gateRequest, IGateResponse $gateResponse) {
        $this->SetRequest($gateRequest);
        $this->SetResponse($gateResponse);
    }

    public function SetProtocolHandle(IHttpsProtocol $protocol) {
        $this->protocol = $protocol;
    }

    /**
     * @param IGateResponse $response 
     */
    public function SetResponse(IGateResponse $response) {
        if (null !== $response) {
            $this->gateResponse = $response;
        } else {
            throw new OperationException("У команды нет объекта response");
        }
    }

    /**
     * @return IGateResponse
     */
    public function GetResponse() {
        return $this->gateResponse;
    }

    /**
     * @param IGateRequest $request 
     */
    public function SetRequest(IGateRequest $request) {
        if (null != $request) {
            $this->gateRequest = $request;
        } else {
            throw new OperationException("У команды нет объекта request");
        }
    }

    /**
     * @return IGateRequest 
     */
    public function GetRequest() {
        return $this->gateRequest;
    }

}