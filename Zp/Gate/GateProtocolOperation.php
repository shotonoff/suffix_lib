<?php

/**
 * GateProtocolOperation - класс коллекция 
 *
 * @author Dmitry Golubev
 */

class GateProtocolOperation {

    /**
     * @var HttpsProtocol 
     */
    protected $protocol;

    /**
     * @var array
     */
    protected $operations = array();

    public function __construct($zpAccount, $idInterface, $password) {
        $this->SetProtocol(new ZpHttpsProtocol($zpAccount, $idInterface, $password));
    }

    public function SetProtocol(IHttpsProtocol $protocol) {
        $this->protocol = $protocol;
    }

    public function GetProtocol() {
        return $this->protocol;
    }

    public function AddOperation($codeOperation, IOperation $operation) {
        $operation->SetProtocolHandle($this->protocol);
//        $gateTelepayRequest, $gateTelepayResponse 
        /** Проверяем наличие уже добавленной операции */
        $this->operations[$codeOperation] = $operation;
    }

    public function GetOperations($codeOperation) {
        if (isset($this->operations[$codeOperation]))
            return $this->operations[$codeOperation];

        return false;
    }

}