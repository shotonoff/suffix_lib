<?php

/**
 * Description of GateRequestBase
 *
 * @author shotonoff
 */
abstract class GateRequestBase {

    /**
     * @var array
     */
    private $data;

    public function __construct() {
        $this->Initialize();
    }

    public function Initialize() {
        
    }

    public function GetData() {
        return $this->data;
    }

    public function SetData($data) {
        $this->data = $data;
    }

}