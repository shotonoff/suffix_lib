<?php

/**
 * Description of ISession
 *
 * @author shotonoff
 */

namespace Zp;

interface ISession {
    
    public function Set($key, $value);
    
    public function Get($key);
    
    public function IsVar($key);
    
}
