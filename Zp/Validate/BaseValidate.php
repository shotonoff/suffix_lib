<?php

/**
 * Description of BaseValidate
 *
 * @author shotonoff
 */

namespace Zp\Validate;

class BaseValidate {

    /**
     * @var \Zp\Validate\BaseValidator
     */
    protected $validator;

    /**
     * @var array
     */
    protected $validData;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string 
     */
    protected $fieldName;

    public function SetValidator(Zp\Validate\BaseValidator $validator) {
        $this->validator = $validator;
    }

    public function SetFieldName($fieldName) {
        $this->fieldName = $fieldName;
    }

    public function GetFieldName() {
        return $this->fieldName;
    }

    public function GetValidator() {
        return $this->validator;
    }

    public function SetValidData($validData) {
        $this->validData = $validData;
    }

    public function GetValidData() {
        return $this->validData;
    }

    public function GetMessage() {
        return $this->message;
    }

    public function SetMessage($message) {
        $this->message = $message;
    }

}