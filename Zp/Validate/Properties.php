<?php

/**
 * Description of Properties
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\Validate\IValidatorProprties,
    Zp\IValidate;

class Properties implements IValidatorProprties {

    protected $validators = array();
    protected $required = false;

    public function IsRequired() {
        return $this->required;
    }
    /**
     * @param $flag bool
     */
    public function SetRequired($flag) {
        $this->required = $flag;
    }
    
    public function AddValidate(IValidate $validate) {
        $this->validators[] = $validate;
    }

    public function GetValidators() {
        return $this->validators;
    }

}