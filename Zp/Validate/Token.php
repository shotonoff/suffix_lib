<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class Token extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Ошибка проверки подписи формы";

    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {       
        $session = new \Zp\Session();
        $tokens = $session->Get("token");
        
        if(!isset($tokens[$this->GetFieldName()])) {
            $this->SetMessage(self::ERROR_MESSAGE);
            return false;
        }

        if ($tokens[$this->GetFieldName()] == $value){
            unset($tokens[$this->GetFieldName()]);
            $session->Set('token', $tokens);
            return true;
        }

        $this->SetMessage(self::ERROR_MESSAGE);
        return false;
    }

}