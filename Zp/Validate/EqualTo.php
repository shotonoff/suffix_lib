<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class EqualTo extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Значение поля %name% не равно %value%";
    
    private $eqValue;
    
    
    public function __construct($eqValue) {
        $this->eqValue = $eqValue;
    }

    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        if ($this->eqValue != $value) {
            $this->setMessage(str_replace(array("%name%", "%value%"), array($this->fieldName, $value), self::ERROR_MESSAGE));          
            return false;
        }
        return true;
    }

}