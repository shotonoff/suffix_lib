<?php

/**
 * Description of Exists
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class Required extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Поле %name% обязаельно для заполнеиня";

    public function IsValid($value) {
        if (!empty($value))
            return true;
        $this->SetMessage(str_replace("%name%", "", self::ERROR_MESSAGE));
        return false;
    }

}
