<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class Email extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Электронная почта \"%phrase%\" указана не верно";
    
    /**
     *
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value) {
        $pattern = "/^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/";
        $pattern = "//";

        if(preg_match($pattern, $value))
            return true;
        
        $this->SetMessage(str_replace(array("%phrase%"), array($value), self::ERROR_MESSAGE));
        return false;
    }

}