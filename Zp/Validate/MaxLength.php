<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class MaxLength extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Превышено максимальная длинна %maxLength%";
    
    private $length;
    
    public function __construct($options) {
        if(!isset($options['length'])) 
            throw new \InvalidArgumentException();
        $this->length = $options['length'];
    }

    /**
     *
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value) {
        if(strlen($value) <= $this->length)
            return true;
        $this->SetMessage(str_replace("%maxLength%", $this->length, self::ERROR_MESSAGE));
        return false;
    }

}