<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class ZPAccount extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Кошелёк указан не верно";

    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        if ("ZP" == substr($value, 0, 2) && strlen($value) == 10 && is_numeric(substr($value, -8)))
            return true;
        $this->SetMessage(self::ERROR_MESSAGE);
        return false;
    }

}