<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
Zp\Validate\BaseValidate;

class Lambda extends BaseValidate implements IValidate
{

    private $func;

    private $errorMessage;

    public function __construct($condition, $errorMessage = null)
    {
        $this->func = $condition;
        if (null !== $errorMessage) {
            $this->errorMessage = $errorMessage;
        }
    }

    /**
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value)
    {
        $func = $this->func;
        $flag = $func($value, $this);
        if (!$flag)
            $this->SetMessage($this->errorMessage);
        return $flag;
    }

}