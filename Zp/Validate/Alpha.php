<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class Alpha extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Строка \"%phrase%\" содержит не только буквенные символы";

    /**
     *
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value) {
//        if(ctype_alpha($value))
        if (preg_match("/[a-zA-Zа-яА-Я]/", $value))
            return true;

        $this->SetMessage(str_replace(array("%phrase%"), array($value), self::ERROR_MESSAGE));
        return false;
    }

}