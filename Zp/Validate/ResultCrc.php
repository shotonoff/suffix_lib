<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class ResultCrc extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Не верная подпись";
    
    private $SECRET_KEY;
    
    public function __construct($SECRET_KEY) {
        $this->SECRET_KEY = $SECRET_KEY;
    }


    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($crc) {
        $LMI_SECRET_KEY = (isset($_POST['LMI_SECRET_KEY']))?$_POST['LMI_SECRET_KEY']:$this->SECRET_KEY;
        $concat = $_POST['LMI_PAYEE_PURSE'] .
                $_POST['LMI_PAYMENT_AMOUNT'] .
                $_POST['LMI_PAYMENT_NO'] .
                $_POST['LMI_MODE'] .
                $_POST['LMI_SYS_INVS_NO'] .
                $_POST['LMI_SYS_TRANS_NO'] .
                $_POST['LMI_SYS_TRANS_DATE'] .
                $LMI_SECRET_KEY .
                $_POST['LMI_PAYER_PURSE'] .
                $_POST['LMI_PAYER_WM'];
        
        if($crc == strtoupper(md5($concat)))
            return true;
        
        $this->SetMessage(self::ERROR_MESSAGE);
        return false;
    }

}