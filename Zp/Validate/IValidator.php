<?php

/**
 * Description of IValidator
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\Validate\IValidatorProprties;

interface IValidator {
    
    public function Init();
    
    public function AppendPropertyValidate($name, IValidatorProprties $validatorProperties);
    
    public function GetPropertyValidate($name);
    
    public function AddErrorMessage($name, $message);
    
    public function RemoveErrorMessage($name, $index = null);
    
    public function GetErrorMessagesForField($name);
    
    public function GetErrorMessages();
    
    public function IsValid(array $request);
    
}