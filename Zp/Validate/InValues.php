<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class InValues extends BaseValidate implements IValidate {
    
    const ERROR_MESSAGE = "Значение %value% не входит в допустимо-возможные %array_values%";
    
    protected $values;
    
    public function __construct(array $values) {
        $this->values = $values;
    }
    
    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        if(!in_array($value, $this->values)) {
            $this->setMessage(str_replace(array("%value%", "%array_values%"), array($value, implode(",", $this->values)), self::ERROR_MESSAGE));
            return false;
        }        
        return true;
    }

}