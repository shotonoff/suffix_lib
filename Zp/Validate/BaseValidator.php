<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\Validate\IValidator,
Zp\Validate\IValidatorProprties;

abstract class BaseValidator implements IValidator
{

    protected $validators = array();
    protected $errorMessage = array();

    public function __construct()
    {
        $this->Init();
    }

    public function Init()
    {

    }

//    public function IsValid(\Zp\Http\IRequest $request) {
    public function IsValid(array $data, $breakChain = false)
    {
        $return = true;

        foreach ($this->validators as $fieldName => $validator) {
            /** @var $validator Zp\Validate\IValidatorProprties $propValid */
            if ($validator->IsRequired() && (!isset($data[$fieldName]) || is_null($data[$fieldName]) || empty($data[$fieldName]))) {
                $return = false;
                $this->AddErrorMessage($fieldName, "Поле " . $fieldName . " обезательно для заполенения");
                if ($breakChain)
                    break;
                continue;
            } elseif (!isset($data[$fieldName]) || is_null($data[$fieldName]) || empty($data[$fieldName])) {
                continue;
            }

            $fieldValue = $data[$fieldName];

            /** @var array $validators  */
            $validators = $validator->GetValidators();

            /** @var $validate Zp\IValidate */
            foreach ($validators as $validate) {

                if (!$validate->IsValid($fieldValue)) {
                    $return = false;
                    $this->AddErrorMessage($fieldName, $validate->GetMessage());
                    if ($breakChain)
                        return false;
                }

            }
        }

        return $return;

        foreach ($data as $fieldName => $fieldValue) {


            $propValid = $this->GetPropertyValidate($fieldName);
            if (null === $propValid)
                continue;

            if ($propValid->IsRequired() === true && ((empty($fieldValue) || is_null($fieldValue)))) {
                $this->AddErrorMessage($fieldName, "Поле " . $fieldName . " обезательно для заполенения");
                $return = false;
            } elseif (empty($fieldValue) || is_null($fieldValue)) {
                continue;
            }

            /** @var array $validators  */
            $validators = $propValid->GetValidators();

            /** @var $validate Zp\IValidate */
            foreach ($validators as $validate) {

                if (!$validate->IsValid($fieldValue)) {
                    $return = false;
                    $this->AddErrorMessage($fieldName, $validate->GetMessage());
                    if ($breakChain)
                        return false;
                }

            }
        }
        return $return;
    }

    public function AppendPropertyValidate($name, IValidatorProprties $validatorProperties)
    {
        foreach ($validatorProperties->GetValidators() as $validate) {
            $validate->SetFieldName($name);
        }

        $this->validators[$name] = $validatorProperties;
    }

    public function GetPropertyValidate($name)
    {
        if (isset($this->validators[$name]))
            return $this->validators[$name];
        else
            return null;
    }

    public function AddErrorMessage($name, $message)
    {
        $this->errorMessage[$name][] = $message;
    }

    public function RemoveErrorMessage($name, $index = null)
    {
        if (isset($this->errorMessage[$name]) && is_array($this->errorMessage[$name])) {
            if (null === $index)
                unset($this->errorMessage[$name]);
            elseif (isset($this->errorMessage[$name][$index])) {
                unset($this->errorMessage[$name][$index]);
            }
        }
    }

    public function GetErrorMessagesForField($name)
    {
        if (isset($this->errorMessage[$name]))
            return $this->errorMessage[$name];
        return null;
    }

    public function GetErrorMessages()
    {
        return $this->errorMessage;
    }

}
