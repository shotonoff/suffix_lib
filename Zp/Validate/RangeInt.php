<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class RangeInt extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Введенное значение %value% не попадает в допустимый диапазон от %min% до %max%";
    
    private $min;
    private $max;

    public function __construct($options) {
        if ((!isset($options['min']) || !isset($options['max'])) &&
                (!is_int($options['min']) || !is_numeric($options['min'])) &&
                (!is_int($options['max']) || !is_numeric($options['max'])) &&
                !($options['min'] < $options['max']))
            throw new \InvalidArgumentException();
        $this->min = $options['min'];
        $this->max = $options['max'];
    }

    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        $value = (int) $value;
        
        if ($value >= $this->min && $value <= $this->max) {
            return true;
        }
        $this->SetMessage(str_replace(array("%value%", "%min%", "%max%"), array($value, $this->min, $this->max), self::ERROR_MESSAGE));
        return false;
    }

}