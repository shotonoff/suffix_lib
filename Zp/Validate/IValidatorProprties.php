<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate;

interface IValidatorProprties {

    public function AddValidate(IValidate $validate);

    public function GetValidators();
    
    public function SetRequired($flag);
}