<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class MinLength extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Не достигнута минимально доспустимая длинна %minLength%";
    
    private $length;
    
    public function __construct($options) {
        if(!isset($options['length'])) 
            throw new \InvalidArgumentException();
        $this->length = $options['length'];
    }

    /**
     *
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value) {
        if(strlen($value) >= $this->length)
            return true;
        $this->SetMessage(str_replace("%minLength%", $this->length, self::ERROR_MESSAGE));
        return false;
    }

}