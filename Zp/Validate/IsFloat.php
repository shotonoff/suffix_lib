<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class IsFloat extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Значение %value% должно быть типа float";

    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        if (is_float($value) || is_numeric($value))
            return true;
        $this->SetMessage(str_replace("%value%", $value, self::ERROR_MESSAGE));
        return false;
    }

}