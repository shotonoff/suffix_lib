<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class Alnum extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Строка \"%phrase%\" содержит не только букво-цфровые символы";
    
    /**
     *
     * @param mixed $value
     * @return boolean
     */
    public function IsValid($value) {
//        if(preg_match("/\w+/", $value))
        
//        if(ctype_alnum($value))
        if(preg_match("/[a-zA-Zа-яА-Я0-9]/", $value))
            return true;
        
        $this->SetMessage(str_replace(array("%phrase%"), array($value), self::ERROR_MESSAGE));
        return false;
    }

}