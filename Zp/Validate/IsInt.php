<?php

/**
 * Description of RangeInt
 *
 * @author shotonoff
 */

namespace Zp\Validate;

use Zp\IValidate,
    Zp\Validate\BaseValidate;

class IsInt extends BaseValidate implements IValidate {

    const ERROR_MESSAGE = "Значение %value% должно быть типа integer";
    
    /**
     * @param mixed $value 
     * @return boolean
     */
    public function IsValid($value) {
        if (is_int($value) || is_numeric($value))
            return true;
        $this->SetMessage(str_replace("%value%", $value, self::ERROR_MESSAGE));
        return false;
    }

}