<?php
namespace Zp\Crypt;
/**
 * Description of Openssl
 *
 * @author shotonoff
 */
class Openssl extends CryptBase implements ICrypt {

    const LENGTH_SEPARATOR = 245;

    /**
     *
     * @var Resource
     */
    static private $defaultPrivateKey;

    /**
     *
     * @var type
     */
    static private $defaultPassPrivateKey = "";

    /**
     *
     * @var Resource
     */
    static private $defaultPublicKey;

    /**
     *
     * @var Resource
     */
    private $privateKey = null;

    /**
     *
     * @var string
     */
    private $passPrivateKey = "";

    /**
     *
     * @var Resource
     */
    private $publicKey = null;

    static function SetDefaultKey($pathToPrivateKey, $passphrase, $pathToPublicKey) {
        if (!file_exists($pathToPrivateKey) && !file_exists($pathToPublicKey)) {
            throw new CryptKeyFileNotFoundException("Путь до приватного или публичного ключа указал неверно");
            
        }

        self::$defaultPassPrivateKey = $passphrase;

        $handle = fopen($pathToPrivateKey, "r");
        self::$defaultPrivateKey = openssl_get_privatekey(fread($handle, 8192), $passphrase);
        fclose($handle);

        $handle = fopen($pathToPublicKey, "r");
        self::$defaultPublicKey = openssl_get_publickey(fread($handle, 8192));
        fclose($handle);
    }

    public function __construct() {

    }

    public function GetPrivateKey() {
        return (null === $this->privateKey) ? self::$defaultPrivateKey : $this->privateKey;
    }

    public function GetPublicKey() {
        return (null === $this->publicKey) ? self::$defaultPublicKey : $this->publicKey;
    }

    public function GetPassPrivateKey() {
        return ("" != self::$defaultPassPrivateKey) ? self::$defaultPassPrivateKey : $this->passPrivateKey;
    }

    public function SetPublicKey($publicKey) {
        if (is_string($publicKey) && file_exists($publicKey)) {
            $handle = fopen($publicKey, "r");
            $this->publicKey = openssl_get_publickey(fread($handle, 8192));
            fclose($handle);
        } else {
            $this->publicKey = $publicKey;
        }
    }

    public function SetPrivateKey($privateKey, $passPhrase = null) {
        if (is_string($privateKey) && file_exists($privateKey)) {
            $handle = fopen($privateKey, "r");
            $this->privateKey = ((null != $passPhrase)) ?
                    openssl_get_privatekey(fread($handle, 8192), $passPhrase) :
                    openssl_get_privatekey(fread($handle, 8192), "");
            fclose($handle);
        } else {
            $this->privateKey = $privateKey;
        }
    }

    /**
     *
     * @param string $data
     * @return \CryptData
     */
    public function EnCrypt($data = null) {
        $encryptedviaprivatekey = "";

        $crypttext = "";
        $crypt = array();
        $length = strlen($data);
        $countIteration = ceil($length / self::LENGTH_SEPARATOR);

        for ($i = 0; $i < $countIteration; $i++) {
            $cryptPhrase = substr($data, $i * self::LENGTH_SEPARATOR, self::LENGTH_SEPARATOR);
            openssl_public_encrypt($cryptPhrase, $encryptedviaprivatekey, $this->GetPublicKey());
            $crypt[] = $encryptedviaprivatekey;
        }
        $crypttext = implode(":::", $crypt);
        return new CryptData($crypttext);
    }

    /**
     *
     * @param string $cryptData
     * @return string
     */
    public function DeCrypt($cryptData = null) {
        $decryptedviapublickey = "";

        $crypt = explode(":::", $cryptData);
        $decryptedviapublickey = "";

        foreach ($crypt as $cryptPhrase) {
            openssl_private_decrypt($cryptPhrase, $decryptedviapublickey, $this->GetPrivateKey());
            $decryptText .= $decryptedviapublickey;
        }

        return $decryptText;
    }

    /**
     *
     * @param type $OPENSSL_ALGO
     * @return CryptData
     */
    public function Sign($data, $OPENSSL_ALGO = OPENSSL_ALGO_MD5) {
        $result = openssl_sign($data, $signature, $this->GetPrivateKey(), $OPENSSL_ALGO);
        if($result != 1)
            return false;
        return new CryptData($signature);
    }

    /**
     *
     * @param string $data
     * @param string $sign
     * @param int $OPENSSL_ALGO man to OPENSSL ALGO
     * @return boolean
     */
    public function Verify($data, $sign, $OPENSSL_ALGO = OPENSSL_ALGO_MD5) {
        $flag = openssl_verify($data, $sign, $this->GetPublicKey(), $OPENSSL_ALGO);
        return $flag;
    }

}

/**
 * Initialize Default
 */