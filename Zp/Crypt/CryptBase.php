<?php
namespace Zp\Crypt;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CryptBase
 *
 * @author shotonoff
 */
abstract class CryptBase {
    
    /**
     *
     * @var CryptData
     */
    protected $data;
    
    /**
     *
     * @var mixed 
     */
    protected $enCryptData;
    
    public function __construct(){
        $this->data = new CryptData();
    }
    
    public function SetData($data){
        if(null !== $data && false !== $data){
            $this->data = $data;
        }
    }
    
    public function SetEnCryptData($data){
        if(null !== $data && false !== $data){
            $this->enCryptData = $data;
        }
    }
    
    public function GetData(){
        return $this->data;
    }
    
    public function GetEnCryptData(){
        return $this->enCryptData;
    }    
    
}