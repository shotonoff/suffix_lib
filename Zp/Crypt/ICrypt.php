<?php
namespace Zp\Crypt;
/**
 *
 * @author shotonoff
 */
interface ICrypt {
    
    /**
     * Шифровать данные
     * @return mixed {Crypt Data}
     */
    public function EnCrypt($data = null);
    
    /**
     * Расшифровать данные
     * @return mixed {DeCrypt Data}
     */
    public function DeCrypt($cryptData = null);
    
    /**
     * Подписать данные
     * @return mixed sign
     */
    public function Sign($data, $OPENSSL_ALGO = OPENSSL_ALGO_MD5);
    
    /**
     * 
     */
    public function Verify($data, $sign, $OPENSSL_ALGO = OPENSSL_ALGO_MD5);   
}