<?php

namespace Zp\Crypt;

/**
 * Description of CryptDataBase
 *
 * @author shotonoff
 */
class CryptData {

    private $data = null;

    public function __construct($data = null) {
        if (null !== $data)
            $this->SetData($data);
    }

    public function SetData($data) {
        $this->data = $data;
    }

    /**
     *  
     */
    public function GetData() {
        return $this->data;
    }

    private function length() {
        return strlen($this->GetData());
    }

    public function ToBin() {
        return pack("H" . $this->length(), $this->GetData());
    }

    /**
     *  
     */
    public function ToHex() {
        return bin2hex($this->GetData());
    }

    public function Bin2Base64() {
        return base64_encode($this->GetData());
    }

    public function Base642Bin() {
        return base64_decode($this->GetData());
    }

}