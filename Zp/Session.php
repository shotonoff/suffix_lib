<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author shotonoff
 */

namespace Zp;

class Session implements ISession
{

    static public function Start()
    {
        session_start();
    }

    public function __construct()
    {

    }

    public function Set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function Get($key)
    {
        if ($this->IsVar($key))
            return $_SESSION[$key];
        else
            return null;
    }

    public function IsVar($key)
    {
        return isset($_SESSION[$key]);
    }

    public function Remove($key)
    {
        if (isset($_SESSION[$key]))
            unset($_SESSION[$key]);
    }

}