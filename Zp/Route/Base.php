<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\Route;

abstract class Base
{
    protected $name;
    protected $pattern;
    protected $action;
    protected $controller;
    protected $options;
    protected $params;

    public function __construct($name, $pattern, $options)
    {
        $this->SetName($name);
        $this->SetPattern($pattern);
        $this->SetOptions($options);
    }

    public function SetOptions($options)
    {
        if (!isset($options['action']) && !empty($options['action']))
            throw new \Exception();
        if (!isset($options['controller']) && !empty($options['controller']))
            throw new \Exception();

        $this->action = $options['action'];
        $this->controller = $options['controller'];
    }

    public function SetName($name)
    {
        $this->name = $name;
    }

    public function GetName()
    {
        return $this->name;
    }

    public function SetPattern($pattern)
    {
        $this->pattern = $pattern;
    }

    public function GetPattern()
    {
        return $this->pattern;
    }

    public function GetAction()
    {
        return $this->action;
    }

    public function GetController()
    {
        return $this->controller;
    }

    /**
     * @return array
     */
    public function GetParams()
    {
        return $this->params;
    }

}