<?php

/**
 * Description of IRoute
 *
 * @author shotonoff
 */

namespace Zp\Route;

interface IRoute {
    
    public function Check(\Zp\Http\Url $uri);
    
}