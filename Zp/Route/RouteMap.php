<?php

/**
 * Description of RouteMap
 *
 * @author shotonoff
 */

namespace Zp\Route;

use Zp\Route\IRoute;

class RouteMap implements \Iterator
{

    protected $routes = array();

    protected $position = 0;

    public function __construct()
    {
        //        $this->Add($route);
    }

    public function Add(IRoute $route)
    {
        $this->routes[] = $route;
    }

    public function current()
    {
        return $this->routes[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->routes[$this->position]);
    }

    public function GetByName($name)
    {
        $this->position = 0;
        foreach ($this->routes as $route)
        {
            /** @var \Zp\Route\IRoute */
            if ($route->GetName() == $name)
                return $route;
        }
        return false;
    }

}