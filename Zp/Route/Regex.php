<?php

/**
 * Description of Regex
 *
 * @author shotonoff
 */

namespace Zp\Route;

class Regex extends Base implements IRoute
{

    public function SetOptions($options)
    {
        parent::SetOptions($options);

        if (isset($options['regexp']) && !empty($options['regexp'])) {
            $this->params = array_fill_keys($options['regexp'], null);
        }

        $this->options = $options;
    }


    public function SetPattern($pattern)
    {
        if (empty($pattern))
            $pattern = "//";
        else {
            if (substr($pattern, 0, 1) != "/")
                $pattern = "/" . $pattern;
            if (substr($pattern, -1, 1) != "/" && strlen($pattern) > 1)
                $pattern .= "/";
        }

        $this->pattern = $pattern;
    }

    public function Check(\Zp\Http\Url $url)
    {
        if (!preg_match($this->GetPattern(), $url->Uri(), $matches))
            return false;

        if (empty($matches) || empty($this->params))
            return true;

        $i = 1;
        foreach ($this->params as &$value) {
            $value = $matches[$i];
            ++$i;
        }

        return true;
    }


}