<?php

/**
 * Description of Regex
 *
 * @author shotonoff
 */

namespace Zp\Route;

class Hardroute extends Base implements IRoute
{
    public function Check(\Zp\Http\Url $url)
    {
        return ($this->GetPattern() === $url->Uri());
    }
}