<?php

function CurlSendForm($TargetUrl, $Values = '', $Head = 0, $NoBody = 0, $Return = 1, $Redirect = 0, $Cert = '', $CertPass = '', $TimeOut = 60) {
    $ch = curl_init($TargetUrl);
    if (!$ch) {
        //WriteLog("Модуль CURL не инициализируется! TargetUrl=$TargetUrl");
        return false;
    }
    curl_setopt($ch, CURLOPT_HEADER, $Head);
    curl_setopt($ch, CURLOPT_NOBODY, $NoBody);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, $Return);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $Redirect);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLE_SSL_PEER_CERTIFICATE, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, $TimeOut);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
    if ($Cert) {
        curl_setopt($ch, CURLOPT_SSLCERT, $Cert);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $CertPass);
    }
    if ($Values) {
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Values);
        curl_setopt($ch, CURLOPT_POST, 1);
    } else {
        curl_setopt($ch, CURLOPT_POST, 0);
    }
    $Result = curl_exec($ch);
    if ($Result === false) {
        //WriteLog("Error CURL number: " . curl_errno($ch) . " message: " . curl_error($ch) . "TargetUrl=$TargetUrl");
        return false;
    }
    curl_close($ch);
    return $Result;
}

//## перекодировка win1251 -> unicode (UTF-8) 
function win_utf8($in_text) {
    $output = "";
    $other[1025] = "Ё";
    $other[1105] = "ё";
    $other[1028] = "Є";
    $other[1108] = "є";
    $other[1030] = "I";
    $other[1110] = "i";
    $other[1031] = "Ї";
    $other[1111] = "ї";

    for ($i = 0; $i < strlen($in_text); $i++) {
        if (ord($in_text{$i}) > 191) {
            $output.="&#" . (ord($in_text{$i}) + 848) . ";";
        } else {
            if (array_search($in_text{$i}, $other) === false) {
                $output.=$in_text{$i};
            } else {
                $output.="&#" . array_search($in_text{$i}, $other) . ";";
            }
        }
    }
    return $output;
}

function zp_round($value) {
    if (is_null($value) || empty($value))
        return "0.00";
    
    if(false == strpos($value, "."))
        return number_format($value, 2, ".", "");
    
    $r = explode(".", $value);
    if (strlen($r[1]) > 2)
        if($r[1][2] > 5){
            if($r[1][1] == 9)
                if($r[1][0] == 9)
                    return ($r[0] + 1) . ".00";
                else
                    return $r[0] . "." . $r[1][0] + 1;
            else
                return $r[0] . "." . $r[1][0] . ($r[1][1] + 1);
        } else {
            return $r[0] . "." . $r[1][0] . ($r[1][1]);
        }
    else
        return number_format($value, 2, ".", "");
}