<?php

/**
 * Description of Interface
 *
 * @author shotonoff
 */

namespace Zp\Form;

interface IForm {

    /**
     *
     */
    public function setAction($action);

    /**
     * 
     */
    public function getAction();

    /**
     * 
     */
    public function setMethod($method);

    /**
     *
     */
    public function getMethod();

    /**
     * Генерирует TOKEN
     * сохраняет токен в СЕССИЮ
     * @return <String> возвращает строку с токеном
     */
//    public function createToken();
//    public function setSession(\Zp\ISession $session);

    /**
     * @return <Session>
     */
    public function getSession();

    /**
     *
     */
    public function filter();

    /**
     * 
     */
    public function isValid(\Zp\Http\IRequest $request);

    /**
     *
     */
    public function __toString();
}
