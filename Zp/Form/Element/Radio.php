<?php

/**
 * Description of Text
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Radio extends ElementBase {

    private $_checked = false;

    /**
     *
     * @param <Boolean> $flag
     */
    public function setChecked($flag) {
        $this->_checked = $flag;
    }

    /**
     *
     * @return <Boolean>
     */
    public function getChecked() {
        return $this->_checked;
    }

    public function build(\Zp\IView $view = null) {
        $checked = ($this->getChecked()) ? 'checked' : '';
        return '<label>' . $this->getLabel() . '</label><input type="radio" name="' . $this->getName() . '" ' . $this->attrToStr() . ' value="' . $this->getValue() . '" ' . $checked . ' />';        
    }

}