<?php

/**
 * Description of Textarea
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Textarea extends ElementBase {

    public function build(IView $view = null) {
        return '<textarea name="' . $this->getName() . '" ' . $this->attrToStr() . '>' . $this->getValue() . '</textarea>';
    }

}