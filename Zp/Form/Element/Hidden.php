<?php

/**
 * Description of Hidden
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Hidden extends ElementBase {

    public function build(IView $view = null) {
        $attrs = $this->attrToStr();
        return '<input type="hidden" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $attrs . ' />';
    }

}