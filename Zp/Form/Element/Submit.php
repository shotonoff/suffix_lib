<?php

/**
 * Description of Submit
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Submit extends ElementBase {

    public function build(IView $view = null) {
        $attrs = $this->attrToStr();
        return '<input type="submit" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $attrs . ' />';
    }

}