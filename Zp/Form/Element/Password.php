<?php

/**
 * Description of Button
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Password extends ElementBase {

    public function build(IView $view = null) {
        $attrs = $this->attrToStr();
        return '<input type="password" name="' . $this->getName() . '" value="' . $this->getValue() . '" ' . $attrs . ' />';
    }

}