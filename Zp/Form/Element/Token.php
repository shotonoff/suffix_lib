<?php

/**
 * Description of Text
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\Element\Hidden,
    Zp\IView;

class Token extends Hidden {
    
    public function __construct($name, array $options = null) {
        parent::__construct('token-' . $name, $options);
        $session = new \Zp\Session();
        
        $gen_numb = rand(100000, 100000000);
        $hash_md5_numb = md5($gen_numb) . '++';

        $token = array();

        if ($session->isVar('token')) {
            $token = $session->Get('token');
        }

        $token[$this->getName()] = $hash_md5_numb;
        $session->Set('token', $token);

        $this->setValue($hash_md5_numb);
    }

    public function build(IView $view = null) {
        $output = parent::build();
        return $output;
    }

}