<?php

/**
 * Description of DownList
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class DownList extends ElementBase {

    private $options;
    
    private $selected;

    public function setItems(array $options) {
        $this->options = $options;
    }
    
    public function Selected($selected) {
        $this->selected = $selected;
    }
    
    public function build(IView $view = null) {
        $attrs = $this->attrToStr();
        
        $output = '<select name="' . $this->getName() . '" ' . $attrs . '>';
        foreach ($this->options as $value => $text) {
            if($this->selected == $value) {
                $output .= sprintf("<option selected value=\"%s\">%s</option>", $value, $text);
            } else {
                $output .= sprintf("<option value=\"%s\">%s</option>", $value, $text);
            }
        }
        $output .= '</select>';
        
        return $output;
    }

}