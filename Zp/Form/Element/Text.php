<?php

/**
 * Description of Text
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
Zp\IView;

class Text extends ElementBase
{

    public function build(IView $view = null)
    {
        $output = '';
        //        if($this->getLabel() != '' && $this->getLabel() != null){
        //            $output .= '<label>' . $this->getLabel() . '</label>';
        //        }
        $required = ($this->_required) ? "required=\"true\"" : "";

        $readonly = ($this->GetReadonly()) ? "disabled=\"TRUE\"" : "";

        $output .= '<input ' .
            $readonly .
            $required . ' type="text" name="' . $this->getName() . '" ' . $this->attrToStr() . ' value="' . $this->getValue() . '"/>';

        if ($this->GetReadonly()) {
            $fieldHidden = new Hidden($this->getName(), array('value' => $this->getValue()));
            $output .= $fieldHidden;
        }

        return $output;
    }

}