<?php

/**
 * Description of Text
 *
 * @author shotonoff
 */

namespace Zp\Form\Element;

use Zp\Form\ElementBase,
    Zp\IView;

class Checkbox extends ElementBase {

    private $_checked = false;

    /**
     *
     * @param <Boolean> $flag
     */
    public function setChecked($flag) {
        $this->_checked = $flag;
    }

    /**
     *
     * @return <Boolean>
     */
    public function getChecked() {
        return $this->_checked;
    }

    public function build(\Zp\IView $view = null) {
        $checked = ($this->getChecked()) ? 'checked' : '';
        //<label>' . $this->getLabel() . '</label>
        return '<input type="hidden" name="' . $this->getName() . '"  value="0" /><input type="checkbox" name="' . $this->getName() . '" ' . $this->attrToStr() . ' ' . $checked . ' />';
//        return '<input type="hidden" name="' . $this->getName() . '"  value="' . $this->getValue() . '" /><input type="checkbox" name="' . $this->getName() . '" ' . $this->attrToStr() . ' value="' . $this->getValue() . '" ' . $checked . ' />';
    }

}