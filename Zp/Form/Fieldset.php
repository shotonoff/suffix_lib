<?php

namespace Zp\Form;

/**
 * Description of Fieldset
 *
 * @author shotonoff
 */
class Fieldset extends Container
{

    /**
     *
     * @var <String>
     */
    private $_legend = null;

    public function setLegend($legend)
    {
        $this->_legend = $legend;
    }

    public function getLegend()
    {
        return $this->_legend;
    }

    public function render(\Zp\IView $view = null)
    {
        $output = '<fieldset>';
        $output .= '<legend>' . $this->getLegend() . '</legend>';
        $output .= parent::render($view);
        $output .= '</fieldset>';
        return $output;
    }

    public function __toString()
    {
        return $this->render();
    }

}