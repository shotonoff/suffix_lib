<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Zp\Form;

interface IElement {

    /**
     * Добавить валидаторы
     */
    public function addValidator(\Zp\IValidate $validator);

    /**
     * добавить массив валидаторов
     * элеметнами массива должны быть элементы типа \Zp\IValidate
     */
    public function addValidators(array $validators);

    /**
     * Добавить фильтр
     */
    public function addFilter(\Zp\IFilter $filter);

    /**
     * Добавить массив фильтров, элементами должны быть элементами типа Library_Filter_Interface
     */
    public function addFilters(array $filters);

    /**
     *
     */
    public function setRequired($flag);

    /**
     * 
     */
    public function isRequired();

    public function filter();

    public function isValid($value);
}