<?php

/**
 * Description of FormAbstract
 *
 * @author shotonoff
 */

namespace Zp\Form;

abstract class Base extends General implements IForm {

    /**
     * @var \Zp\Form\IContainer
     */
    protected $_container;

    /**
     * @var string
     */
    protected $_method = 'POST';

    /**
     * @var string
     */
    protected $_action;

    /**
     * @var <Session>
     */
    protected $_session = null;

    /**
     * @var <type>
     */
    protected $isCreateToken = true;

    public function init() {
        parent::init();
        $this->_container = new Container('main_container');
    }

    public function addElement($name, IContainer $element) {
//        $element->setView($this->getView());
        $this->_container->addElement($name, $element);
        return $this;
    }

    public function addElements(array $elements) {
        return $this;
    }

    public function getElement($name) {
        return $this->_container->getElement($name);
    }

    public function getElements() {
        ;
    }

    public function getAllElements() {
        $container = $this->_container->getElementNonContainer();
        return $container;
    }

    public function getAction() {
        return $this->_action;
    }

    public function getMethod() {
        return $this->_method;
    }

    public function setAction($action) {
        $this->_action = $action;
    }

    public function setMethod($method) {
        $this->_method = strtoupper($method);
    }

    public function setSession(\Zp\ISession $session) {
        $this->_session = $session;
    }

    public function getSession() {
        return $this->_session;
    }

    /**
     * @param boolean $flag 
     */
    
    /*
    public function MakeToken($flag) {
        $this->isCreateToken = $flag;
    }
    
    public function createToken() {
        $session = $this->getSession();
        
        if (null === $session) {
            throw new \Exception();
        }

        $gen_numb = rand(100000, 100000000);
        $hash_md5_numb = md5($gen_numb) . '++';

        $token = array();

        if ($this->getSession()->isVar('token')) {
            $token = $this->getSession()->Get('token');
        }

        $token[$this->getName()] = $hash_md5_numb;
        $this->getSession()->Set('token', $token);

        return $hash_md5_numb;
    }*/

    /**
     * @return <Boolean>
     */
    public function isPost() {
        return ('POST' === $this->getMethod());
    }

    public function filter() {
        
    }

    public function isValid(\Zp\Http\IRequest $request) {

        if ($this->isCreateToken) {
            $token_name = 'token-' . $this->getName();
            $tokenParam = $request->Get('token-' . $this->getName());
            if (!$request->IsPost($token_name)) {
                return false;
            }
            
            $token = $request->Get($token_name);
            $sToken = $this->getSession()->Get('token');
            if (!$sToken[$this->getName()] === $token) {
                return false;
            }
        }

        $allElements = $this->getAllElements();
        
        try {
            foreach ($allElements as $el) {
                $elName = $el->getName();
                $value = $request->Get($elName);
                $flag = $el->isValid($value);
                if (false === $flag) {
                    return false;
                }
                $el->setValue($value);
            }
            return true;
        } catch (Exception $e) {
            return false;
        }

        /**
         *
         * @todo проверить на совпадение токена
         */
    }
    
    public function BindForModel($Model) {       
        $Model;
    }

    public function __toString() {
        return '';
    }

}