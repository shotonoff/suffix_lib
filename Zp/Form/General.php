<?php

namespace Zp\Form;

/**
 * Description of general
 *
 * @author shotonoff
 */
abstract class General {

    /**
     *
     * @var <Library_View_Interface>
     */
    protected $_view;

    /**
     *
     * @var <array>
     */
    protected $_attribs = array();

    /**
     *
     * @var <String>
     */
    protected $_name;

    /**
     *
     * @var <String>
     */
    protected $_id;

    /**
     *
     * @var <array>
     */
    protected $_errorMessage = array();

    /**
     *
     * @var <String>
     */
    protected $_tpl = null;

    public function __construct($name, array $options = null) {
        $this->setName($name);

        if (null !== $options) {
            $this->setOptions($options);
        }

        $this->init();
    }

    public function init() {
        $this->createId();
    }

    /**
     *
     * @param <String> $id
     */
    public function setId($id) {
        $this->_id = $id;
    }

    public function getId() {
        return $this->_id;
    }

    private function createId() {
        $this->_id = 'form-' . $this->getName();
    }

    /**
     *
     * @param Library_View_Interface $view 
     */
    public function setView(\Zp\IView $view) {
        $this->_view = $view;
    }

    /**
     *
     * @return <Library_View_Interface>
     */
    public function getView() {
        return $this->_view;
    }

    /**
     *
     * @param array $options
     */
    public function setOptions(array $options) {
        foreach ($options as $name => $value) {    
            $methodName = 'set' . ucfirst(strtolower($name));
            if (method_exists($this, $methodName)) {
                call_user_func(array($this, $methodName), $value);
            } elseif (property_exists($this, $name)) {
                $this->$name = $value;
            }
        }
    }

    public function setName($name) {
        $this->_name = $name;
    }

    public function getName() {
        return $this->_name;
    }

    /**
     * Добавить элемент
     */
    public function addAttrib($key, $value) {
        $this->_attribs[$key] = $value;
    }

    public function setAttribs(array $attribs) {
        foreach ($attribs as $key => $value) {
            $this->addAttrib($key, $value);
        }
    }

    public function removeAttrib($key){
        if(isset($this->_attribs[$key])){
            unset($this->_attribs[$key]);
        }
    }

    /**
     * Получить массив аттрибутов
     * @return <array>
     */
    public function getAttribs() {
        return $this->_attribs;
    }

    /**
     * Очистить массив с аттрибутов
     */
    public function clearAttribs() {
        $this->_attribs = array();
    }

    protected function attrToStr() {
        $attrs = array();
        foreach ($this->getAttribs() as $name => $value) {
            if (null === $value) {
                continue;
            }
            $attrs[] = $name . '="' . $value . '"';
        }
        return implode(' ', $attrs);
    }

    public function addErrorMessage($message) {
        $this->_errorMessage[] = $message;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

    public function clearErrorMessage() {
        $this->_errorMessage = array();
    }

    public function setTpl($tpl) {
        $this->_tpl = $tpl;
    }

    public function getTpl() {
        return $this->_tpl;
    }

}