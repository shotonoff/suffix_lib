<?php

/**
 * Description of Fieldset
 *
 * @author shotonoff
 */

namespace Zp\Form;

class Container extends General implements IContainer
{

    /**
     * @var <array>
     */
    protected $_children;

    public function addElement($name, IContainer $element)
    {
        $this->_children[$name] = $element;
    }

    public function addElements(array $elements)
    {
        foreach ($elements as $name => $element) {
            $this->addElement($name, $element);
        }
    }

    public function getElement($name)
    {
        if (array_key_exists($name, $this->_children)) {
            return $this->_children[$name];
        }
        return null;
    }

    public function getElements()
    {
        return $this->_children;
    }

    public function getElementNonContainer()
    {
        $_ = array();
        if (null != $this->_children)
            foreach ($this->_children as $element) {
                if ($element instanceof ElementBase) {
                    $_[] = $element;
                    continue;
                }
                $_ = array_merge($_, $element->getElementNonContainer());
            }
        return $_;
    }

    public function render(\Zp\IView $view = null)
    {
        if (null === $view) {

        }

        $output = '';
        if (!empty($this->_children)) {
            foreach ($this->_children as $name => $element) {
                $output .= $element->render();
            }
        }

        return $output;
    }

}