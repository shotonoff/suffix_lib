<?php

/**
 * Description of Text
 *
 * @author shotonoff
 */

namespace Zp\Form;

abstract class ElementBase extends General implements IElement, IContainer
{

    /**
     *
     * @var <Boolean>
     */
    protected $_required;

    /**
     *
     * @var <String>
     */
    protected $_value;

    /**
     *
     * @var <String>
     */
    protected $_label;

    /**
     *
     * @var <String>
     */
    protected $description;

    protected $isReadonly = false;

    public function SetReadonly($flag)
    {
        if (!is_bool($flag))
            throw new \Exception;
        $this->isReadonly = $flag;
    }

    public function GetReadonly()
    {
        return $this->isReadonly;
    }

    public function isRequired()
    {
        return $this->_required;
    }

    public function setRequired($flag)
    {
        if (!is_bool($flag)) {
            throw new Exception('Неверный тип REQUIRE, должен быть BOOLEAN');
        }
        $this->_required = $flag;
    }

    public function setValue($value)
    {
        $this->_value = $value;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function addFilter(\Zp\IFilter $filter)
    {
        ;
    }

    public function addFilters(array $filters)
    {
        ;
    }

    public function addValidator(\Zp\IValidate $validator)
    {
        ;
    }

    public function addValidators(array $validators)
    {
        ;
    }

    public function filter()
    {

    }

    public function isValid($value)
    {
        return true;
    }

    public function setLabel($label)
    {
        $this->_label = $label;
        return $this;
    }

    public function getLabel()
    {
        return $this->_label;
    }

    abstract public function build(\Zp\IView $view = null);

    public function render(\Zp\IView $view = null)
    {
        return $this->build($view);
    }

    /*     * ************************ */
    /*     * ************************ */
    /*     * ************************ */
    /*     * ************************ */

    public function addElement($name, IContainer $element)
    {
        ;
    }

    public function addElements(array $elements, $group = null)
    {
        ;
    }

    public function getElement($name)
    {
        ;
    }

    public function getElements()
    {
        ;
    }

    public function __toString()
    {
        return $this->render($this->_view);
    }

}