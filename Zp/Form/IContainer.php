<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Description of Interface
 *
 * @author shotonoff
 */

namespace Zp\Form;

interface IContainer
    {

    /**
     *
     */
    public function addElement($name, IContainer $element);

    /**
     * 
     */
    public function addElements(array $elements);

    /**
     * 
     */
    public function getElement($name);

    /**
     *
     */
    public function getElements();

    /**
     * Сформировать html
     */
//    public function render(Library_View_Interface $view = null);
    public function render(\Zp\IView $view = null);
    
    }
