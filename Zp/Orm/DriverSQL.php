<?php

namespace Zp\Orm;

class DriverSql {
	protected $MySqlI;
	protected $MySqlResult;
	//Кодировка 
	protected $EncodeBD = '';
	//Текущий язык контента
	protected $ContentLang = '';
	//последняя Ошибка 
	protected $Error = '';
	//Владелец запроса
	protected $OwnerQuery = ''; 
	//Время актуальности данных из кеша в секундах
	protected $TimeLifeCach = 0;
	//Обновлять данные в кеш?
	protected $UpdateCach = false;
	//Вести логи запросов
	protected $ArWriteLog = array('select'=>false, 'insert'=>true, 'update'=>true, 'delete'=>true);	
	//SQL последнего выполненного запроса в БД 
	protected $LastSQL = '';
	//Конструктор подключение к базе данных
	public function __construct($Host, $User, $Pass, $Base, $Charset = 'utf8', $Lang = 'ru_RU') {
		global $Debug;
		//Подключение к БД через интерфейс mysqli
		$this->MySqlI = new mysqli($Host, $User, $Pass, $Base);
		if(mysqli_connect_errno()) {
			$this->Error = gettext('Не установлено соединение с БД').' Err: '.mysqli_connect_error();
			if($Debug) printf("Connect failed: %s\n", mysqli_connect_error());
		}
		//Устанавливаем текущую кодировку 
		$this->SetCharset($Charset);
		//Устанавливаем текущий язык
		$this->SetContentLang($Lang);
		if($_SESSION['ZP']) $this->OwnerQuery = $_SESSION['ZP'];
		elseif($_SESSION['zpay']) $this->OwnerQuery = $_SESSION['zpay'];
	}
	//Деструктор объекта
	public function __destruct() {
		if($this->MySqlI) $this->MySqlI->close();
		if(is_object($this->MySqlResult)) $this->MySqlResult->free_result();
	}
	//Получаем последнию ошибку SQL 
	public function GetSqlError() {
		return $this->MySqlI->error;
	}
	//Получаем последнию ошибку объекта 
	public function GetError() {
		return $this->Error;
	}
	//Получаем последний запрос
	public function GetLastSQL() {
		return $this->LastSQL;
	}	
	//Получить Текущая кодировка
	public function GetCharset() {
		return $this->MySqlI->character_set_name();
	}
	//Установить Текущая кодировка
	public function SetCharset($NewCharset) {
		global $Debug;
		if(!$this->MySqlI->set_charset($NewCharset)) { 
			$this->Error = gettext('Не верно указана кодировка БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		return $this->MySqlI->character_set_name();
	}
	//Получить Текущий язык
	public function GetContentLang() {
		return $this->ContentLang;
	}
	//Установить Текущий язык
	public function SetContentLang($NewLang) {
		global $ArCodeLang, $Debug, $DefaultLang;
                if(!$NewLang) $NewLang = $DefaultLang;
		if(array_search($NewLang, $ArCodeLang)===false) {
			$this->Error = gettext('Не верно указан текущий язык контента').' '.$NewLang;
			if($Debug) echo $this->Error.'<br />';
			return false; 
		}
		else $this->ContentLang = $NewLang;
		return $this->ContentLang;
	}
	//Функция устанавляивает время актуальности кеша при получении данных время задается в секундах
	public function SetTimeLifeCach($NewTime = 0) {
		$this->TimeLifeCach = $NewTime;
		return true;
	}
	public function GetTimeLifeCach($NewTime = 0) {
		return $this->TimeLifeCach;
	}
	//Отключает обновление кеша
	public function DisableUpdateCach() {
		$this->UpdateCach = false;
		return true;
	}
	//Включает обновление кеша
	public function EnableUpdateCach() {
		$this->UpdateCach = true;
		return true;
	}
	public function GetUpdateCach() {
		return $this->UpdateCach;
	}
	/*Функция выборки из БД 
	if(false === $ResultQuery = $f->Select("`ПОЛЯ`", "`ТАБЛИЦЫ`", "`УСЛОВИЕ` = ?", array(ЗНАЧЕНИЕ), $_SESSION['Lang'])) {
		$this->ErrCode = '600';
		$this->ErrName = gettext('Ошибка SQL запроса');
		$this->ErrNameDebug = '';
		return false;			
	}	
	*/
	//В ответ возвращает ассоциативный массив из БД, все поля с префиксом LNG_ содержат значения на базовом языке
	//Поля без префикса перевод на язык который указан в $Lang
	public function Select($What, $From, $Where, $ArValueWhere, $Lang = 'ru_RU', $WriteLog = false) {
		global $Debug, $DefaultLang, $ArCodeLang, $DefaultEncoding, $PathTempFile;
		if(!$What) {
			$this->Error = gettext('Не указано какие поля выбирать из таблицы');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!$From) {
			$this->Error = gettext('Не указано из каких таблиц сделать выборку');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!$Where) {
			$this->Error = gettext('Не указан шаблон условия выборки из таблицы');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!is_array($ArValueWhere) || !count($ArValueWhere)) {
			$this->Error = gettext('Не указаны переменные для условия выборки из таблицы');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}	
		//Если нет соединения с БД то ошибка
		if(!$this->MySqlI) {
			$this->Error = gettext('Не установлено соединение с БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Выбор языка выборки 
		if(array_search($Lang, $ArCodeLang)===false) {
			if($this->ContentLang) $CurrrentLang = $this->ContentLang;
			else $CurrrentLang = $DefaultLang;
		} else {
			$CurrrentLang = $Lang;
		}
		//if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />';
		//Формируем условие с фильрацией переменных 
		$Where = $this->PrepareQuery($Where, $ArValueWhere);
		if(!$Where) return false;
		//Получаем список полей с LNG_ для всех таблиц из которых идет выборка
		$ArNameField = $this->GetInfoFieldTable($From);
		//Если такие поля в таблицах есть, нужно внести изменения в названия полей указав LNG_ для тех полей
		if(is_array($ArNameField)) {
			//Перебор всех полей если указаны поля конкретные для выборки 
			if(trim($What)!='*') {
				//Убираем все префиксы LNG_ что бы не вышло LNG_LNG_ 
				$What = str_replace("LNG_", "", $What);
				foreach($ArNameField as $NameField) {
					//Обрезаем поле что бы получился без префикса LNG_
					$NameColumn = substr($NameField['Field'], 4);
					//Ищем это поле в запросе и меняем его на с префиксом
					$What = preg_replace("|(.*)($NameColumn)(.*)|", "\\1LNG_\\2\\3", $What);
				}
			}
		}
		$SqlSelect = 'SELECT '.$What.' FROM '.$From.' WHERE '.$Where;
		if($this->TimeLifeCach>0 || $this->UpdateCach) {
			//Ищем файл который содержащий кеш
			$NameCachFile = substr(sha1($SqlSelect.$Lang), 0, 14).'.cch';
		}
		//Если есть вермя жизни кеша? пробуем получить данные из кеша	
		if($this->TimeLifeCach>0) {
			if(file_exists($PathTempFile.$NameCachFile)) {
				//Пробуем получить данные из кеша
				$ArData = unserialize(file_get_contents($PathTempFile.$NameCachFile));
				//Если есть дата обновления кеша
				if($ArData['CachUpdate']>0) {
					//Проверяем срок годности кеша
					if((time() - $ArData['CachUpdate']) <= $this->TimeLifeCach) {
						//Если время прошло меньше чем время жизни кеша		
						//if($Debug) echo 'Время жизни '.$this->TimeLifeCach.' Текущее время '.time().' Время записи в кеш '.$ArData['CachUpdate'].' Данные получены из кеша<br />';
						unset($ArData['CachUpdate']);
						return $ArData;
					} 
				}
			}
		}
		//Пишем в лог если этого требуется или если это режим отладки
		if($WriteLog || $this->ArWriteLog['select'] || $Debug) WriteLog('#'.$SqlSelect.'#', 'sql_select.log');
		//Сохраняем последний запрос SQL 
		$this->LastSQL = $SqlSelect;
		//Получаем записи из БД
		$Result = $this->MyQuery($SqlSelect);
		//Получены данные из таблицы
		if(is_array($Result)) {
			//Перебор всех полученных записей
			foreach($Result as $Key => $Row) {
				//Поиск всех полей с префиксом LNG_
				foreach($Row as $NameField => $Value) {
					//Если переменная есть то проверяем текущий язык
					//Находим все поля с префиксом языка 
					if($Value && strpos($NameField, 'LNG_')===0) {	
						//Если текущий язык отличается от базового
						//if($DefaultLang!=$CurrrentLang) {
								/*
								//Определение кодировки полученного результата
								$FieldEncoding = mb_detect_encoding($Value, "Windows-1251, UTF-8");
								//Если кодировка поля WIN1251 а сайта UTF-8 нужно это значение 
								//конвертировать в UTF-8
								if($DefaultEncoding == 'utf-8' && $FieldEncoding=='Windows-1251') {
									$Value = win_utf8($Value);
								}*/	
								//Формируем Хеш для поиска из значения по умолчанию
								$IndexKey = md5($Value);
								//Получем записи из таблицы переводов
								$ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE` LIKE '$IndexKey' LIMIT 1");
								if(is_array($ResultTranslate) && $ResultTranslate[0][$CurrrentLang]) {
									//Сохраняем поле с нужным языком в переменной без префикса
									$Result[$Key][substr($NameField, 4)] = $ResultTranslate[0][$CurrrentLang];
									if($Debug) {
										//echo 'Запись с переводами: <br />';
										//EchoArray($ResultTranslate);
									}
								} else {
									//Если просто нет перевода
									if(!is_array($ResultTranslate)) {
										//Если это базовый язык и есть значение поля из старой версии без префикса
										//Нужно скопироать значение этого поля в новое с префиксом и добавить в таблицу перевода
										//Если это выборка уникальных элементов, то нельзя подставлять поле из старого значения
										//так как это помешает функции Update выполнить добавление записей в таблицу перевода  
										if($DefaultLang==$CurrrentLang && $Result[$Key][$NameField] && substr($What, 0, 8)!='DISTINCT') {
											$Result[$Key][substr($NameField, 4)] = $Result[$Key][$NameField];
										} else {
											//Вообще нет записи для этого поля 
											$Result[$Key][substr($NameField, 4)] = "_NOT_FOUND_";
										}
									} else {
										//Иначе присваеваем спец значение, указывающее, что перевод не найден в записи
										$Result[$Key][substr($NameField, 4)] = "_NOT_TRANSLATED_";
										//$Result[$Key][$NameField];// gettext("Нет перевода");
									}
								}
					    /* } else {
						 	//Если есть префикс языка 
						 	if(strpos($NameField, 'LNG_')===0) {
								//Добавляем поле без префикса и сохраняем туда значение базового языка
								$Result[$Key][substr($NameField, 4)] = $Result[$Key][$NameField];
							}
					     }*/
					} elseif(!$Value && strpos($NameField, 'LNG_')===0) {
						if($Debug) {
							//echo 'Значение языкового поля '.$NameField.' не найдено в таблице '.$From.' для выборки '.$Where.'<br />';
						}						
						if($Result[$Key][substr($NameField, 4)]) $Result[$Key][$NameField] = $Result[$Key][substr($NameField, 4)]; 
						else $Result[$Key][$NameField] = '';
					}
				}
			}
		}
		//Если нужно обновить кеш данные 	
		if($this->UpdateCach && is_array($Result) && count($Result)) {
			$CachData = $Result;
			$CachData['CachUpdate'] = time();
			if(!file_put_contents($PathTempFile.$NameCachFile, serialize($CachData))) {
				if($Debug) {
					echo 'Не удалось обновить даные к кеш файле '.$NameCachFile.'<br />';
				}
			} else {
				//if($Debug) echo 'Данные записаны в кеш<br />';
			}
			//$ArData = unserialize(@file_get_contents($PathTempFile.$NameCachFile));
		}
		return $Result;
	}
	//Функция добавления записей в БД
	/*	
	if(false === $f->Insert("`ТАБЛИЦА`", array('ПОЛЕ'=>'ЗНАЧЕНИЕ', 'func_ПОЛЕ' => 'ФУНКЦИЯ SQL'), $_SESSION['Lang'])) {
		$this->ErrCode = '400';
		$this->ErrName = gettext('Ошибка при добавлении новых курсов валют на '.$Date);
		$this->ErrNameDebug = '';
		return false;
	}
	$Table - Таблица в которой выполняются изменения
	$ArValueWhat - Ассоциативный массив новых значений переменых для вставки
	$Lang - Язык для текстовых полей
	$WriteLog - Флаг записи запроса в лог файл
	*/
	public function Insert($Table, $ArValueWhat, $Lang = 'ru_RU', $WriteLog = true) {
		global $Debug, $DefaultLang, $ArCodeLang;
		if(!$Table) {
			$this->Error = gettext('Не указана таблица для вставки записей');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!is_array($ArValueWhat) || !count($ArValueWhat)) {
			$this->Error = gettext('Не указаны переменные для вставки записей в таблице');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Если нет соединения с БД то ошибка
		if(!$this->MySqlI) {
			$this->Error = gettext('Не установлено соединение с БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Выбор языка выборки 
		if(array_search($Lang, $ArCodeLang)===false) {
			if($this->ContentLang) $CurrrentLang = $this->ContentLang;
			else $CurrrentLang = $DefaultLang;
		} else {
			$CurrrentLang = $Lang;
		}
		//if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />';
		//Собираем все данные для отражения в ошибках
		$StrSet = '';
		//Массив в который сохраняем строку для форматирования новой переменной
		$ArFormatWhat = array();
		foreach($ArValueWhat as $NameWhat => $ValueWhat) {
			if(strpos($NameWhat, 'func_')===0) {
				$ArFormatWhat[substr($NameWhat, 5)] = $ValueWhat;
				unset($ArValueWhat[$NameWhat]);
				continue;
			}
			$StrSet .= '`'.$NameWhat."`='".$ValueWhat."', ";
		}
		if($Debug) {
			if(count($ArFormatWhat)) {
				echo 'Массив данных для форматирования переменных:<br />';
				EchoArray($ArFormatWhat);
			}
		}
		$StrSet = substr($StrSet, 0, strlen($StrSet)-2);
		//Проверка всех значений обновления 
		list($ArValueWhat, $ArNameLngField) = $this->CheckValueForTable($Table, $ArValueWhat);
		//Если проверка не прошла, то нужно сообщить об ошибке
		if($ArValueWhat===false) {
			WriteLog('#INSERT INTO '.$Table.' SET '.$StrSet.'#Error: '.$ArNameLngField, 'sql_error.log');
			return false;
		}
		//Зачистка новых переменных от кавычек и прочего не желательного
		foreach($ArValueWhat as $NameWhat => $ValueWhat) {
			if(!is_numeric($ValueWhat)) {
				//Фильтруем все переменные для обновления
				if (get_magic_quotes_gpc()) $ArValueWhat[$NameWhat] = "'".$ValueWhat."'";
				else $ArValueWhat[$NameWhat] = "'".$this->MySqlI->real_escape_string($ValueWhat)."'";
			}
		}
		//Проверить нет ли в языковых полях указания использовать функции для их задания, это не допустимо
		if(count($ArFormatWhat)) {
			//Если есть форматированные переменные для полей нужно объединить массивы
			$ArValueWhat = array_merge($ArFormatWhat, $ArValueWhat);	
			//Получаем список полей для всех таблиц из которых идет выборка
			$ArAllNameLngField = $this->GetInfoFieldTable($Table);
			if(count($ArAllNameLngField) && is_array($ArAllNameLngField)) {
				foreach($ArAllNameLngField as $NameLngField => $ValueLngField) {
					foreach($ArFormatWhat as $FormatWhat => $ValueFormatWhat) {
						//Если ключ поля равен языковому полю, это запрещено
						if($FormatWhat==substr($NameLngField, 4) || $FormatWhat==$NameLngField) {
							$this->Error = gettext('Не удалось выполнить запрос, код:')." 351";
							WriteLog('#INSERT INTO '.$Table.' SET '.$StrSet.'#Error: Попытка указать языковое поле '.$FormatWhat.' через функцию '.$ValueFormatWhat, 'sql_error.log');
							if($Debug) echo $this->Error.'<br />';
							return false;
						}
					}
				}
			}
		}
		if($Debug) {	
			if($ArNameLngField) {
				echo 'Языковые поля <br />';
				EchoArray($ArNameLngField);
			}
			echo 'Добавляемые поля<br />';
			EchoArray($ArValueWhat);
		}
		/** @desc Если есть языковые параметры нужно найти их в таблице перевода или вставить в таблицу перевода новые */
		if(is_array($ArNameLngField)) {
			//Перебор всех языковых полей последовательно с целью формирования массива SQL запросов для обновления записей 
			//в таблице перевода
			$ArSql = array();
			//Индекс в массиве доп. SQL
			$i = 0;
			//Перебор всех языковых полей 
			foreach($ArNameLngField as $NameLngField => $ValueLngField) {
				if($Debug) echo 'Языковое поле: '.$NameLngField.'<br />';
				//Ищем такое значение в таблице перевода
				//Если задан базовый язык то ищем по ключу  значения
				if($CurrrentLang==$DefaultLang) {
					$ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='".md5($ValueLngField)."'");
					//Если НЕ найдены записи нужно добавить в таблицу перевода запись  
					if(!$ResultTranslate[0]['KEY_TRANSLATE']) {
						//Нужно добавить новую запись в таблицу переводов
						$ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `".$DefaultLang."`, `EDIT_".$DefaultLang."`) VALUES ('".md5($ValueLngField)."', '".$ValueLngField."', NOW())";
					}
				} else { //Указан не базовый язык
					$ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `".$CurrrentLang."` LIKE '".$ValueLngField."'");
					//Если запись найдена нужно получить значение базового языка и установить ее для основного запроса
					if($ResultTranslate[0][$DefaultLang]) {
						if($Debug) echo 'Найден перевод на основном языке для поля '.$NameLngField.' и значения '.$ValueLngField.' перевод '.$ResultTranslate[0][$DefaultLang].'<br />';
						//Устанавливаем базовое значение для языкового поля в основной таблице
						$ArValueWhat[$NameLngField] = "'".$ResultTranslate[0][$DefaultLang]."'";
					}
					//Если НЕ найдены записи нужно попытаться найти уже по ключу  
					if(!$ResultTranslate[0]['KEY_TRANSLATE']) {
						$ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='".md5($ValueLngField)."'");
						//Если нет таких записей и по ключу 
						if(!$ResultTranslate[0]['KEY_TRANSLATE']) {
							//Нужно добавить новую запись в таблицу переводов, указать текст на другом языке в значении базового а так же и сам пеервод
							$ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `".$DefaultLang."`, `EDIT_".$DefaultLang."`, `".$CurrrentLang."`, `EDIT_".$CurrrentLang."`) VALUES ('".md5($ValueLngField)."', '".$ValueLngField."', NOW(), '".$ValueLngField."', NOW())";
						}
					}
				}
			}
		}
		//Если есть переменные в массиве основного запроса
		if(count($ArValueWhat)) {
			//Формируем строку для SQL обновления 
			$StrSet = '';
			foreach($ArValueWhat as $NameWhat => $ValueWhat) {
				$StrSet .= '`'.$NameWhat."`=".$ValueWhat.", ";
			}
			$StrSet = substr($StrSet, 0, strlen($StrSet)-2);
			$MainSql = "INSERT INTO $Table SET ".$StrSet;
			//Сохраняем последний запрос SQL 
			$this->LastSQL = $MainSql;
			if(count($ArSql)) array_unshift($ArSql, $MainSql); else $ArSql = array($MainSql);
		}
		if($Debug) {
			echo 'Итого добавляем в основную таблицу: <br />';
			EchoArray($ArValueWhat);
			echo 'Итого SQL запросы: <br />';
			EchoArray($ArSql);
		}
		//Если есть SQL запросы для выполнения
		if(count($ArSql)) {
			//Непосредственое выполнение запросов на обновление таблиц
			foreach($ArSql as $Key => $Sql) {
				//Пишем в лог если этого требуется или если это режим отладки
				if($WriteLog || $this->ArWriteLog['insert'] || $Debug) WriteLog('#'.$Sql.'#', 'sql_insert.log');
				$ResultInsert[$Key] = $this->MyQuery($Sql);
				if($ResultInsert[$Key]===false) {
					//Если это выполнение основго запроса, то это фатальная ошибка
					if(!$Key) {
						if($Debug) printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
						$this->Error = gettext('Не удалось выполнить запрос, код:')." 3557";
						if($Debug) echo $this->Error.'<br />';
						return false;
					} else { //не удалось обновить таблицы переводоа
						if($Debug) printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
						$this->Error = gettext('Не удалось выполнить запрос, код:')." 3540";
						if($Debug) echo $this->Error.'<br />';
					}
				}
			}
		} else {
			return 0;
		}
		//Возвращаем кол-во вставленных записей в основной таблице
		return $ResultInsert[0];
	}
	//Функция обновления записей в БД
	/*
	if(false === $f->Update("`ТАБЛИЦА`", array("`ПОЛЕ`"=>$НОВ_ЗНАЧ), "`ПОЛЕ` = ?", array($УСЛОВИЕ), $_SESSION['Lang'])) {
		$this->ErrCode = '607';
		$this->ErrName = gettext('Ошибка SQL запроса');
		$this->ErrNameDebug = '';
		return false;
	}	
	$Table - Таблица в которой выполняются изменения
	$ArValueWhat - Ассоциативный массив новых значений переменых для обновления
	$Where - Шаблон для формирования условия обновления
	$ArValueWhere - Массив для заполнения шаблона условия обновления
	$WriteLog - Флаг записи запроса в лог файл
	*/
	public function Update($Table, $ArValueWhat, $Where, $ArValueWhere, $Lang = 'ru_RU', $WriteLog = true) {
		global $Debug, $DefaultLang, $ArCodeLang;
		if(!$Table) {
			$this->Error = gettext('Не указана таблица для обновления записей');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!is_array($ArValueWhat) || !count($ArValueWhat)) {
			$this->Error = gettext('Не указаны переменные для обновления записей в таблице');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!$Where) {
			$this->Error = gettext('Не указан шаблон условия обновления записей в таблице');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!is_array($ArValueWhere) || !count($ArValueWhere)) {
			$this->Error = gettext('Не указаны переменные для условия обновления записей в таблице');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Если нет соединения с БД то ошибка
		if(!$this->MySqlI) {
			$this->Error = gettext('Не установлено соединение с БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Выбор языка выборки 
		if(array_search($Lang, $ArCodeLang)===false) {
			if($this->ContentLang) $CurrrentLang = $this->ContentLang;
			else $CurrrentLang = $DefaultLang;
		} else {
			$CurrrentLang = $Lang;
		}
		//if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />';
		//Формируем условие WHERE с фильрацией переменных 
		$ClearWhere = $this->PrepareQuery($Where, $ArValueWhere);
		if(!$ClearWhere) return false;
		//Собираем все данные для отражения в ошибках
		$StrSet = '';
		//Массив в который сохраняем строку для форматирования новой переменной
		$ArFormatWhat = array();
		foreach($ArValueWhat as $NameWhat => $ValueWhat) {
			//Если это поле задается функцией SQL 
			if(strpos($NameWhat, 'func_')===0) {
				$ArFormatWhat[substr($NameWhat, 5)] = $ValueWhat;
				unset($ArValueWhat[$NameWhat]);
				continue;
			}
			$StrSet .= '`'.$NameWhat."`='".$ValueWhat."', ";
		}
		if($Debug) {
			if(count($ArFormatWhat)) {
				echo 'Массив данных для форматирования переменных:<br />';
				EchoArray($ArFormatWhat);
			}
		}
		$StrSet = substr($StrSet, 0, strlen($StrSet)-2);
		//Проверка всех значений обновления 
		list($ArValueWhat, $ArNameLngField) = $this->CheckValueForTable($Table, $ArValueWhat);
		//Если проверка не прошла, то нужно сообщить об ошибке
		if($ArValueWhat===false) {
			WriteLog('#UPDATE '.$Table.' SET '.$StrSet.' WHERE '.$ClearWhere.'#Error: '.$ArNameLngField, 'sql_error.log');
			return false;
		}
		//Зачистка новых переменных от кавычек и прочего не желательного
		foreach($ArValueWhat as $NameWhat => $ValueWhat) {
			if(!is_numeric($ValueWhat)) {
				//Фильтруем все переменные для обновления
				if (get_magic_quotes_gpc()) $ArValueWhat[$NameWhat] = "'".$ValueWhat."'";
				else $ArValueWhat[$NameWhat] = "'".$this->MySqlI->real_escape_string($ValueWhat)."'";
			}
		}
		//Проверить нет ли в языковых полях указания использовать функции для их задания, это не допустимо
		if(count($ArFormatWhat)) {
			//Если есть форматированные переменные для полей добавим их в основной запрос
			$ArValueWhat = array_merge($ArFormatWhat, $ArValueWhat);
			//Получаем список полей для всех таблиц из которых идет выборка
			$ArAllNameLngField = $this->GetInfoFieldTable($Table);
			if(count($ArAllNameLngField) && is_array($ArAllNameLngField)) {
				foreach($ArAllNameLngField as $NameLngField => $ValueLngField) {
					foreach($ArFormatWhat as $FormatWhat => $ValueFormatWhat) {
						//Если ключ поля равен языковому полю, это запрещено
						if($FormatWhat==substr($NameLngField, 4) || $FormatWhat==$NameLngField) {
							$this->Error = gettext('Не удалось выполнить запрос, код:')." 351";
							WriteLog('#UPDATE '.$Table.' SET '.$StrSet.' WHERE '.$ClearWhere.'#Error: Попытка указать языковое поле '.$FormatWhat.' через функцию '.$ValueFormatWhat, 'sql_error.log');
							if($Debug) echo $this->Error.'<br />';
							return false;
						}
					}
				}
			}
			$ArAllNameLngField = NULL;
		}
		if($Debug) {	
			if($ArNameLngField) {
				echo 'Языковые поля <br />';
				EchoArray($ArNameLngField);
			}
			echo 'Обновляемые поля<br />';
			EchoArray($ArValueWhat);
		}
		//Если есть языковые параметры нужно получить текущие значения из таблицы
		if(count($ArNameLngField)) {
			//Перебор всех языковых полей последовательно с целью формирования массива SQL запросов для обновления записей 
			//в таблице перевода
			$ArSql = array();
			//Индекс в массиве доп. SQL
			$i = 0;
			foreach($ArNameLngField as $NameLngField => $ValueLngField) {
				if($Debug) echo 'Языковое поле: '.$NameLngField.'<br />';
				//Получаем уникальные значения полей для текущего языка
				$ArCurrentLngValue = $this->Select('DISTINCT '.$NameLngField, $Table, $Where, $ArValueWhere, $CurrrentLang, false);
				//Если ошибка или записей не получено
				if(!$ArCurrentLngValue) {
					if($Debug) printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
					$this->Error = gettext('Не удалось выполнить запрос, код:')." 1201";
					if($Debug) echo $this->Error.'<br />';
					return false;
				}
				if($Debug) {
					echo 'Результат запроса в таблицу перевода<br />';
					EchoArray($ArCurrentLngValue);
				}
				//Перебор этих значений и поиск в таблице перевода с формированием SQL для их обноления
				foreach($ArCurrentLngValue as $CurrentLngValue) {
					//Значение на заданном языке 
					$NameCurrentLngField = substr($NameLngField, 4);
					//Ключ перевода в таблице  
					$KeyTranslate = md5($CurrentLngValue[$NameLngField]);
					//Если значение есть и это перевод
					if($CurrentLngValue[$NameCurrentLngField] &&
					$CurrentLngValue[$NameCurrentLngField]!='_NOT_TRANSLATED_' && 
					$CurrentLngValue[$NameCurrentLngField]!='_NOT_FOUND_') {
						//Если текущее значение поля не совпадает с новым значеним, требуется обновление записи
						if($ValueLngField != $CurrentLngValue[$NameCurrentLngField]) {
							if($Debug) {
								echo 'Старое значение "'.$CurrentLngValue[$NameCurrentLngField].'" для поля "'.$NameCurrentLngField.
								'" ключ в таблице перевода '.$KeyTranslate.'<br />';
								echo 'Новое значение "'.$ValueLngField.'" для поля "'.$NameCurrentLngField.'"<br />';
							}
							//Если текущий язык отличается от базового, нужно сформировать запросы в базу 
							//Перевода и удалить это поле из основного запроса в таблицу
							if($CurrrentLang!=$DefaultLang) {
								$ArSql[$i++] = "UPDATE `TRANSLATES` SET `".$CurrrentLang."`='".$ValueLngField."', `EDIT_".$CurrrentLang."` = NOW() WHERE `KEY_TRANSLATE`='".$KeyTranslate."'";
								//Удаляем это значение из массива переменных для основного запроса в таблице
								unset($ArValueWhat[$NameLngField]);
							} else {
								//Нужно выполнить обновление ключа в таблице перевода, значение перевода на базовом языке и дату его обновления
							 	$ArSql[$i++] = "UPDATE `TRANSLATES` SET `KEY_TRANSLATE`='".md5($ValueLngField)."', `".$CurrrentLang."`='".$ValueLngField."', `EDIT_".$CurrrentLang."` = NOW() WHERE `KEY_TRANSLATE`='".$KeyTranslate."'";
							}
						} else { //Иначе нужно исключить из запроса на обновление поле 
							if($Debug) echo 'Поле не изменяется '.$NameCurrentLngField.'<br />';
							//Удаляем это значение из массива переменных
							unset($ArValueWhat[$NameLngField]);
						}
						
					} else { //Перевод не найден для этого значения, нужно добавить запись или обновить существующую
						//Вообще нет записи для этого значения
						if($CurrentLngValue[$NameCurrentLngField]=='_NOT_FOUND_') {
							if($Debug) echo 'Не найдена запись в таблице переводов для поля '.$NameCurrentLngField.'<br />';
							//Если это попытка выполнить обновление не для базового языка, то это ошибка, так как без базового нельзя
							//Определить ключ в таблице перевода
							if($CurrrentLang!=$DefaultLang) {
								$this->Error = gettext('Не удалось выполнить запрос, необходимо задать значение на основном языке. код:')." 1233";
								if($Debug) echo $this->Error.'<br />';
								return false;
							}
							//Нужно проверить нет ли такого значения в таблице перевода
							$ResultTranslate = $this->MyQuery("SELECT COUNT(*) AS COUNT_TRANSLATE FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='".md5($ValueLngField)."'");
							if($ResultTranslate[0]['COUNT_TRANSLATE']) {
								//Нужно установить новое значение для этого поля в таблице переводов 
						 		$ArSql[$i++] = "UPDATE `TRANSLATES` SET `".$CurrrentLang."`='".$ValueLngField."', `EDIT_".$CurrrentLang."` = NOW() WHERE `KEY_TRANSLATE`='".md5($ValueLngField)."'";	
							} else {
								//Нужно добавить новую запись в таблицу переводов
								$ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `".$DefaultLang."`, `EDIT_".$CurrrentLang."`) VALUES ('".md5($ValueLngField)."', '".$ValueLngField."', NOW())";
							}
							
						} else { //Запись есть но значения для языка нет 
							//Если это обновление не для основного языка, нужно удалить из основного запроса эти поля
							if($CurrrentLang!=$DefaultLang) {
								//Удаляем это значение из массива переменных для основного запроса в таблице
								unset($ArValueWhat[$NameLngField]);
							}
							if($Debug) echo 'Не найден перевод в записи перевода для поля '.$NameCurrentLngField.'<br />';
							//Нужно установить новое значение для этого поля в таблице переводов 
						 	$ArSql[$i++] = "UPDATE `TRANSLATES` SET `".$CurrrentLang."`='".$ValueLngField."', `EDIT_".$CurrrentLang."` = NOW() WHERE `KEY_TRANSLATE`='".$KeyTranslate."'";
						}
					}
				}
			}
		}
		//Если есть переменные в массиве основного запроса
		if(count($ArValueWhat)) {
			//Формируем строку для SQL обновления 
			$StrSet = '';
			foreach($ArValueWhat as $NameWhat => $ValueWhat) {
				$StrSet .= '`'.$NameWhat."`=".$ValueWhat.", ";
			}
			$StrSet = substr($StrSet, 0, strlen($StrSet)-2);
			$MainSql = "UPDATE $Table SET ".$StrSet." WHERE ".$ClearWhere;
			//Сохраняем последний запрос SQL 
			$this->LastSQL = $MainSql;
			if(count($ArSql)) array_unshift($ArSql, $MainSql); else $ArSql = array($MainSql);
		}
		if($Debug) {
			echo 'Итого обновляем в основной таблице: <br />';
			EchoArray($ArValueWhat);
			echo 'Итого обновляем в таблицах: <br />';
			EchoArray($ArSql);
		}
		//Если есть SQL запросы для выполнения
		if(count($ArSql)) {
			//Непосредственое выполнение запросов на обновление таблиц
			foreach($ArSql as $Key => $Sql) {
				//Пишем в лог если этого требуется или если это режим отладки
				if($WriteLog || $this->ArWriteLog['update'] || $Debug) WriteLog('#'.$Sql.'#', 'sql_update.log');
				$ReaultUpdate[$Key] = $this->MyQuery($Sql);
				if($ReaultUpdate[$Key]===false) {
					//Если это выполнение основго запроса, то это фатальная ошибка
					if(!$Key) {
						if($Debug) printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
						$this->Error = gettext('Не удалось выполнить запрос, код:')." 3557";
						if($Debug) echo $this->Error.'<br />';
						return false;
					} else { //не удалось обновить таблицы переводоа
						if($Debug) printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
						$this->Error = gettext('Не удалось выполнить запрос, код:')." 3540";
						if($Debug) echo $this->Error.'<br />';
					}
				}
			}
		} else {
			return 0;
		}	
		//Возвращаем кол-во измененых записей в основной странице
		return $ReaultUpdate[0];
	}
	//Проверка соответсвия полей таблицы с новыми значениями 
	private function CheckValueForTable($Table, $ArValueWhat) {
		global $Debug, $DefaultLang, $ArCodeLang;
		//Получаем список полей для всех таблиц из которых идет выборка
		$ArNameField = $this->GetInfoFieldTable($Table, '', '', '');
		//Перебор полученных полей и проверка на соответствие формата новым значениям
		if(is_array($ArNameField) && count($ArNameField)) {
			//Здаесь будут сохранены все названия полей, которые требуют перевода
			$ArNameLngField = array();
			//Перебор всех новых значений, для придания полям должного значения
			foreach($ArValueWhat as $NameWhat => $ValueWhat) {
				global $Debug;
				//Удаляем все префиксы из названий столбцов
				$NewNameWhat = str_replace("LNG_", "", $NameWhat);
				//Флаг поиска поля
				$bCorrectNameField = false;
				//Перебор всех полей с префиксом полученных по изменяемой таблице
				foreach($ArNameField as $NameField) {
					//Если найдено поле которое будем менять, можно выполнить 
					//проверку на соответвие данных формату поля, формат поля проверяется 
					//исходя из регулярного выражения в свойстве поля Comment
					if($NameWhat == $NameField['Field'] || 'LNG_'.$NameWhat == $NameField['Field']) {
						//Поле найдено, все ок
						$bCorrectNameField = true;
						//echo 'Данные о поле<br />';
						//EchoArray($NameField);
						//Если поле не может быть NULL то нужно проверить новое значение и выдать ошибку
						if($NameField['Null']=='NO' && !$NameField['Default'] && ($ValueWhat===NULL || $ValueWhat==='')) {
							$this->Error = gettext('Обязательное значение поля не указано в запросе');
							if($Debug) echo $this->Error.'<br />';
							return array(false, 'Не указано обязательное поле: '.$NameField['Field']);
						}
						//Если в комментарии к полю есть регуляное выражение, оно должно начинаться с /
						if($NameField['Comment'][0]=='/' || $NameField['Type']=='datetime' || $NameField['Type']=='date' || $NameField['Type']=='time') {
							if($NameField['Type']=='datetime' && $NameField['Comment'][0]!='/') {
								$NameField['Comment'] = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
							}
							if($NameField['Type']=='date' && $NameField['Comment'][0]!='/') {
								$NameField['Comment'] = '/\d{4}-\d{2}-\d{2}/';
							}
							if($NameField['Type']=='time' && $NameField['Comment'][0]!='/') {
								$NameField['Comment'] = '\d{2}:\d{2}:\d{2}/';
							}
							//if($Debug) echo 'Регулярное выражение: '.$NameField['Comment'].'<br />';
							$ResultCheck = @preg_match($NameField['Comment'], $ValueWhat, $Matches);
							if($Debug) if(is_array($Matches)) EchoArray($Matches);
							//Ошибка проверки скорее всего регулярное выражение с ошибками 
							if($ResultCheck===false) {
								$this->Error = gettext('Ошибка в процессе проверки валидности данных поля');
								if($Debug) echo $this->Error.'<br />';
								return array(false, 'В таблице '.$Table.' не удалось выполнить проверку поля: '.$NameField['Field'].' по регулярному выражению '.$NameField['Comment']);
							}
							//Выражение не совпадает с шаблоном, ошибка формата данных
							if($Matches[0]!=$ValueWhat) {
								$this->Error = gettext('Новое значение поля не прошло проверку на валидность');
								if($Debug) echo $this->Error.'<br />';
								return array(false, 'В таблице '.$Table.' Новое значение поля не прошло проверку на валидность '.$NameField['Field'].'='.$ValueWhat.' регулярное выражение '.$NameField['Comment']);
							}
							//if($Debug) echo 'Проверка прошла успешно!<br />';
						}
						//Выделение типа столбца из параметра 
						$TypeField = substr($NameField['Type'], 0, (($StrPos = strpos($NameField['Type'], '('))?$StrPos:strlen($NameField['Type'])));
						//Если это поле является перечислением, то можно проверить соответвует ли новое значение одному из разрешенных
						if($TypeField=='enum') {
							@preg_match("|enum\((.*)\)|i", $NameField['Type'], $ResFind);
							if($ResFind[1]) {
								$ResFind[1] = str_replace("'", "", $ResFind[1]);
								$ArEnumValues = explode(',',$ResFind[1]);
							}
							if(array_search($ValueWhat, $ArEnumValues)===false) {
								$this->Error = gettext('Новое значение поля не прошло проверку на валидность');
								if($Debug) echo $this->Error.'<br />';
								return array(false, 'В таблице '.$Table.' Новое значение поля не прошло проверку на валидность '.$NameField['Field'].'='.$ValueWhat.' значение может принимать только '.$ResFind[1]);
							}	
						}
						//Если это целое число, нужно проверить на предмет корректности
						if($TypeField=='int' || $TypeField=='tinyint' || $TypeField=='smallint' || $TypeField=='mediumint' || $TypeField=='bigint') {
							if($ValueWhat != (int) $ValueWhat) {
								$this->Error = gettext('Новое значение поля не прошло проверку на валидность');
								if($Debug) echo $this->Error.'<br />';
								return array(false, 'В таблице '.$Table.' Новое значение поля не прошло проверку на валидность '.$NameField['Field'].'='.$ValueWhat.' значение может принимать только целое цисло');
							}
						}
						//Если это дробное  число, нужно проверить на предмет корректности
						if($TypeField=='float' || $TypeField=='real' || $TypeField=='double' || $TypeField=='decimal') {
							if(!is_numeric($ValueWhat)) {
								$this->Error = gettext('Новое значение поля не прошло проверку на валидность');
								if($Debug) echo $this->Error.'<br />';
								return array(false, 'В таблице '.$Table.' Новое значение поля не прошло проверку на валидность '.$NameField['Field'].'='.$ValueWhat.' значение может принимать только число');
							}
						}
					}
					//Если это поле языковое 
					if(strpos($NameField['Field'], 'LNG_')===0) {
						//Обрезаем поле что бы получился без префикса LNG_
						$NameColumn = substr($NameField['Field'], 4);
						//Если поле найдено, нужно его дополнить префиксом LNG_
						if($NewNameWhat == $NameColumn) {
							//Добавляем в массив новое значение гарантировано с префиксом и сохраняем в отдельный массив 
							$ArNameLngField[$NameField['Field']] = $ArValueWhat[$NameField['Field']] = $ValueWhat;
							//Зануляем старое значение которое без LNG_
							unset($ArValueWhat[$NewNameWhat]);
						}
					}					
				}
				//Если поле не найдено в списке допустимых
				if(!$bCorrectNameField) {
					$this->Error = gettext('Не верно указано поле в запросе на обновление данных');
					if($Debug) echo $this->Error.'<br />';
					return array(false, 'В таблице '.$Table.' Не найдено поле '.$NameWhat.'='.$ValueWhat);	
				}
			}
			//Удаляем лишние символы в конце
			//if($NameLngField) $NameLngField = substr($NameLngField, 0, strlen($NameLngField)-2);
		} else {
			$this->Error = gettext('Не удалось выполнить запрос, код:')." 1158";
			if($Debug) echo $this->Error.'<br />';
			return array(false, 'Не удалось получить информацию о полях таблицы '.$Table);
		}
		return array($ArValueWhat, $ArNameLngField);
	}
	//Функция формирует массив необходимый для формирования форм редактирования данных для таблиц
	public function GetInfoFormTable($Table) {
		global $Debug;
		$ArResultInfo = array();
		//Получаем список полей для всех таблиц из которых идет выборка
		$ArNameField = $this->GetInfoFieldTable($Table, '', '', '');
		//EchoArray($ArNameField);
		//Перебор полученных полей и проверка на соответствие формата новым значениям
		if(is_array($ArNameField) && count($ArNameField)) {
			//Перебираем все поля этой таблицы
			foreach($ArNameField as $NameField) {
				//Нужно зачистить все языковые названия полей
				$NameField['Field'] = str_replace("LNG_", "", $NameField['Field']);
				$TypeField = $ArResultInfo[$NameField['Field']]['TYPE'] = substr($NameField['Type'], 0, (($StrPos = strpos($NameField['Type'], '('))?$StrPos:strlen($NameField['Type'])));
				@preg_match("|(.*)\((.*)\)|i", $NameField['Type'], $Found);
				$ArResultInfo[$NameField['Field']]['MAX_LEN'] = (int)$Found[2];
				//Если это перечисление 
				if($ArResultInfo[$NameField['Field']]['TYPE']=='enum') {
					@preg_match("|enum\((.*)\)|i", $NameField['Type'], $ResFind);
					if($ResFind[1]) {
						$ResFind[1] = str_replace("'", "", $ResFind[1]);
						$ArResultInfo[$NameField['Field']]['LIST_VALUES'] = explode(',',$ResFind[1]);
						if(count($ArResultInfo[$NameField['Field']]['LIST_VALUES'])<3) 
							$ArResultInfo[$NameField['Field']]['TAG']='radio';
						else $ArResultInfo[$NameField['Field']]['TAG'] = 'select';
						$ArResultInfo[$NameField['Field']]['REG'] = '/';
						foreach($ArResultInfo[$NameField['Field']]['LIST_VALUES'] as $Value) {
							$ArResultInfo[$NameField['Field']]['REG'] .= $Value.'|';
						}
						$ArResultInfo[$NameField['Field']]['REG'] = substr($ArResultInfo[$NameField['Field']]['REG'] , 0, strlen($ArResultInfo[$NameField['Field']]['REG'] )-1).'/';
					}
				}
				//Если это целое число
				if($TypeField=='int' || $TypeField=='tinyint' || $TypeField=='smallint' || $TypeField=='mediumint' || $TypeField=='bigint') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'int';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
					$ArResultInfo[$NameField['Field']]['REG'] = '/\d{1,'.$ArResultInfo[$NameField['Field']]['MAX_LEN'].'}/';
				}
				//Если это дробное  число
				if($TypeField=='float' || $TypeField=='real' || $TypeField=='double' || $TypeField=='decimal') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'float';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
				}
				//Если это текст 
				if($TypeField=='text'|| $TypeField=='tinytext' || $TypeField=='mediumtext' || $TypeField=='longtext' ||
				   $TypeField=='blob'|| $TypeField=='tinyblob' || $TypeField=='mediumblob' || $TypeField=='longblob') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'text';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'textarea';
				}
				//Если это строка 
				if($TypeField=='char' || $TypeField=='varchar') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'string';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
				}
				//Если это дата и время
				if($TypeField=='datetime') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'datetime';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
					$ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
				}
				if($TypeField=='time') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'time';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
					$ArResultInfo[$NameField['Field']]['REG'] = '/\d{2}:\d{2}:\d{2}/';
				}
				//Если это дата и время
				if($TypeField=='date') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'date';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
					$ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}-\d{2}-\d{2}/';
				}
				//Если это дата и время
				if($TypeField=='year') {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'year';
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
					$ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}/';
				}
				if(!$ArResultInfo[$NameField['Field']]['TAG']) {
					$ArResultInfo[$NameField['Field']]['TAG'] = 'text';
				}
				if(!$ArResultInfo[$NameField['Field']]['TYPE']) {
					$ArResultInfo[$NameField['Field']]['TYPE'] = 'string';
				}
				//Если задано регулярное выражение
				if($NameField['Comment'][0]=='/') {
					$ArResultInfo[$NameField['Field']]['REG'] = $NameField['Comment'][0];
				}
				//Если нельзя задавать NULL и нет значения по умолчанию и это не автоинкремент, то поле обязательное
				if($NameField['Null']=='NO' && !$NameField['Default'] && $NameField['Extra']!='auto_increment') {
					$ArResultInfo[$NameField['Field']]['NULL'] = 'no';
				} else {
					$ArResultInfo[$NameField['Field']]['NULL'] = 'yes';
				}
				if($NameField['Key']=='PRI') {
					$ArResultInfo[$NameField['Field']]['KEY'] = 'yes';
				}
				if($NameField['Extra']=='auto_increment') {
					$ArResultInfo[$NameField['Field']]['AUTO'] = 'yes';
				}
				$ArResultInfo[$NameField['Field']]['RIGHT'] = explode(',',$NameField['Privileges']);		
			}
			return $ArResultInfo;
		} else {
			$this->Error = gettext('Не удалось выполнить запрос, код:')." 1178";
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
	}
	//Функция получения информацию о всех полей таблицы
	private function GetInfoFieldTable($Table, $ArTypeField = array('varchar', 'char', 'text'), $NameField = 'LNG_%', $Key = '') {
		global $Debug;
		$ArInfoColumns = false;
		//Если указаны типы выбираемых полей
		if(is_array($ArTypeField)) {
			$AddWhere = ' WHERE (';
			foreach($ArTypeField as $TypeField) {
				$AddWhere .= " Type LIKE '".$TypeField."%' OR";
			}
			$AddWhere .= ' 0)';
		}
		//Если указано имя поля или его часть, то фильтр по имени
		if($NameField) {
			if(!$AddWhere) $AddWhere = ' WHERE';
			$AddWhere .= " AND Field LIKE '".$NameField."'";
		}
		//Если указано ключ поля, то выборку полей идем чисто ключи PRI - Первичный ключ
		if($Key) {
			if(!$AddWhere) $AddWhere = ' WHERE';
			$AddWhere .= " AND Key LIKE '".$Key."'";
		}
		//Разбор таблиц если несколько то в цикле ищем поля и добавляем в один массив 
		$ArTable = explode(',', $Table);
		if(is_array($ArTable)) {	
			foreach($ArTable as $Table) {
				$ArInfoField = $this->MyQuery("SHOW FULL COLUMNS FROM $Table".$AddWhere);
				//Если получены данные, то надо добавить их в массив
				if(is_array($ArInfoField)) {
					if(is_array($ArInfoColumns)) $ArInfoColumns = array_merge($ArInfoColumns, $ArInfoField);
					else $ArInfoColumns =  $ArInfoField;
				}
			}
		} else { //Одна таблица, можно делать прямой запрос полей
			$ArInfoField = $this->MyQuery("SHOW FULL COLUMNS FROM $Table".$AddWhere);
			//Если получены данные, то надо добавить их в массив
			if(is_array($ArInfoField)) {
				$ArInfoColumns = $ArInfoField;
			}
		}
		/*
	    [Field]=>PUBLIC_TARIF
		[Type]=>enum('yes','no')
		[Collation]=>cp1251_general_ci
		[Null]=>NO
		[Key]=>
		[Default]=>no
		[Extra]=>
		[Privileges]=>select,insert,update,references
		[Comment]=>
		*/
		//if($Debug) if(is_array($ArInfoColumns)) EchoArray($ArInfoColumns);
		return $ArInfoColumns;
	}
	//Функция подготовки запроса для использования, экранирует все переменные
	private function PrepareQuery($Template, $ArValue) {
		global $Debug;
		if(!$Template) {
			$this->Error = gettext('Не определено условие выборки из БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!is_array($ArValue)) {
			$this->Error = gettext('Не определены значения переменных для условия выборки из БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if(!$this->MySqlI) {
			$this->Error = gettext('Не установлено соединение с БД');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		//Количество необходимых значений
		$CountValue = substr_count($Template, '?');
		if($CountValue > count($ArValue)) {
			$this->Error = gettext('Не хватает переменных для запроса по шаблону');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		if($CountValue < count($ArValue)) {
			$this->Error = gettext('Избыток переменных для запроса по шаблону');
			if($Debug) echo $this->Error.'<br />';
			return false;
		}
		$Template = str_replace("%", "%%", $Template);
		$Template = str_replace("?", "%s", $Template);
		foreach($ArValue as $Key => $Value) {
			if(is_int($Value)) continue;
			if (get_magic_quotes_gpc()) $ArValue[$Key] = "'".$Value."'";
			else $ArValue[$Key] = "'".$this->MySqlI->real_escape_string($Value)."'";
		}
		array_unshift($ArValue, $Template);
		return call_user_func_array("sprintf", $ArValue);
	}
	//Функция запроса к БД
	private function MyQuery($Sql) {
		global $Debug;
		$ResultQuery = false; 
		$Index = 0;
		//if($Debug) echo $Sql.'<br />';
		$this->MySqlResult = $this->MySqlI->query($Sql);
		if($this->MySqlResult) {
			//INSERT DELETE UPDATE
			if($this->MySqlResult===true) {
				if(strpos($Sql, 'INSERT') === 0) { //Возвращаем ключ Id последнего INSERT запроса 
					$ResultQuery = $this->MySqlI->insert_id;
				} else  { //Возвращаем число записей затронутых запросом
					$ResultQuery = $this->MySqlI->affected_rows; 
				}
			} else { //SELECT, SHOW, DESCRIBE or EXPLAIN 
				//Если не получено ни одной записи то возвращаем 0
				if($this->MySqlResult->num_rows===0) return 0;
				//Иначе формируем ассоциативный массив
				while ($Row = $this->MySqlResult->fetch_assoc()) {
				   $ResultQuery[$Index++] = $Row;
				}
			} 
		}
		//Если возникла какая-то ошибка
		if($this->MySqlI->error) {
			//Если результата нет, нужно записать лог
			WriteLog('#'.$Sql.'#Error: '.$this->MySqlI->error, 'sql_error.log');
			return false;
		}
		return $ResultQuery;
	}
} 
/*
$Debug = true;

//Корень сайта 	
$DocRoot = $_SERVER['DOCUMENT_ROOT'];
//Если это не локальная версия
if(!getenv('COMSPEC')) $DocRoot.='/new/z-payment'; 

require_once($DocRoot.'/application/main/controllers/ctrl_start.php'); 

if(getenv('COMSPEC')) {
	$MyDriverSql = new DriverSql("localhost", "root", "", "zpayment_basa", "utf8");  
} else {
	$MyDriverSql = new DriverSql("localhost", "zpay", "WbmaMRE8TYrb4hD5", "zpayment_basa2");
}

/* check connection */
//Если возникла ошибка, возвращаем фальшь
/*
if(mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

*/
/*
if($Result = $MyDriverSql->Select("*", "`TYPE_OPER`", "`CODE_OPER` LIKE ?", array('PAY_ZP'))) {
	EchoArray($Result);
} else {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
}
*/
/*
if($Result = $MyDriverSql->Update("`CLIENTS`", array('DATE_ATT'=>'2011-12-12', 'UP'=>'no'), "`ID_ACCOUNT` LIKE ?", array('ZP67753453'))) {
	//EchoArray($Result);
} else {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
}
*/

/*
$Result = $MyDriverSql->Update("HELP", 
array('TITLE'=>'Новое описание элемента', 'LNG_DESC'=>'Описание подробное всего этого безобразия'), 
"`KEY_WORD`=?", array('Test45'));//, 'en_US');

if($Result===false) {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
} else {
	echo 'Обновлено '.$Result.' записей<br />';
}
*/

/*
$Result = $MyDriverSql->Insert("HELP", array('KEY_WORD'=>'Test45', 'TITLE'=>'Help for user', 'DESC'=>'This is test text'), 'en_US');

if($Result===false) {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
} else {
	echo 'Внесено '.$Result.' записей<br />';
}
*/
/*
if($Result = $MyDriverSql->Select("*", "`HELP`", "`KEY_WORD`=?", array('Test45'))) {
	EchoArray($Result);
} else {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
}
*/
/*
$Result = $MyDriverSql->Update("TYPE_OPER", 
array('NAME_OPER'=>'Z-Payment', 'DESCR_OPER'=>'Internal instant payment Z-Payment'), 
"`CODE_OPER` LIKE ?", array('PAY_ZP'), 'en_US');

if($Result===false) {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
} else {
	echo 'Обновлено '.$Result.' записей<br />';
}

if($Result = $MyDriverSql->Select("`NAME_OPER`, `DESCR_OPER`", "`TYPE_OPER`", "`CODE_OPER` LIKE ?", array('PAY_ZP'), 'ru_RU')) {
	EchoArray($Result);
} else {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
}

if($Result = $MyDriverSql->Select("`NAME_OPER`, `DESCR_OPER`", "`TYPE_OPER`", "`CODE_OPER` LIKE ?", array('PAY_ZP'), 'en_US')) {
	EchoArray($Result);
} else {
	printf("Query failed: %s %s\n", $MyDriverSql->GetSqlError(), $MyDriverSql->GetError());
}
 
*/

//printf("Charset BD: %s\n", $MyDriverSql->GetCharset());

//echo 'Запрос '.$MyDriverSql->PrepareQuery("ID_POINT=? AND ID_TRANS=?", array("121212", "NUM12' OR 1=1\\ //"));

?>