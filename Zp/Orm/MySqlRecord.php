<?php

namespace Zp\Orm;

class MySqlRecord {

    protected $Driver;
    protected $MySqlResult;
    //Кодировка 
    protected $EncodeBD = '';
    //Текущий язык контента
    protected $ContentLang = '';
    //последняя Ошибка 
    protected $Error = '';
    //Владелец запроса
    protected $OwnerQuery = '';
    //Время актуальности данных из кеша в секундах
    protected $TimeLifeCache = 0;
    //Обновлять данные в кеш?
    protected $UpdateCache = false;
    //Вести логи запросов
    protected $ArWriteLog = array('select' => false, 'insert' => true, 'update' => true, 'delete' => true);
    //SQL последнего выполненного запроса в БД 
    protected $LastSQL = '';

    //Конструктор подключение к базе данных
    public function __construct($Host, $User, $Pass, $Base, $Charset = 'UTF8', $Lang = 'ru_RU') {
        global $Debug;

        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES " . $Charset,
            \PDO::ATTR_STATEMENT_CLASS => array("\Zp\Orm\MysqlStatment")
        );

        $connection = sprintf("mysql:host=%s;dbname=%s;", $Host, $Base);

        try {
            $this->Driver = new MysqlPDO($connection, $User, $Pass, $options);
        } catch (\PDOException $e) {
            $this->Error = gettext('Не установлено соединение с БД') . ' Err: ' . $e->getMessage();
            if ($Debug)
                printf("Connect failed: %s\n", $e->getMessage());
            throw $e;
        }
        /**
         * Устанавливаем текущую кодировку  
         */
//        $this->SetCharset($Charset);
        //Устанавливаем текущий язык
        $this->SetContentLang($Lang);
    }

    /**
     * @return MysqlPDO 
     */
    public function GetDriver() {
        return $this->Driver;
    }

    //Деструктор объекта
    public function __destruct() {
        
    }

    //Получаем последнию ошибку SQL 
    public function GetSqlError() {
        return $this->Driver->error;
    }

    //Получаем последнию ошибку объекта 
    public function GetError() {
        return $this->Error;
    }

    //Получаем последнию ошибку запроса 
    public function GetLastSQL() {
        return $this->LastSQL;
    }

    //Получить Текущая кодировка
    public function GetCharset() {
        return $this->Driver->character_set_name();
    }

    //Установить Текущая кодировка
    public function SetCharset($NewCharset) {
        global $Debug;

        if (!$this->Driver->query("SET NAMES " . $NewCharset)->execute()) {
            $this->Error = gettext('Не верно указана кодировка БД');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
    }

    //Получить Текущий язык
    public function GetContentLang() {
        return $this->ContentLang;
    }

    //Установить Текущий язык
    public function SetContentLang($NewLang) {
        global $ArCodeLang, $Debug;

        if (!is_array($ArCodeLang))
            return false;

        if (array_search($NewLang, $ArCodeLang) === false) {
            $this->Error = gettext('Не верно указан текущий язык контента');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        else
            $this->ContentLang = $NewLang;
        return $this->ContentLang;
    }

    //Функция устанавляивает время актуальности кеша при получении данных время задается в секундах
    public function SetTimeLifeCache($NewTime = 0) {
        $this->TimeLifeCachee = $NewTime;
        return true;
    }

    public function GetTimeLifeCache($NewTime = 0) {
        return $this->TimeLifeCache;
    }

    //Отключает обновление кеша
    public function DisableUpdateCache() {
        $this->UpdateCache = false;
        return true;
    }

    //Включает обновление кеша
    public function EnableUpdateCache() {
        $this->UpdateCache = true;
        return true;
    }

    public function GetUpdateCache() {
        return $this->UpdateCache;
    }

    /**
     *
     * @global type $Debug
     * @global type $DefaultLang
     * @global type $ArCodeLang
     * @global type $DefaultEncoding
     * @global type $PathTempFile
     * @param type $What
     * @param type $From
     * @param type $Where
     * @param type $ArValueWhere
     * @param type $Lang
     * @param type $WriteLog
     * @return boolean|string 
     * 
     * @desc В ответ возвращает ассоциативный массив из БД, все поля с префиксом LNG_ содержат значения на базовом языке
     * Поля без префикса перевод на язык который указан в $Lang
     * @example Функция выборки из БД <br/>
     * if(false === $ResultQuery = $f->Select("`ПОЛЯ`", "`ТАБЛИЦЫ`", "`УСЛОВИЕ` = ?", array(ЗНАЧЕНИЕ), $_SESSION['Lang'])) {<br/>
     *   $this->ErrCode = '600';<br/>
     *   $this->ErrName = gettext('Ошибка SQL запроса');<br/>
     *   $this->ErrNameDebug = '';<br/>
     *   return false;<br/>
     * }
     */
    public function Select($What, $From, $Where, $ArValueWhere, $Lang = 'ru_RU', $WriteLog = false) {
        global $Debug, $DefaultLang, $ArCodeLang, $DefaultEncoding, $PathTempFile;
        if (!$What) {
            $this->Error = gettext('Не указано какие поля выбирать из таблицы');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!$From) {
            $this->Error = gettext('Не указано из каких таблиц сделать выборку');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!$Where) {
            $this->Error = gettext('Не указан шаблон условия выборки из таблицы');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!is_array($ArValueWhere) || !count($ArValueWhere)) {
            $this->Error = gettext('Не указаны переменные для условия выборки из таблицы');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        //Если нет соединения с БД то ошибка
        if (!$this->Driver) {
            $this->Error = gettext('Не установлено соединение с БД');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }

        //Выбор языка выборки

        if (!is_array($ArCodeLang) || array_search($Lang, $ArCodeLang) === false) {
            if ($this->ContentLang)
                $CurrrentLang = $this->ContentLang;
            else
                $CurrrentLang = $DefaultLang;
        } else {
            $CurrrentLang = $Lang;
        }

        //if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />';
        //Формируем условие с фильрацией переменных 

        if (!$Where)
            return false;

        $SqlSelect = 'SELECT ' . $What . ' FROM ' . $From . ' WHERE ' . $Where;

        //Получаем список полей с LNG_ для всех таблиц из которых идет выборка
        $ArNameField = $this->GetInfoFieldTable($From);

        //Если такие поля в таблицах есть, нужно внести изменения в названия полей указав LNG_ для тех полей
        if (is_array($ArNameField)) {
            //Перебор всех полей если указаны поля конкретные для выборки 
            if (trim($What) != '*') {
                //Убираем все префиксы LNG_ что бы не вышло LNG_LNG_ 
                $What = str_replace("LNG_", "", $What);
                foreach ($ArNameField as $NameField) {
                    //Обрезаем поле что бы получился без префикса LNG_
                    $NameColumn = substr($NameField['Field'], 4);
                    //Ищем это поле в запросе и меняем его на с префиксом
                    $What = preg_replace("|(.*)($NameColumn)(.*)|", "\\1LNG_\\2\\3", $What);
                }
            }
        }


        if ($this->TimeLifeCache > 0 || $this->UpdateCache) {
            //Ищем файл который содержащий кеш
            $NameCachFile = substr(sha1($SqlSelect . $Lang), 0, 14) . '.cch';
        }
        //Если есть вермя жизни кеша? пробуем получить данные из кеша	
        if ($this->TimeLifeCache > 0) {
            if (file_exists($PathTempFile . $NameCachFile)) {
                //Пробуем получить данные из кеша
                $ArData = unserialize(file_get_contents($PathTempFile . $NameCachFile));
                //Если есть дата обновления кеша
                if ($ArData['CachUpdate'] > 0) {
                    //Проверяем срок годности кеша
                    if ((time() - $ArData['CachUpdate']) <= $this->TimeLifeCache) {
                        //Если время прошло меньше чем время жизни кеша		
                        //if($Debug) echo 'Время жизни '.$this->TimeLifeCache.' Текущее время '.time().' Время записи в кеш '.$ArData['CachUpdate'].' Данные получены из кеша<br />';
                        unset($ArData['CachUpdate']);
                        return $ArData;
                    }
                }
            }
        }

        //Пишем в лог если этого требуется или если это режим отладки
        if ($WriteLog || $this->ArWriteLog['select'] || $Debug)
            WriteLog('#' . $SqlSelect . '#', 'sql_select.log');
        //Сохраняем последний запрос SQL 
        $this->LastSQL = $SqlSelect;

        //Получаем записи из БД
        $Result = $this->MyQuery($SqlSelect, $ArValueWhere);

        //Получены данные из таблицы
        if (is_array($Result)) {
            //Перебор всех полученных записей
            foreach ($Result as $Key => $Row) {
                //Поиск всех полей с префиксом LNG_
                foreach ($Row as $NameField => $Value) {
                    //Если переменная есть то проверяем текущий язык
                    //Находим все поля с префиксом языка 
                    if ($Value && strpos($NameField, 'LNG_') === 0) {
                        //Если текущий язык отличается от базового
                        //if($DefaultLang!=$CurrrentLang) {
                        /*
                          //Определение кодировки полученного результата
                          $FieldEncoding = mb_detect_encoding($Value, "Windows-1251, UTF-8");
                          //Если кодировка поля WIN1251 а сайта UTF-8 нужно это значение
                          //конвертировать в UTF-8
                          if($DefaultEncoding == 'utf-8' && $FieldEncoding=='Windows-1251') {
                          $Value = win_utf8($Value);
                          } */
                        //Формируем Хеш для поиска из значения по умолчанию
                        $IndexKey = md5($Value);
                        //Получем записи из таблицы переводов
                        $ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE` LIKE '$IndexKey' LIMIT 1");
                        if (is_array($ResultTranslate) && $ResultTranslate[0][$CurrrentLang]) {
                            //Сохраняем поле с нужным языком в переменной без префикса
                            $Result[$Key][substr($NameField, 4)] = $ResultTranslate[0][$CurrrentLang];
                            if ($Debug) {
                                //echo 'Запись с переводами: <br />';
                                //EchoArray($ResultTranslate);
                            }
                        } else {
                            //Если просто нет перевода
                            if (!is_array($ResultTranslate)) {
                                //Если это базовый язык и есть значение поля из старой версии без префикса
                                //Нужно скопироать значение этого поля в новое с префиксом и добавить в таблицу перевода
                                //Если это выборка уникальных элементов, то нельзя подставлять поле из старого значения
                                //так как это помешает функции Update выполнить добавление записей в таблицу перевода  
                                if ($DefaultLang == $CurrrentLang && $Result[$Key][$NameField] && substr($What, 0, 8) != 'DISTINCT') {
                                    $Result[$Key][substr($NameField, 4)] = $Result[$Key][$NameField];
                                } else {
                                    //Вообще нет записи для этого поля 
                                    $Result[$Key][substr($NameField, 4)] = "_NOT_FOUND_";
                                }
                            } else {
                                //Иначе присваеваем спец значение, указывающее, что перевод не найден в записи
                                $Result[$Key][substr($NameField, 4)] = "_NOT_TRANSLATED_";
                                //$Result[$Key][$NameField];// gettext("Нет перевода");
                            }
                        }
                        /* } else {
                          //Если есть префикс языка
                          if(strpos($NameField, 'LNG_')===0) {
                          //Добавляем поле без префикса и сохраняем туда значение базового языка
                          $Result[$Key][substr($NameField, 4)] = $Result[$Key][$NameField];
                          }
                          } */
                    } elseif (!$Value && strpos($NameField, 'LNG_') === 0) {
                        if ($Debug) {
                            //echo 'Значение языкового поля '.$NameField.' не найдено в таблице '.$From.' для выборки '.$Where.'<br />';
                        }
                        if ($Result[$Key][substr($NameField, 4)])
                            $Result[$Key][$NameField] = $Result[$Key][substr($NameField, 4)];
                        else
                            $Result[$Key][$NameField] = '';
                    }
                }
            }
        }
        //Если нужно обновить кеш данные 	
        if ($this->UpdateCache && is_array($Result) && count($Result)) {
            $CachData = $Result;
            $CachData['CachUpdate'] = time();
            if (!file_put_contents($PathTempFile . $NameCachFile, serialize($CachData))) {
                if ($Debug) {
                    echo 'Не удалось обновить даные к кеш файле ' . $NameCachFile . '<br />';
                }
            } else {
                //if($Debug) echo 'Данные записаны в кеш<br />';
            }
            //$ArData = unserialize(@file_get_contents($PathTempFile.$NameCachFile));
        }
        return $Result;
    }

    //Функция добавления записей в БД
    /* 	
      if(false === $f->Insert("`ТАБЛИЦА`", array('ПОЛЕ'=>'ЗНАЧЕНИЕ', 'func_ПОЛЕ' => 'ФУНКЦИЯ SQL'), $_SESSION['Lang'])) {
      $this->ErrCode = '400';
      $this->ErrName = gettext('Ошибка при добавлении новых курсов валют на '.$Date);
      $this->ErrNameDebug = '';
      return false;
      }
      $Table - Таблица в которой выполняются изменения
      $ArValueWhat - Ассоциативный массив новых значений переменых для вставки
      $Lang - Язык для текстовых полей
      $WriteLog - Флаг записи запроса в лог файл
     */
    public function Insert($Table, $ArValueWhat, $Lang = 'ru_RU', $WriteLog = true) {
        global $Debug, $DefaultLang, $ArCodeLang;
        if (!$Table) {
            $this->Error = gettext('Не указана таблица для вставки записей');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!is_array($ArValueWhat) || !count($ArValueWhat)) {
            $this->Error = gettext('Не указаны переменные для вставки записей в таблице');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }

        if (null != $ArCodeLang || !empty($ArCodeLang)) {
            //Выбор языка выборки 
            if (array_search($Lang, $ArCodeLang) === false) {
                if ($this->ContentLang)
                    $CurrrentLang = $this->ContentLang;
                else
                    $CurrrentLang = $DefaultLang;
            } else {
                $CurrrentLang = $Lang;
            }
        }
        //if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />';
        //Собираем все данные для отражения в ошибках
        $StrSet = '';
        //Массив в который сохраняем строку для форматирования новой переменной
        $ArFormatWhat = array();
        foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
            if (strpos($NameWhat, 'func_') === 0) {
                $ArFormatWhat[substr($NameWhat, 5)] = $ValueWhat;
                unset($ArValueWhat[$NameWhat]);
                continue;
            }
            $StrSet .= '`' . $NameWhat . "`='" . $ValueWhat . "', ";
        }
        if ($Debug) {
            if (count($ArFormatWhat)) {
                echo 'Массив данных для форматирования переменных:<br />';
                EchoArray($ArFormatWhat);
            }
        }

        $StrSet = substr($StrSet, 0, strlen($StrSet) - 2);

        /** @desc Проверка всех значений обновления */
        list($ArValueWhat, $ArNameLngField) = $this->CheckValueForTable($Table, $ArValueWhat);

        //Если проверка не прошла, то нужно сообщить об ошибке
        if ($ArValueWhat === false) {
            WriteLog('#INSERT INTO ' . $Table . ' SET ' . $StrSet . '#Error: ' . $ArNameLngField, 'sql_error.log');
            return false;
        }

        //Проверить нет ли в языковых полях указания использовать функции для их задания, это не допустимо
        if (count($ArFormatWhat)) {
            //Если есть форматированные переменные для полей нужно объединить массивы
            $ArValueWhat = array_merge($ArFormatWhat, $ArValueWhat);
            //Получаем список полей для всех таблиц из которых идет выборка
            $ArAllNameLngField = $this->GetInfoFieldTable($Table);
            if (count($ArAllNameLngField) && is_array($ArAllNameLngField)) {
                foreach ($ArAllNameLngField as $NameLngField => $ValueLngField) {
                    foreach ($ArFormatWhat as $FormatWhat => $ValueFormatWhat) {
                        //Если ключ поля равен языковому полю, это запрещено
                        if ($FormatWhat == substr($NameLngField, 4) || $FormatWhat == $NameLngField) {
                            $this->Error = gettext('Не удалось выполнить запрос, код:') . " 351";
                            WriteLog('#INSERT INTO ' . $Table . ' SET ' . $StrSet . '#Error: Попытка указать языковое поле ' . $FormatWhat . ' через функцию ' . $ValueFormatWhat, 'sql_error.log');
                            if ($Debug)
                                echo $this->Error . '<br />';
                            return false;
                        }
                    }
                }
            }
        }

        if ($Debug) {
            if ($ArNameLngField) {
                echo 'Языковые поля <br />';
                EchoArray($ArNameLngField);
            }
            echo 'Добавляемые поля<br />';
            EchoArray($ArValueWhat);
        }
        //Если есть языковые параметры нужно найти их в таблице перевода или вставить в таблицу перевода новые
        if (is_array($ArNameLngField)) {
            //Перебор всех языковых полей последовательно с целью формирования массива SQL запросов для обновления записей 
            //в таблице перевода
            $ArSql = array();
            //Индекс в массиве доп. SQL
            $i = 0;
            //Перебор всех языковых полей 
            foreach ($ArNameLngField as $NameLngField => $ValueLngField) {
                if ($Debug)
                    echo 'Языковое поле: ' . $NameLngField . '<br />';
                //Ищем такое значение в таблице перевода
                //Если задан базовый язык то ищем по ключу  значения
                if ($CurrrentLang == $DefaultLang) {
                    $ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='" . md5($ValueLngField) . "'");
                    //Если НЕ найдены записи нужно добавить в таблицу перевода запись  
                    if (!$ResultTranslate[0]['KEY_TRANSLATE']) {
                        //Нужно добавить новую запись в таблицу переводов
                        $ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `" . $DefaultLang . "`, `EDIT_" . $DefaultLang . "`) VALUES ('" . md5($ValueLngField) . "', '" . $ValueLngField . "', NOW())";
                    }
                } else { //Указан не базовый язык
                    $ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `" . $CurrrentLang . "` LIKE '" . $ValueLngField . "'");
                    //Если запись найдена нужно получить значение базового языка и установить ее для основного запроса
                    if ($ResultTranslate[0][$DefaultLang]) {
                        if ($Debug)
                            echo 'Найден перевод на основном языке для поля ' . $NameLngField . ' и значения ' . $ValueLngField . ' перевод ' . $ResultTranslate[0][$DefaultLang] . '<br />';
                        //Устанавливаем базовое значение для языкового поля в основной таблице
                        $ArValueWhat[$NameLngField] = "'" . $ResultTranslate[0][$DefaultLang] . "'";
                    }
                    //Если НЕ найдены записи нужно попытаться найти уже по ключу  
                    if (!$ResultTranslate[0]['KEY_TRANSLATE']) {
                        $ResultTranslate = $this->MyQuery("SELECT * FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='" . md5($ValueLngField) . "'");
                        //Если нет таких записей и по ключу 
                        if (!$ResultTranslate[0]['KEY_TRANSLATE']) {
                            //Нужно добавить новую запись в таблицу переводов, указать текст на другом языке в значении базового а так же и сам пеервод
                            $ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `" . $DefaultLang . "`, `EDIT_" . $DefaultLang . "`, `" . $CurrrentLang . "`, `EDIT_" . $CurrrentLang . "`) VALUES ('" . md5($ValueLngField) . "', '" . $ValueLngField . "', NOW(), '" . $ValueLngField . "', NOW())";
                        }
                    }
                }
            }
        }

        //Если есть переменные в массиве основного запроса
        if (count($ArValueWhat)) {
            //Формируем строку для SQL обновления 
            $StrSet = '';
            foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
                $StrSet .= '`' . $NameWhat . "`=" . $ValueWhat . ", ";
            }
            $StrSet = substr($StrSet, 0, strlen($StrSet) - 2);

            /**/
            $strFields = "`".implode("`,`", array_keys($ArValueWhat))."`";
            $strValues = trim(str_repeat("?,", count($ArValueWhat)), ",");
            $preQuery = "INSERT INTO %s(%s) VALUES(%s)";
            $MainSql = sprintf($preQuery, $Table, $strFields, $strValues);
//            $MainSql = "INSERT INTO $Table SET " . $StrSet;            
            
            $Result = $this->MyQuery($MainSql, $ArValueWhat);
            
            //Сохраняем последний запрос SQL 
            $this->LastSQL = $MainSql;
            if (count($ArSql))
                array_unshift($ArSql, $MainSql);
//            else
//                $ArSql = array($MainSql);
        }
        if ($Debug) {
            echo 'Итого добавляем в основную таблицу: <br />';
            EchoArray($ArValueWhat);
            echo 'Итого SQL запросы: <br />';
            EchoArray($ArSql);
        }

        $ResultInsert = array();

        //Если есть SQL запросы для выполнения
        if (count($ArSql)) {
            //Непосредственое выполнение запросов на обновление таблиц
            foreach ($ArSql as $Key => $Sql) {
                //Пишем в лог если этого требуется или если это режим отладки
                if ($WriteLog || $this->ArWriteLog['insert'] || $Debug)
                    WriteLog('#' . $Sql . '#', 'sql_insert.log');
                $ResultInsert[$Key] = $this->MyQuery($Sql);
                if ($ResultInsert[$Key] === false) {
                    //Если это выполнение основго запроса, то это фатальная ошибка
                    if (!$Key) {
                        if ($Debug)
                            printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
                        $this->Error = gettext('Не удалось выполнить запрос, код:') . " 3557";
                        if ($Debug)
                            echo $this->Error . '<br />';
                        return false;
                    } else {
                        /** @desc не удалось обновить таблицы переводоа */
                        if ($Debug)
                            printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
                        $this->Error = gettext('Не удалось выполнить запрос, код:') . " 3540";
                        if ($Debug)
                            echo $this->Error . '<br />';
                    }
                }
            }
        }
//        else {
//            return 0;
//        }

        /**
         * @desc Возвращаем кол-во вставленных записей в основной таблице 
         */
        return $Result;
    }

    //Функция обновления записей в БД
    /**
     *
     * @global type $Debug
     * @global type $DefaultLang
     * @global type $ArCodeLang
     * @param type $Table
     * @param type $ArValueWhat
     * @param type $Where
     * @param type $ArValueWhere
     * @param type $Lang
     * @param type $WriteLog
     * @return boolean|int 
     * 
     * if(false === $f->Update("`ТАБЛИЦА`", array("`ПОЛЕ`"=>$НОВ_ЗНАЧ), "`ПОЛЕ` = ?", array($УСЛОВИЕ), $_SESSION['Lang'])) {
     *       $this->ErrCode = '607';
     *       $this->ErrName = gettext('Ошибка SQL запроса');
     *       $this->ErrNameDebug = '';
     *       return false;
     * }
     * 
     * $Table - Таблица в которой выполняются изменения
     * $ArValueWhat - Ассоциативный массив новых значений переменых для обновления
     * $Where - Шаблон для формирования условия обновления
     * $ArValueWhere - Массив для заполнения шаблона условия обновления
     * $WriteLog - Флаг записи запроса в лог файл
     */
    public function Update($Table, $ArValueWhat, $Where, $ArValueWhere, $Lang = 'ru_RU', $WriteLog = true) {
        global $Debug, $DefaultLang, $ArCodeLang;
        if (!$Table) {
            $this->Error = gettext('Не указана таблица для обновления записей');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!is_array($ArValueWhat) || !count($ArValueWhat)) {
            $this->Error = gettext('Не указаны переменные для обновления записей в таблице');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!$Where) {
            $this->Error = gettext('Не указан шаблон условия обновления записей в таблице');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        if (!is_array($ArValueWhere) || !count($ArValueWhere)) {
            $this->Error = gettext('Не указаны переменные для условия обновления записей в таблице');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }

        /**
         * @desc Выбор языка выборки  
         */
        if (is_array($ArCodeLang) && array_search($Lang, $ArCodeLang) === false) {
            if ($this->ContentLang)
                $CurrrentLang = $this->ContentLang;
            else
                $CurrrentLang = $DefaultLang;
        } else {
            $CurrrentLang = $Lang;
        }

        /** if($Debug) echo 'Текущий язык '.$CurrrentLang.'<br />'; */
        /** @desc Формируем условие WHERE с фильрацией переменных  */
        $ClearWhere = $this->PrepareQuery($Where, $ArValueWhere);
        if (!$ClearWhere)
            return false;
        //Собираем все данные для отражения в ошибках
        $StrSet = '';
        //Массив в который сохраняем строку для форматирования новой переменной
        $ArFormatWhat = array();
        foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
            //Если это поле задается функцией SQL 
            if (strpos($NameWhat, 'func_') === 0) {
                $ArFormatWhat[substr($NameWhat, 5)] = $ValueWhat;
                unset($ArValueWhat[$NameWhat]);
                continue;
            }
            $StrSet .= '`' . $NameWhat . "`='" . $ValueWhat . "', ";
        }


        if ($Debug) {
            if (count($ArFormatWhat)) {
                echo 'Массив данных для форматирования переменных:<br />';
                EchoArray($ArFormatWhat);
            }
        }
        $StrSet = substr($StrSet, 0, strlen($StrSet) - 2);
        //Проверка всех значений обновления 
        list($ArValueWhat, $ArNameLngField) = $this->CheckValueForTable($Table, $ArValueWhat);
        //Если проверка не прошла, то нужно сообщить об ошибке
        if ($ArValueWhat === false) {
            WriteLog('#UPDATE ' . $Table . ' SET ' . $StrSet . ' WHERE ' . $ClearWhere . '#Error: ' . $ArNameLngField, 'sql_error.log');
            return false;
        }

        //Зачистка новых переменных от кавычек и прочего не желательного
        /** @todo DELETE BLOCK */
//        foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
//            if (!is_numeric($ValueWhat)) {
//                //Фильтруем все переменные для обновления
//                if (get_magic_quotes_gpc())
//                    $ArValueWhat[$NameWhat] = "'" . $ValueWhat . "'";
//                else
//                    $ArValueWhat[$NameWhat] = "'" . $this->Driver->real_escape_string($ValueWhat) . "'";
//            }
//        }
        //Проверить нет ли в языковых полях указания использовать функции для их задания, это не допустимо
        if (count($ArFormatWhat)) {
            //Если есть форматированные переменные для полей добавим их в основной запрос
            $ArValueWhat = array_merge($ArFormatWhat, $ArValueWhat);
            //Получаем список полей для всех таблиц из которых идет выборка
            $ArAllNameLngField = $this->GetInfoFieldTable($Table);
            if (count($ArAllNameLngField) && is_array($ArAllNameLngField)) {
                foreach ($ArAllNameLngField as $NameLngField => $ValueLngField) {
                    foreach ($ArFormatWhat as $FormatWhat => $ValueFormatWhat) {
                        //Если ключ поля равен языковому полю, это запрещено
                        if ($FormatWhat == substr($NameLngField, 4) || $FormatWhat == $NameLngField) {
                            $this->Error = gettext('Не удалось выполнить запрос, код:') . " 351";
                            WriteLog('#UPDATE ' . $Table . ' SET ' . $StrSet . ' WHERE ' . $ClearWhere . '#Error: Попытка указать языковое поле ' . $FormatWhat . ' через функцию ' . $ValueFormatWhat, 'sql_error.log');
                            if ($Debug)
                                echo $this->Error . '<br />';
                            return false;
                        }
                    }
                }
            }
            $ArAllNameLngField = NULL;
        }
        if ($Debug) {
            if ($ArNameLngField) {
                echo 'Языковые поля <br />';
                EchoArray($ArNameLngField);
            }
            echo 'Обновляемые поля<br />';
            EchoArray($ArValueWhat);
        }
        //Если есть языковые параметры нужно получить текущие значения из таблицы
        if (count($ArNameLngField)) {
            //Перебор всех языковых полей последовательно с целью формирования массива SQL запросов для обновления записей 
            //в таблице перевода
            $ArSql = array();
            //Индекс в массиве доп. SQL
            $i = 0;
            foreach ($ArNameLngField as $NameLngField => $ValueLngField) {
                if ($Debug)
                    echo 'Языковое поле: ' . $NameLngField . '<br />';
                //Получаем уникальные значения полей для текущего языка
                $ArCurrentLngValue = $this->Select('DISTINCT ' . $NameLngField, $Table, $Where, $ArValueWhere, $CurrrentLang, false);
                //Если ошибка или записей не получено
                if (!$ArCurrentLngValue) {
                    if ($Debug)
                        printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
                    $this->Error = gettext('Не удалось выполнить запрос, код:') . " 1201";
                    if ($Debug)
                        echo $this->Error . '<br />';
                    return false;
                }
                if ($Debug) {
                    echo 'Результат запроса в таблицу перевода<br />';
                    EchoArray($ArCurrentLngValue);
                }
                //Перебор этих значений и поиск в таблице перевода с формированием SQL для их обноления
                foreach ($ArCurrentLngValue as $CurrentLngValue) {
                    //Значение на заданном языке 
                    $NameCurrentLngField = substr($NameLngField, 4);
                    //Ключ перевода в таблице  
                    $KeyTranslate = md5($CurrentLngValue[$NameLngField]);
                    //Если значение есть и это перевод
                    if ($CurrentLngValue[$NameCurrentLngField] &&
                            $CurrentLngValue[$NameCurrentLngField] != '_NOT_TRANSLATED_' &&
                            $CurrentLngValue[$NameCurrentLngField] != '_NOT_FOUND_') {
                        //Если текущее значение поля не совпадает с новым значеним, требуется обновление записи
                        if ($ValueLngField != $CurrentLngValue[$NameCurrentLngField]) {
                            if ($Debug) {
                                echo 'Старое значение "' . $CurrentLngValue[$NameCurrentLngField] . '" для поля "' . $NameCurrentLngField .
                                '" ключ в таблице перевода ' . $KeyTranslate . '<br />';
                                echo 'Новое значение "' . $ValueLngField . '" для поля "' . $NameCurrentLngField . '"<br />';
                            }
                            //Если текущий язык отличается от базового, нужно сформировать запросы в базу 
                            //Перевода и удалить это поле из основного запроса в таблицу
                            if ($CurrrentLang != $DefaultLang) {
                                $ArSql[$i++] = "UPDATE `TRANSLATES` SET `" . $CurrrentLang . "`='" . $ValueLngField . "', `EDIT_" . $CurrrentLang . "` = NOW() WHERE `KEY_TRANSLATE`='" . $KeyTranslate . "'";
                                //Удаляем это значение из массива переменных для основного запроса в таблице
                                unset($ArValueWhat[$NameLngField]);
                            } else {
                                //Нужно выполнить обновление ключа в таблице перевода, значение перевода на базовом языке и дату его обновления
                                $ArSql[$i++] = "UPDATE `TRANSLATES` SET `KEY_TRANSLATE`='" . md5($ValueLngField) . "', `" . $CurrrentLang . "`='" . $ValueLngField . "', `EDIT_" . $CurrrentLang . "` = NOW() WHERE `KEY_TRANSLATE`='" . $KeyTranslate . "'";
                            }
                        } else { //Иначе нужно исключить из запроса на обновление поле 
                            if ($Debug)
                                echo 'Поле не изменяется ' . $NameCurrentLngField . '<br />';
                            //Удаляем это значение из массива переменных
                            unset($ArValueWhat[$NameLngField]);
                        }
                    } else { //Перевод не найден для этого значения, нужно добавить запись или обновить существующую
                        //Вообще нет записи для этого значения
                        if ($CurrentLngValue[$NameCurrentLngField] == '_NOT_FOUND_') {
                            if ($Debug)
                                echo 'Не найдена запись в таблице переводов для поля ' . $NameCurrentLngField . '<br />';
                            //Если это попытка выполнить обновление не для базового языка, то это ошибка, так как без базового нельзя
                            //Определить ключ в таблице перевода
                            if ($CurrrentLang != $DefaultLang) {
                                $this->Error = gettext('Не удалось выполнить запрос, необходимо задать значение на основном языке. код:') . " 1233";
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return false;
                            }
                            //Нужно проверить нет ли такого значения в таблице перевода
                            $ResultTranslate = $this->MyQuery("SELECT COUNT(*) AS COUNT_TRANSLATE FROM `TRANSLATES` WHERE `KEY_TRANSLATE`='" . md5($ValueLngField) . "'");
                            if ($ResultTranslate[0]['COUNT_TRANSLATE']) {
                                //Нужно установить новое значение для этого поля в таблице переводов 
                                $ArSql[$i++] = "UPDATE `TRANSLATES` SET `" . $CurrrentLang . "`='" . $ValueLngField . "', `EDIT_" . $CurrrentLang . "` = NOW() WHERE `KEY_TRANSLATE`='" . md5($ValueLngField) . "'";
                            } else {
                                //Нужно добавить новую запись в таблицу переводов
                                $ArSql[$i++] = "INSERT INTO `TRANSLATES` (`KEY_TRANSLATE`, `" . $DefaultLang . "`, `EDIT_" . $CurrrentLang . "`) VALUES ('" . md5($ValueLngField) . "', '" . $ValueLngField . "', NOW())";
                            }
                        } else { //Запись есть но значения для языка нет 
                            //Если это обновление не для основного языка, нужно удалить из основного запроса эти поля
                            if ($CurrrentLang != $DefaultLang) {
                                //Удаляем это значение из массива переменных для основного запроса в таблице
                                unset($ArValueWhat[$NameLngField]);
                            }
                            if ($Debug)
                                echo 'Не найден перевод в записи перевода для поля ' . $NameCurrentLngField . '<br />';
                            //Нужно установить новое значение для этого поля в таблице переводов 
                            $ArSql[$i++] = "UPDATE `TRANSLATES` SET `" . $CurrrentLang . "`='" . $ValueLngField . "', `EDIT_" . $CurrrentLang . "` = NOW() WHERE `KEY_TRANSLATE`='" . $KeyTranslate . "'";
                        }
                    }
                }
            }
        }
        //Если есть переменные в массиве основного запроса
        if (count($ArValueWhat)) {
            //Формируем строку для SQL обновления 
            $StrSet = '';
            foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
                $StrSet .= '`' . $NameWhat . "`=" . $ValueWhat . ", ";
            }
            $StrSet = substr($StrSet, 0, strlen($StrSet) - 2);

            /*
             * $Table - Таблица в которой выполняются изменения
             * $ArValueWhat - Ассоциативный массив новых значений переменых для обновления
             * $Where - Шаблон для формирования условия обновления
             * $ArValueWhere - Массив для заполнения шаблона условия обновления
             * $WriteLog - Флаг записи запроса в лог файл
             */

//            $MainSql = "UPDATE $Table SET " . $StrSet . " WHERE " . $ClearWhere;


            $fields = trim(implode(" = ?,", array_keys($ArValueWhat)));
            $fields .= " = ?";
            $params = array_merge($ArValueWhat, $ArValueWhere);



            $MainSql = sprintf("UPDATE `%s` SET %s WHERE %s", $Table, $fields, $Where);
//            $MainSql = "UPDATE $Table SET  WHERE " . $ClearWhere;

            $params = array_values($params);

            $Result = $this->MyQuery($MainSql, $params);

            var_dump($Result);
            die();

            //Сохраняем последний запрос SQL 
            $this->LastSQL = $MainSql;

            if (isset($ArSql) && count($ArSql) > 0)
                array_unshift($ArSql, $MainSql);
            else
                $ArSql = array($MainSql);
        }

        if ($Debug) {
            echo 'Итого обновляем в основной таблице: <br />';
            EchoArray($ArValueWhat);
            echo 'Итого обновляем в таблицах: <br />';
            EchoArray($ArSql);
        }

        /** @desc Если есть SQL запросы для выполнения */
        if (count($ArSql)) {
            //Непосредственое выполнение запросов на обновление таблиц
            foreach ($ArSql as $Key => $Sql) {
                //Пишем в лог если этого требуется или если это режим отладки
                if ($WriteLog || $this->ArWriteLog['update'] || $Debug)
                    WriteLog('#' . $Sql . '#', 'sql_update.log');
                $ReaultUpdate[$Key] = $this->MyQuery($Sql);
                if ($ReaultUpdate[$Key] === false) {
                    //Если это выполнение основго запроса, то это фатальная ошибка
                    if (!$Key) {
                        if ($Debug)
                            printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
                        $this->Error = gettext('Не удалось выполнить запрос, код:') . " 3557";
                        if ($Debug)
                            echo $this->Error . '<br />';
                        return false;
                    } else { //не удалось обновить таблицы переводоа
                        if ($Debug)
                            printf("Query failed: %s %s\n", $this->GetSqlError(), $this->GetError());
                        $this->Error = gettext('Не удалось выполнить запрос, код:') . " 3540";
                        if ($Debug)
                            echo $this->Error . '<br />';
                    }
                }
            }
        }

        //Возвращаем кол-во измененых записей в основной странице
        return $Reault;
    }

    //Проверка соответсвия полей таблицы с новыми значениями 
    private function CheckValueForTable($Table, $ArValueWhat) {
        global $Debug, $DefaultLang, $ArCodeLang;
        //Получаем список полей для всех таблиц из которых идет выборка
        $ArNameField = $this->GetInfoFieldTable($Table, '', '', '');

        //Перебор полученных полей и проверка на соответствие формата новым значениям
        if (is_array($ArNameField) && count($ArNameField)) {
            //Здаесь будут сохранены все названия полей, которые требуют перевода
            $ArNameLngField = array();
            //Перебор всех новых значений, для придания полям должного значения
            foreach ($ArValueWhat as $NameWhat => $ValueWhat) {
                global $Debug;
                //Удаляем все префиксы из названий столбцов
                $NewNameWhat = str_replace("LNG_", "", $NameWhat);
                //Флаг поиска поля
                $bCorrectNameField = false;
                //Перебор всех полей с префиксом полученных по изменяемой таблице
                foreach ($ArNameField as $NameField) {
                    //Если найдено поле которое будем менять, можно выполнить 
                    //проверку на соответвие данных формату поля, формат поля проверяется 
                    //исходя из регулярного выражения в свойстве поля Comment
                    if ($NameWhat == $NameField['Field'] || 'LNG_' . $NameWhat == $NameField['Field']) {
                        //Поле найдено, все ок
                        $bCorrectNameField = true;
                        //echo 'Данные о поле<br />';
                        //EchoArray($NameField);
                        //Если поле не может быть NULL то нужно проверить новое значение и выдать ошибку
                        if ($NameField['Null'] == 'NO' && !$NameField['Default'] && ($ValueWhat === NULL || $ValueWhat === '')) {
                            $this->Error = gettext('Обязательное значение поля не указано в запросе');
                            if ($Debug)
                                echo $this->Error . '<br />';
                            return array(false, 'Не указано обязательное поле: ' . $NameField['Field']);
                        }
                        //Если в комментарии к полю есть регуляное выражение, оно должно начинаться с /

                        if ($NameField['Comment'] == '/' || $NameField['Type'] == 'datetime' || $NameField['Type'] == 'date' || $NameField['Type'] == 'time') {
                            if ($NameField['Type'] == 'datetime' && $NameField['Comment'] != '/') {
                                $NameField['Comment'] = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
                            }
                            if ($NameField['Type'] == 'date' && $NameField['Comment'] != '/') {
                                $NameField['Comment'] = '/\d{4}-\d{2}-\d{2}/';
                            }
                            if ($NameField['Type'] == 'time' && $NameField['Comment'] != '/') {
                                $NameField['Comment'] = '\d{2}:\d{2}:\d{2}/';
                            }
                            //if($Debug) echo 'Регулярное выражение: '.$NameField['Comment'].'<br />';
                            $ResultCheck = @preg_match($NameField['Comment'], $ValueWhat, $Matches);
                            if ($Debug)
                                if (is_array($Matches))
                                    EchoArray($Matches);
                            //Ошибка проверки скорее всего регулярное выражение с ошибками 
                            if ($ResultCheck === false) {
                                $this->Error = gettext('Ошибка в процессе проверки валидности данных поля');
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return array(false, 'В таблице ' . $Table . ' не удалось выполнить проверку поля: ' . $NameField['Field'] . ' по регулярному выражению ' . $NameField['Comment']);
                            }
                            //Выражение не совпадает с шаблоном, ошибка формата данных
                            if ($Matches[0] != $ValueWhat) {
                                $this->Error = gettext('Новое значение поля не прошло проверку на валидность');
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return array(false, 'В таблице ' . $Table . ' Новое значение поля не прошло проверку на валидность ' . $NameField['Field'] . '=' . $ValueWhat . ' регулярное выражение ' . $NameField['Comment']);
                            }
                            //if($Debug) echo 'Проверка прошла успешно!<br />';
                        }


                        //Выделение типа столбца из параметра 
                        $TypeField = substr($NameField['Type'], 0, (($StrPos = strpos($NameField['Type'], '(')) ? $StrPos : strlen($NameField['Type'])));
                        //Если это поле является перечислением, то можно проверить соответвует ли новое значение одному из разрешенных
                        if ($TypeField == 'enum') {
                            @preg_match("|enum\((.*)\)|i", $NameField['Type'], $ResFind);
                            if ($ResFind[1]) {
                                $ResFind[1] = str_replace("'", "", $ResFind[1]);
                                $ArEnumValues = explode(',', $ResFind[1]);
                            }
                            if (array_search($ValueWhat, $ArEnumValues) === false) {
                                $this->Error = gettext('Новое значение поля не прошло проверку на валидность');
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return array(false, 'В таблице ' . $Table . ' Новое значение поля не прошло проверку на валидность ' . $NameField['Field'] . '=' . $ValueWhat . ' значение может принимать только ' . $ResFind[1]);
                            }
                        }
                        //Если это целое число, нужно проверить на предмет корректности
                        if ($TypeField == 'int' || $TypeField == 'tinyint' || $TypeField == 'smallint' || $TypeField == 'mediumint' || $TypeField == 'bigint') {
                            if ($ValueWhat != (int) $ValueWhat) {
                                $this->Error = gettext('Новое значение поля не прошло проверку на валидность');
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return array(false, 'В таблице ' . $Table . ' Новое значение поля не прошло проверку на валидность ' . $NameField['Field'] . '=' . $ValueWhat . ' значение может принимать только целое цисло');
                            }
                        }
                        //Если это дробное  число, нужно проверить на предмет корректности
                        if ($TypeField == 'float' || $TypeField == 'real' || $TypeField == 'double' || $TypeField == 'decimal') {
                            if (!is_numeric($ValueWhat)) {
                                $this->Error = gettext('Новое значение поля не прошло проверку на валидность');
                                if ($Debug)
                                    echo $this->Error . '<br />';
                                return array(false, 'В таблице ' . $Table . ' Новое значение поля не прошло проверку на валидность ' . $NameField['Field'] . '=' . $ValueWhat . ' значение может принимать только число');
                            }
                        }
                    }
                    //Если это поле языковое 
                    if (strpos($NameField['Field'], 'LNG_') === 0) {
                        //Обрезаем поле что бы получился без префикса LNG_
                        $NameColumn = substr($NameField['Field'], 4);
                        //Если поле найдено, нужно его дополнить префиксом LNG_
                        if ($NewNameWhat == $NameColumn) {
                            //Добавляем в массив новое значение гарантировано с префиксом и сохраняем в отдельный массив 
                            $ArNameLngField[$NameField['Field']] = $ArValueWhat[$NameField['Field']] = $ValueWhat;
                            //Зануляем старое значение которое без LNG_
                            unset($ArValueWhat[$NewNameWhat]);
                        }
                    }
                }
                //Если поле не найдено в списке допустимых
                if (!$bCorrectNameField) {
                    $this->Error = gettext('Не верно указано поле в запросе на обновление данных');
                    if ($Debug)
                        echo $this->Error . '<br />';
                    return array(false, 'В таблице ' . $Table . ' Не найдено поле ' . $NameWhat . '=' . $ValueWhat);
                }
            }
            //Удаляем лишние символы в конце
            //if($NameLngField) $NameLngField = substr($NameLngField, 0, strlen($NameLngField)-2);
        } else {
            $this->Error = gettext('Не удалось выполнить запрос, код:') . " 1158";
            if ($Debug)
                echo $this->Error . '<br />';
            return array(false, 'Не удалось получить информацию о полях таблицы ' . $Table);
        }
        return array($ArValueWhat, $ArNameLngField);
    }

    //Функция формирует массив необходимый для формирования форм редактирования данных для таблиц
    public function GetInfoFormTable($Table) {
        global $Debug;
        $ArResultInfo = array();
        //Получаем список полей для всех таблиц из которых идет выборка
        $ArNameField = $this->GetInfoFieldTable($Table, '', '', '');
        //EchoArray($ArNameField);
        //Перебор полученных полей и проверка на соответствие формата новым значениям
        if (is_array($ArNameField) && count($ArNameField)) {
            //Перебираем все поля этой таблицы
            foreach ($ArNameField as $NameField) {
                //Нужно зачистить все языковые названия полей
                $NameField['Field'] = str_replace("LNG_", "", $NameField['Field']);
                $TypeField = $ArResultInfo[$NameField['Field']]['TYPE'] = substr($NameField['Type'], 0, (($StrPos = strpos($NameField['Type'], '(')) ? $StrPos : strlen($NameField['Type'])));
                @preg_match("|(.*)\((.*)\)|i", $NameField['Type'], $Found);
                $ArResultInfo[$NameField['Field']]['MAX_LEN'] = (int) $Found[2];
                //Если это перечисление 
                if ($ArResultInfo[$NameField['Field']]['TYPE'] == 'enum') {
                    @preg_match("|enum\((.*)\)|i", $NameField['Type'], $ResFind);
                    if ($ResFind[1]) {
                        $ResFind[1] = str_replace("'", "", $ResFind[1]);
                        $ArResultInfo[$NameField['Field']]['LIST_VALUES'] = explode(',', $ResFind[1]);
                        if (count($ArResultInfo[$NameField['Field']]['LIST_VALUES']) < 3)
                            $ArResultInfo[$NameField['Field']]['TAG'] = 'radio';
                        else
                            $ArResultInfo[$NameField['Field']]['TAG'] = 'select';
                        $ArResultInfo[$NameField['Field']]['REG'] = '/';
                        foreach ($ArResultInfo[$NameField['Field']]['LIST_VALUES'] as $Value) {
                            $ArResultInfo[$NameField['Field']]['REG'] .= $Value . '|';
                        }
                        $ArResultInfo[$NameField['Field']]['REG'] = substr($ArResultInfo[$NameField['Field']]['REG'], 0, strlen($ArResultInfo[$NameField['Field']]['REG']) - 1) . '/';
                    }
                }
                //Если это целое число
                if ($TypeField == 'int' || $TypeField == 'tinyint' || $TypeField == 'smallint' || $TypeField == 'mediumint' || $TypeField == 'bigint') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'int';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                    $ArResultInfo[$NameField['Field']]['REG'] = '/\d{1,' . $ArResultInfo[$NameField['Field']]['MAX_LEN'] . '}/';
                }
                //Если это дробное  число
                if ($TypeField == 'float' || $TypeField == 'real' || $TypeField == 'double' || $TypeField == 'decimal') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'float';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                }
                //Если это текст 
                if ($TypeField == 'text' || $TypeField == 'tinytext' || $TypeField == 'mediumtext' || $TypeField == 'longtext' ||
                        $TypeField == 'blob' || $TypeField == 'tinyblob' || $TypeField == 'mediumblob' || $TypeField == 'longblob') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'text';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'textarea';
                }
                //Если это строка 
                if ($TypeField == 'char' || $TypeField == 'varchar') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'string';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                }
                //Если это дата и время
                if ($TypeField == 'datetime') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'datetime';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                    $ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/';
                }
                if ($TypeField == 'time') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'time';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                    $ArResultInfo[$NameField['Field']]['REG'] = '/\d{2}:\d{2}:\d{2}/';
                }
                //Если это дата и время
                if ($TypeField == 'date') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'date';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                    $ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}-\d{2}-\d{2}/';
                }
                //Если это дата и время
                if ($TypeField == 'year') {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'year';
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                    $ArResultInfo[$NameField['Field']]['REG'] = '/\d{4}/';
                }
                if (!$ArResultInfo[$NameField['Field']]['TAG']) {
                    $ArResultInfo[$NameField['Field']]['TAG'] = 'text';
                }
                if (!$ArResultInfo[$NameField['Field']]['TYPE']) {
                    $ArResultInfo[$NameField['Field']]['TYPE'] = 'string';
                }
                //Если задано регулярное выражение
                if ($NameField['Comment'][0] == '/') {
                    $ArResultInfo[$NameField['Field']]['REG'] = $NameField['Comment'][0];
                }
                //Если нельзя задавать NULL и нет значения по умолчанию и это не автоинкремент, то поле обязательное
                if ($NameField['Null'] == 'NO' && !$NameField['Default'] && $NameField['Extra'] != 'auto_increment') {
                    $ArResultInfo[$NameField['Field']]['NULL'] = 'no';
                } else {
                    $ArResultInfo[$NameField['Field']]['NULL'] = 'yes';
                }
                if ($NameField['Key'] == 'PRI') {
                    $ArResultInfo[$NameField['Field']]['KEY'] = 'yes';
                }
                if ($NameField['Extra'] == 'auto_increment') {
                    $ArResultInfo[$NameField['Field']]['AUTO'] = 'yes';
                }
                $ArResultInfo[$NameField['Field']]['RIGHT'] = explode(',', $NameField['Privileges']);
            }
            return $ArResultInfo;
        } else {
            $this->Error = gettext('Не удалось выполнить запрос, код:') . " 1178";
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
    }

    //Функция получения информацию о всех полей таблицы
    private function GetInfoFieldTable($Table, $ArTypeField = array('varchar', 'char', 'text'), $NameField = 'LNG_%', $Key = '') {
        global $Debug;
        $ArInfoColumns = false;
        //Если указаны типы выбираемых полей
        $AddWhere = "";
        if (is_array($ArTypeField)) {
            $AddWhere = ' WHERE (';
            foreach ($ArTypeField as $TypeField) {
                $AddWhere .= " Type LIKE '" . $TypeField . "%' OR";
            }
            $AddWhere .= ' 0)';
        }
        //Если указано имя поля или его часть, то фильтр по имени
        if ($NameField) {
            if (!$AddWhere)
                $AddWhere = ' WHERE';
            $AddWhere .= " AND Field LIKE '" . $NameField . "'";
        }
        //Если указано ключ поля, то выборку полей идем чисто ключи PRI - Первичный ключ
        if ($Key) {
            if (!$AddWhere)
                $AddWhere = ' WHERE';
            $AddWhere .= " AND Key LIKE '" . $Key . "'";
        }
        //Разбор таблиц если несколько то в цикле ищем поля и добавляем в один массив 
        $ArTable = explode(',', $Table);

        if (is_array($ArTable)) {
            foreach ($ArTable as $Table) {
                $ArInfoField = $this->MyQuery("SHOW FULL COLUMNS FROM $Table" . $AddWhere);
                if (is_array($ArInfoField)) {
                    if (is_array($ArInfoColumns))
                        $ArInfoColumns = array_merge($ArInfoColumns, $ArInfoField);
                    else
                        $ArInfoColumns = $ArInfoField;
                }
            }
        } else { //Одна таблица, можно делать прямой запрос полей
            $ArInfoField = $this->MyQuery("SHOW FULL COLUMNS FROM $Table" . $AddWhere);
            //Если получены данные, то надо добавить их в массив
            if (is_array($ArInfoField)) {
                $ArInfoColumns = $ArInfoField;
            }
        }
        /*
          [Field]=>PUBLIC_TARIF
          [Type]=>enum('yes','no')
          [Collation]=>cp1251_general_ci
          [Null]=>NO
          [Key]=>
          [Default]=>no
          [Extra]=>
          [Privileges]=>select,insert,update,references
          [Comment]=>
         */
        //if($Debug) if(is_array($ArInfoColumns)) EchoArray($ArInfoColumns);
        return $ArInfoColumns;
    }

    //Функция подготовки запроса для использования, экранирует все переменные
    private function PrepareQuery($Template) {
        global $Debug;
        if (!$Template) {
            $this->Error = gettext('Не определено условие выборки из БД');
            if ($Debug)
                echo $this->Error . '<br />';
            return false;
        }
        $stmt = $this->Driver->prepare($Template);
        return $stmt;
    }

    /*     * *
     * 
      $stmt = $this->PrepareQuery($SqlSelect);

      $index = 1;
      foreach ($ArValueWhere as $value) {
      $stmt->bindParam($index, $value);
      ++$index;
      }
      if (false === $stmt->execute())
      throw new \PDOException();

      try {
      $result = $stmt->fetchAll();
      } catch (\PDOException $e) {
      /** @todo PDOException */
    /* }
     */

    //Функция запроса к БД
    private function MyQuery($Sql, array $params = null) {
        global $Debug;
        $ResultQuery = false;
        try {
            $this->Driver->beginTransaction();
            if (empty($params) && null == $params) {
                $stmt = $this->Driver->query($Sql);
            } else {
                $stmt = $this->Driver->prepare($Sql);
            }

            if (false === $stmt)
                throw new \PDOException();

            $crudOperation = substr($Sql, 0, strpos($Sql, " "));
            $ResultQuery = $this->CallCRUDOperation($crudOperation, $stmt, $params);
            $this->Driver->commit();
        } catch (\PDOException $e) {
            $this->Driver->rollBack();
            /**
             * @desc Если возникла какая-то ошибка 
             * Если результата нет, нужно записать лог
             */
            WriteLog('#' . $Sql . '#Error: ' . $this->Driver->errorInfo(), 'sql_error.log');
            return false;
        }

        return $ResultQuery;
    }

    private function CallCRUDOperation($sqlCommand, \PDOStatement $stmt, $params) {
        switch (strtolower($sqlCommand)) {
            case "insert": $methodName = "Create";
                break;
            case "update": $methodName = "Update";
                break;
            case "delete": $methodName = "Delete";
                break;
            case "select":
            default :
                $methodName = "Read";
//                new \Exception();
        }
        $methodName .= "Process";
        return call_user_func(array($this, $methodName), $stmt, $params);
    }

    private function CreateProcess(\PDOStatement $stmt, $params) {
        $index = 1;
        foreach ($params as $param)
            $stmt->bindParam($index++, $param);
        $stmt->execute();
        return $this->Driver->lastInsertId();
    }

    private function ReadProcess(\PDOStatement $stmt) {
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    private function UpdateProcess(\PDOStatement $stmt, $params) {
        if (false === ($execute = $stmt->execute($params)))
            throw new \PDOException();
        return $stmt->rowCount();
    }

    private function DeleteProcess(\PDOStatement $stmt) {
        
        return $stmt->rowCount();
    }

}