<?php

/**
 * Description of RepositoryBase
 *
 * @author shotonoff
 */

namespace Zp\Repository;

class Base
{

    static protected $recordContext;

//    static public function SetRecordContext(\Zp\Orm\MySqlRecord $context) {
    static public function SetRecordContext($context)
    {
        self::$recordContext = $context;
    }

    /**
     * @return type
     */
    static public function GetRecordContext()
    {
        return self::$recordContext;
    }

    /**
     *
     * @return \Zp\Orm\MySqlRecord
     */
    public function GetContext()
    {
        return self::$recordContext;
    }

    public function FindById($entity, $id)
    {
        $QueryBuilder = $this->GetContext()->createQueryBuilder();
        $q = $QueryBuilder->select('o');
        if (is_string($entity)) {
            $q->from($entity, 'o');
        } elseif (is_object($entity)) {
            $q->from(get_class($entity), 'o');
        }
        $s = $q->Where($QueryBuilder->expr()->andx($QueryBuilder->expr()->eq('o.id', '?1')))
            ->setParameter(1, $id)
            ->getQuery();
        return $s->getOneOrNullResult();
    }

    public function Update($obl)
    {
        $this->GetContext()->merge($obl);
        $this->GetContext()->flush();
    }

}