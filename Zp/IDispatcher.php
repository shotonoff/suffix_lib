<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Zp;
use \Zp\Http\IRequest,
    \Zp\Http\IResponse;

interface IDispatcher {

    public function Dispatch(IRequest $request = null, IResponse $response = null);
    
}