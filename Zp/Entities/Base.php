<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\Entities;

class Base {
    
    public function GetTable() {
        
        $reflection = new \ReflectionClass(get_class($this));
        $flag = preg_match("/\@Table\((\w+)\)/", $reflection->getDocComment(), $matches);
        return $matches[1];
    }

    public function GetColumnArr() {
        $reflection = new \ReflectionClass(get_class($this));
        $properties = $reflection->getProperties();
        
        $out = array();
        
        foreach($properties as $property) {
            $flag = preg_match("/\@Column\(([a-z\-]+)\)/", $property->getDocComment(), $matches);
            if(false == $flag) {
                if(null != $this->{$property->name})
                    $out[$property->name] = $this->{$property->name};
            } else {
                $out[$matches[1]] = $this->{$property->name};
            }
        }
        return $out;
    }
    
}