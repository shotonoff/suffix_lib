<?php

/**
 * Description of Translate
 *
 * @author shotonoff
 */

namespace Zp;

class Translate {

    private static $local = "ru-RU";
    static protected $instance = null;
    public $translates = array();

    private function __construct() {
        $this->translates =
                include DocRoot . DIRECTORY_SEPARATOR . 'View' .
                DIRECTORY_SEPARATOR . '_lang' . DIRECTORY_SEPARATOR .
                self::$local . '.php';
    }

    static public function getInstance() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    static public function SetLocal($local) {
        self::$local = $local;
    }

    static public function Translate($prase) {
        return (isset(self::getInstance()->translates[$prase]))?self::getInstance()->translates[$prase]:"";
    }

}