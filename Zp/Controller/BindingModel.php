<?php

/**
 * Description of BindingModel
 *
 * @author shotonoff
 */

namespace Zp\Controller;

use Zp\Http\IRequest;

class BindingModel {

    /**
     *
     * @param \Zp\Http\IRequest $request
     * @param \ReflectionMethod $reflectionMethod
     * @return Object 
     */
    public function Binding(IRequest $request, $reflectionMethod) {
        $bindReturn = array();
        $parameters = $reflectionMethod->getParameters();
        do {
            $param = array_shift($parameters);
            if (null == $param)
                break;
            $classBinding = $param->getClass();
            if ($classBinding == null) {
                $bindModel = $request->Get($param->name);
            } else {
                $bindModel = $classBinding->newInstance();
                foreach ($classBinding->getProperties() as $property) {
                    if ($property->isProtected()) {
                        $methodName = 'Set' . ucfirst($property->name);
                        if (method_exists($bindModel, $methodName)) {
                            $prop = $request->Get($property->name);
                            if (!is_null($prop)) {
                                call_user_func(array($bindModel, $methodName), $prop);
                            }
                        }
                    }
                }
            }
            $bindReturn[$param->name] = $bindModel;
        } while (!empty($parameters));
        return $bindReturn;
    }

}