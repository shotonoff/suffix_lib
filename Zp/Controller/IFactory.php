<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Controller;

interface IFactory {

    static public function Create($nameController, \Zp\Http\IRequest $request = null, \Zp\Http\IResponse $response = null);
    
}
    