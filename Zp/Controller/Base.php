<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\Controller;

abstract class Base {

    /**
     * @var \Zp\Http\IRequest 
     */
    protected $request;

    /**
     * @var \Zp\Http\IResponse 
     */
    protected $response;

    /**
     * @var \Zp\IView
     */
    protected $view;

    /**
     * @var type 
     */
    protected $actionTemplate = null;

    /**
     * @var type 
     */
    protected $themeTemplate = null;

    public function __construct(\Zp\Http\IRequest $request, \Zp\Http\IResponse $response) {
        $this->SetRequest($request);
        $this->SetResponse($response);
    }

    public function SetRequest(\Zp\Http\IRequest $request) {
        $this->request = $request;
    }

    public function SetResponse(\Zp\Http\IResponse $response) {
        $this->response = $response;
    }

    /**
     * @param \Zp\IView $view 
     */
    public function SetView(\Zp\IView $view) {
        $this->view = $view;
    }

    /**
     * @return \Zp\IView $response 
     */
    public function GetView() {
        return $this->view;
    }

    /**
     * @return \Zp\Http\IResponse
     */
    public function GetResponse() {
        return $this->response;
    }

    /**
     * @return \Zp\Http\IRequest
     */
    public function GetRequest() {
        return $this->request;
    }

    public function CallAction() {
        $controller = $this->GetRequest()->GetController();
        $action = $this->GetRequest()->GetAction();

        $actionName = $action . "Action";
        $refMethod = new \ReflectionMethod(get_class($this), $actionName);

        $binding = new BindingModel();
        $bindReturn = $binding->Binding($this->request, new \ReflectionMethod(get_class($this), $actionName));

        call_user_func_array(array($this, $actionName), $bindReturn);
        if (null !== $this->actionTemplate)
            $content = $this->GetView()->Render($this->actionTemplate, $controller);
        else
            $content = $this->GetView()->Render($action, $controller);

        $this->GetResponse()->AddBuffer($content);
        $this->GetResponse()->Send();
    }

    protected function redirect($url) {
        $this->GetResponse()->AddHeader("Location: " . $url);
        $this->GetView()->SwitchRenderViewTheme(false);
        $this->GetView()->SwitchRenderViewController(false);
    }

    public function onBinding() {
        
    }

    public function init(){}

}