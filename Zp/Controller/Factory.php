<?php

/**
 * Description of Factory
 *
 * @author shotonoff
 */
namespace Zp\Controller;

class Factory implements IFactory
{
    /**
     * @var \Closure
     */
    protected $CallbackAfterCreateController;

    static private $instance = null;

    private function __construct()
    {
    }

    /**
     * @return Zp\Controller\Base
     */
    static public function Create($nameController, \Zp\Http\IRequest $request = null, \Zp\Http\IResponse $response = null)
    {
        $nameController = "\\Controller\\" . $nameController;
        /** @var $controller \Zp\Controller\Base */
        $controller = new $nameController($request, $response);
        ($request === null) || $controller->SetRequest($request);
        ($response === null) || $controller->SetResponse($response);
        $controller->SetView(new \Zp\View());
        $controller->init();
        self::getInstanse()->InvokeAfterCallback($controller);
        return $controller;
    }

    public function InvokeAfterCallback(\Zp\Controller\Base $controller)
    {
        $func = $this->CallbackAfterCreateController;
        return $func($controller);
    }

    /**
     * @static
     * @param \Closure $function
     */
    static public function SetAfterCallback(\Closure $function)
    {
        self::getInstanse()->CallbackAfterCreateController = $function;
    }

    public static function getInstanse()
    {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * @static
     * @param array $options
     */
    static public function Setup(array $options)
    {
        if (isset($options['afterCallback']) && $options['afterCallback'] instanceof \Closure) {
            self::getInstanse()->SetAfterCallback($options['afterCallback']);
        }

    }


}