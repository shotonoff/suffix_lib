<?php

/**
 * Description of Dispatcher
 *
 * @author shotonoff
 */

namespace Zp;

use Zp\Http\IRequest,
Zp\Http\IResponse,
Zp\Route\RouteMap;

class Dispatcher implements IDispatcher
{

    /**
     * @var \Zp\Http\IRequest
     */
    protected $request;

    /**
     * @var \Zp\Http\IResponse
     */
    protected $response;

    /**
     * @var \Zp\Route\RouteMap
     */
    protected $RouteMap;

    public function __construct(IRequest $request, IResponse $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->RouteMap = new RouteMap();
    }

    protected function ChooseRoute(IRequest $request)
    {
        $flagRouteChoose = false;
        $url = new \Zp\Http\Url();

        foreach ($this->RouteMap() as $route) {
            if ($route->Check($url)) {
                $flagRouteChoose = true;
                break;
            }
        }
        if ($flagRouteChoose)
            $request->SetRoute($route);
        else {
            throw new DispatcherException("Route Not Found", DispatcherException::RouteNotFound);
        }
    }

    public function Dispatch(IRequest $request = null, IResponse $response = null)
    {
        $request = ($request === null) ? $this->request : $request;
        $response = ($response === null) ? $this->response : $response;

        try {
            $this->ChooseRoute($request);
            $this->dispatcher($request->GetController(), $request, $response);
        } catch (\Zp\Exception $exc) {
            $errorRequest = new \Zp\Http\Request();
            $errorRoute = $this->RouteMap()->GetByName("error");
            $errorRequest->SetRoute($errorRoute);
            $errorRequest->Set("error", $exc);
            $this->dispatcher("Error", $errorRequest, $response);
        }
    }

    private function dispatcher($nameController, $request, $response)
    {
        $controller = \Zp\Controller\Factory::Create($nameController, $request, $response);
        $controller->CallAction();
    }

    public function RouteMap()
    {
        return $this->RouteMap;
    }

}

class DispatcherException extends \Zp\Exception
{
    const RouteNotFound = 1;
}