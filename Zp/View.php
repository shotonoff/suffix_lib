<?php

/**
 * Description of View
 *
 * @author shotonoff
 */

namespace Zp;

use Zp\View\Base,
    \Zp\View\Render;

class View extends Base implements IView {

    protected static $ViewDirectories;
    protected static $Extension = "php";

    /**
     * @var boolean
     */
    protected $IsRenderViewTheme = true;

    /**
     * @var boolean
     */
    protected $IsRenderViewController = true;
    protected $ThemeName = "Default";

    /**
     * @param boolean $flag 
     */
    public function SwitchRenderViewTheme($flag) {
        if (!is_bool($flag))
            throw new \InvalidArgumentException();
        $this->IsRenderViewTheme = $flag;
    }

    /**
     * @param boolean $flag 
     */
    public function SwitchRenderViewController($flag) {
        if (!is_bool($flag))
            throw new \Exception();
        $this->IsRenderViewController = $flag;
    }

    public static function SetViewDirectories($pathView) {
        if (!file_exists($pathView))
            throw new \Exception("File not found");

        static::$ViewDirectories = $pathView;
    }

    public static function GetViewDirectories() {
        return static::$ViewDirectories;
    }

    public static function SetExtension($extension) {
        static::$Extension = $extension;
    }

    public function SetThemeName($ThemeName) {
        $this->ThemeName = $ThemeName;
    }

    public function GetThemeName() {
        return $this->ThemeName . "." . static::$Extension;
    }

    public function Render($action, $controller = null) {
        $renderView = "";

        if ($this->IsRenderViewController) {
            $pathView = static::$ViewDirectories . DIRECTORY_SEPARATOR . $controller .
                    DIRECTORY_SEPARATOR . $action . "." . static::$Extension;

            $renderView = $this->RenderFile($pathView, $this->GetVars());
        }
        if ($this->IsRenderViewTheme) {
            $pathTheme = static::$ViewDirectories . DIRECTORY_SEPARATOR . "_Theme" .
                    DIRECTORY_SEPARATOR . $this->GetThemeName();

            $vars = array('content' => $renderView, 'vars' => $this->GetVars());
            $renderView = \Zp\View\Render::IncludeTemplate($pathTheme, $vars);
        }

        return $renderView;
    }

    public function Partial($viewPath, $vars = null) {
        return Render::IncludeTemplate(self::GetViewDirectories() . DIRECTORY_SEPARATOR . $viewPath, $vars, $this->GetHtmlHelper());
    }

    public function RenderFile($pathView, $vars = array()) {
        return Render::IncludeTemplate($pathView, $vars, $this->GetHtmlHelper());
    }

}