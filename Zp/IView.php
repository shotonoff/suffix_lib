<?php

/**
 *
 * @author shotonoff
 */

namespace Zp;

interface IView {
        
    public function Render($action, $controller = null);
    
    public function RenderFile($file);
    
}