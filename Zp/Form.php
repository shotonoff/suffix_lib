<?php

/**
 * Description of Form
 *
 * @author shotonoff
 */

namespace Zp;

use Zp\Form\Base,
Zp\Form\Element\Hidden;

class Form extends Base
{

    public function mergeValues($mergeValue)
    {
        $allElements = $this->getAllElements();

        foreach ($mergeValue as $name => $value) {
            foreach ($allElements as $el) {
                if ($el->getName() == $name) {
                    if (null != $el) {
                        if ($el instanceof \Zp\Form\Element\Checkbox) {
                            $el->setChecked(($value === 'on' || $value == 1) ? true : false);
                            $el->setValue(($value === 'on' || $value == 1) ? 1 : 0);
                        } elseif ($el instanceof \Zp\Form\Element\DownList) {
                            $el->Selected($value);
                        } else {
                            $el->setValue($value);
                        }
                    }
                }
            }
        }
    }

    public function build()
    {
        $output = '';
        $tpl = $this->getTpl();

        if (null !== $tpl && $this->getView() != null) {
            $params = ($this->getView() != null) ? array('form' => $this, 'view' => $this->getView()) : array('form' => $this);
            $output = $this->getView()->RenderFile(\Zp\View::GetViewDirectories() . DIRECTORY_SEPARATOR . $tpl, $params);
        } else {
            $attrs = $this->attrToStr();

            $output = '<form name="' . $this->getName() . '" action="' . $this->getAction() . '" method="' . $this->getMethod() . '" ' . $attrs . '>';
            $output .= $this->_container->render();
            $output .= '</form>';
        }

        return $output;
    }

    public function __toString()
    {
        return $this->build();
    }

}