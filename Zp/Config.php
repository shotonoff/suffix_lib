<?php

/**
 * Description of Config
 *
 * @author shotonoff
 */

class Config
{

    public $config = array();
    static protected $instance = null;

    private function __construct()
    {

    }

    public function __staticCall()
    {

    }

    public function Load(array $options, $env)
    {
        self::getInstance()->config = array_merge_recursive($options['default'], $options[$env]);
    }

    static public function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    static public function Set($name, $value)
    {
        self::getInstance()->config[$name] = $value;
    }

    static public function Get($name)
    {
        return (isset(self::getInstance()->config[$name])) ? self::getInstance()->config[$name] : null;
    }

}