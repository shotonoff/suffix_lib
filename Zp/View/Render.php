<?php

/**
 * Description of Render
 *
 * @author shotonoff
 */

namespace Zp\View;

class Render {

    public static function IncludeTemplate($pathView, $view, $html = null) {
        if (!file_exists($pathView))
            throw new \Exception("File not found. " . $pathView);
        ob_start();
        include $pathView;
        $output = ob_get_clean();
        return $output;
    }

}