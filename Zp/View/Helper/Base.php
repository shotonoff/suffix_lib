<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\View\Helper;

class Base {

    protected $options = array();
    
    public function __construct() {
        $this->SetOptions(func_get_args());
    }
    
    public function SetOptions(array $options) {
        $this->options = $options;
    }

    public function __toString() {
        return $this->Render();
    }

}