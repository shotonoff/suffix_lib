<?php

/**
 * Description of DownListBox
 *
 * @author shotonoff
 */

namespace Zp\View\Helper;

use Zp\View,
    Zp\IView,
    Zp\View\IHelper,
    Zp\Form\Element\DownList;

class DownListBox extends Base implements IHelper {

    private $items = array();
    private $name;
    
    public function __construct($name, $items, array $options = array()) {
        $this->name = $name;
        $this->items = $items;
        $this->options = $options;
    }

    public function Render(IView $view = null) {
        $options = array_merge(array('items' => $this->items), $this->options);
        $DownList = new DownList($this->name, $options);
        return $DownList;
    }

}