<?php

/**
 * Description of Html
 *
 * @author shotonoff
 */

namespace Zp\View\Helper;

use \Zp\View\IHelper;

class Html
{

    protected $namespaces = array(
        __NAMESPACE__
    );
    protected $helpers = array();

    public function __call($viewClassName, $args)
    {
        if ($this->HasHelper($viewClassName)) {
            $helper = $this->GetHelper($viewClassName);
            $helper->SetOptions($args);
            return $helper->Render(null);
        }

        foreach ($this->namespaces as $namespace) {
            $className = $namespace . '\\' . $viewClassName;

            if ($this->HasHelper($className)) {
                $helper = $this->GetHelper($className);
                $helper->SetOptions($args);
                return $helper->Render(null);
            }

            if (class_exists($className)) {
                $reflection_class = new \ReflectionClass($className);
                $viewHelper = $reflection_class->newInstanceArgs($args);
                $this->AddHelper($className, $viewHelper);
                return $viewHelper->Render(null);
            }
        }
    }

    public function __construct()
    {

    }

    public function AddNamespace($namespace)
    {
        $this->namespaces[] = $namespace;
    }

    public function AddHelper($name, IHelper $viewHelper)
    {
        $this->helpers[$name] = $viewHelper;
    }

    public function GetHelper($name)
    {
        return $this->helpers[$name];
    }

    public function HasHelper($name)
    {
        return (isset($this->helpers[$name]) && $this->helpers[$name] instanceof IHelper);
    }

}