<?php

/**
 * Description of TextBox
 *
 * @author shotonoff
 */

namespace Zp\View\Helper;

use Zp\View\IHelper,
    Zp\View\Helper\Base,
    Zp\Form\Element\Text;

class TextBox extends Base implements IHelper {
//
//    private $name;
//
//    public function __construct($name) {
//        $this->name = $name;
//    }
    
    public function Render(\Zp\IView $view = null) {
        $name = $this->options[0];
//        $htmlElement = new Text($this->name);
        $htmlElement = new Text($name);
        return $htmlElement->render();
    }
    
}