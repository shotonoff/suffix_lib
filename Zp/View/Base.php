<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\View;

use Zp\View\Helper\Html;

abstract class Base {

    protected $variables = array();

    /**
     * @var \Zp\View\Helper\Html
     */
    protected $HtmlHelper;

    public function __construct() {
        $this->HtmlHelper = new Html();
    }

    public function __set($name, $value) {
        $this->AppendVar($name, $value);
    }

    public function __get($name) {
        return $this->GetVar($name);
    }

    public function AppendVar($name, $value) {
        $this->variables[$name] = $value;
    }

    public function GetVar($name) {
        return $this->variables[$name];
    }

    public function GetVars() {
        return $this->variables;
    }

    /**
     * @return \Zp\View\Helper\Html 
     */
    public function GetHtmlHelper() {
        return $this->HtmlHelper;
    }

}