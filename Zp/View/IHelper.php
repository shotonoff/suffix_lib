<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\View;

interface IHelper {
    
    public function Render(\Zp\IView $view = null);
    
}