<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Interface
 *
 * @author shotonoff
 */

namespace Zp;

interface IValidate
{
    /**
     * @abstract
     * @param $value
     * @desc метод который будет вызван аггрегатором в ходе проверки значения, проверяемое значение будет передано в $value
     * @return bool
     */
    public function IsValid($value);

    /**
     * @abstract
     * @desc Метод возвращает строку с фразой ошибки
     * @return string
     */
    public function GetMessage();

    /**
     * @abstract
     * @param $message string
     * @desc Метод задаёт сообщение с ошибкой
     * @return void
     */
    public function SetMessage($message);
}