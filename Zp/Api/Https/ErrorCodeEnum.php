<?php

namespace Zp\Api\Https;

class ErrorCodeEnum {
//    Успешное выполнение запроса/Операция в процессе исполнения
    const PROCESS = 1;
//    Операция успешно завершена
    const SUCCESS = 2;
//    Операция завершена с ошибками
    const ERROR = 3;
    
    const OPERATION_WITH_PAYID_NOT_FOUND = 575;   
    
    const OPERATION_NOT_FOUND = 600;
    
    /**
     * @desc Код перевода TYPE_TRANSFER не соответствует настройкам интерфейса! 
     */
    const BAD_TYPE_TRANSFER = 200;
    /**
     * @desc Счет для оплаты не найден/Кошелек клиента заблокирован или не существует 
     */
    const INVOICE_NOT_FOUND = 195;
    
    /**
     * @todo описать все варианты ошибок
     */
}