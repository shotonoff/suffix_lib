<?php
namespace Zp\Api\Https;

use Zp\Api\Https\Exception;

class Protocol implements IProtocol
{

    const SERVICE_URL = "https://z-payment.com/api/";

    /**
     * @desc MATCH - |ZP\d{10}|
     * @var string
     */
    private $zpAccount;

    /**
     *
     * @var Int
     */
    private $idInterface;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $lastResponse;

    /**
     * @var array
     */
    private $errors = array();

    public function __construct($zpAccount, $idInterface, $password = "")
    {
        $this->SetZpAccount($zpAccount);
        $this->SetIdInterfece($idInterface);
        $this->SetPassword($password);
    }

    public function SetZpAccount($account)
    {
        if (!preg_match("/ZP\d{8}/", $account))
            throw new \Zp\Api\Https\Exception("Не верно указан ZP Account", \Zp\Api\Https\Exception::ERROR_PARAM);
        $this->zpAccount = $account;
    }

    public function SetIdInterfece($idInterface)
    {
        if ((int)$idInterface == 0)
            throw new \Zp\Api\Https\Exception("ID Interface должно быть целочисленным", \Zp\Api\Https\Exception::ERROR_PARAM);
        $this->idInterface = $idInterface;
    }

    public function SetPassword($password)
    {
        $this->password = $password;
    }

    public function GetZpAccount()
    {
        return $this->zpAccount;
    }

    public function GetIdInterfece()
    {
        return $this->idInterface;
    }

    public function GetPassword()
    {
        return $this->password;
    }

    private function toArray($get_str)
    {
        $arKeyValues = explode("&", $get_str);
        $output = array();
        foreach ($arKeyValues as $arKeyValue) {
            list($key, $value) = explode("=", $arKeyValue);
            $output[$key] = $value;
        }
        return $output;
    }

    /**
     * @desc
     * FORMAT RETURN:
     *  STATUS - INT {STATUS=1 – Операция успешно}
     *  MESSAGE - VARCHAR {Ответ сервера}
     *  BALANCE - UNSIGNED FLOAT (8,2) {Сумма на балансе кошелька}
     *  CURRENCY - CONST=RUR {Код валюты кошелька, всегда RUR – рубли РФ}
     *
     * @return array
     * @throws \Zp\Api\Https\Exception
     */
    public function Zp1()
    {
        $service_url = self::SERVICE_URL . "get_balance.php";
        $request = array(
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount()
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP1 Получение баланса по кошельку, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    /**
     *
     * @param type $service_url
     * @param type $ID_PAY
     * @param type $DST_ACCOUNT
     * @param array $SUMMA
     * @param type $COMMENT
     * @return type
     * @throws \Zp\Api\Https\Exception
     */
    private function zp2Step1($service_url, $ID_PAY, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT)
    {
        if ($SUMMA_IN != null && $SUMMA_OUT == null) {
            $key = 'SUMMA_IN';
            $val = $SUMMA_IN;
        } elseif ($SUMMA_IN == null && $SUMMA_OUT != null) {
            $key = 'SUMMA_OUT';
            $val = $SUMMA_OUT;
        }

        $request = array(
            'ID_PAY' => $ID_PAY,
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'DST_ACCOUNT' => $DST_ACCOUNT,
            $key => $val,
            'COMMENT' => $COMMENT,
            'TYPE_TRANSFER' => self::TYPE_TRANSFER
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP2 Запрос перевода и перевод средств с кошелька, не валидный. Код: %s. Сообщение: %s.", $response['STATUS'], $response['MESSAGE']);
//            $message = sprintf("Ответ на запрос - HTTPS/ZP2 Запрос перевода и перевод средств с кошелька, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    private function zp2Step2($service_url, $ID_PAY, $ID_OPERATION, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT)
    {
        $request = array(
            'ID_PAY' => $ID_PAY,
            'ID_OPERATION' => $ID_OPERATION,
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'DST_ACCOUNT' => $DST_ACCOUNT,
            'SUMMA_IN' => $SUMMA_IN,
            'SUMMA_OUT' => $SUMMA_OUT,
            'COMMENT' => $COMMENT,
            'TYPE_TRANSFER' => self::TYPE_TRANSFER
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP2 Запрос перевода и перевод средств с кошелька, не валидный. Код: %s. Сообщение: %s.", $response['STATUS'], $response['MESSAGE']);
//            $message = sprintf("Ответ на запрос - HTTPS/ZP2 Запрос перевода и перевод средств с кошелька, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    public function Zp2($ID_PAY, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT = "", $ID_OPERATION = null, $step = 0)
    {
        if (null == $ID_PAY)
            throw new Exception("Параметр ID_PAY пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "create_pay.php";
        /*
          if (null != $SUMMA_IN && null == $SUMMA_OUT && (is_float($SUMMA_IN) || is_numeric($SUMMA_IN)))
          $SUMMA = $SUMMA_IN;
          elseif (null == $SUMMA_IN && null != $SUMMA_OUT && (is_float($SUMMA_OUT) || is_numeric($SUMMA_OUT)))
          $SUMMA = $SUMMA_OUT;
         */
        /**
         * Шаг 1 - Запрос на перевод
         * Шаг 2 - Выполнение перевода
         */
        switch ($step) {
            case 0:
                if (null == $DST_ACCOUNT)
                    throw new Exception("Параметр DST_ACCOUNT пустой", Exception::ERROR_PARAM);
                if (null == $SUMMA_IN && null == $SUMMA_OUT)
                    throw new Exception("Параметр SUMMA_IN и SUMMA_OUT пустые", Exception::ERROR_PARAM);

                $response = $this->zp2Step1($service_url, $ID_PAY, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT);
                $response = $this->zp2Step2($service_url, $ID_PAY, $response['ID_OPERATION'], $DST_ACCOUNT, $response['SUMMA_IN'], $response['SUMMA_OUT'], $COMMENT);
                break;
            case 1:
                /**
                 * @todo Проверить суммы на валидность
                 */
                if (null == $DST_ACCOUNT)
                    throw new Exception("Параметр DST_ACCOUNT пустой", Exception::ERROR_PARAM);
                if (null == $SUMMA_IN && null == $SUMMA_OUT)
                    throw new Exception("Параметр SUMMA_IN и SUMMA_OUT пустые", Exception::ERROR_PARAM);
                $response = $this->zp2Step1($service_url, $ID_PAY, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT);
                break;
            case 2:
                /**
                 * @todo Проверить суммы на валидность
                 */
                if (null == $DST_ACCOUNT)
                    throw new Exception("Параметр DST_ACCOUNT пустой", Exception::ERROR_PARAM);
                if (null == $SUMMA_IN && null == $SUMMA_OUT)
                    throw new Exception("Параметр SUMMA_IN и SUMMA_OUT пустые", Exception::ERROR_PARAM);
                $response = $this->zp2Step2($service_url, $ID_PAY, $ID_OPERATION, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT);
                break;
            default:
                throw new Exception("У запроса ZP2 нет действия " . $step, Exception::ERROR_PARAM);
        }

        return $response;
    }

    public function Zp3($START_DATE, $END_DATE, $TYPE_TRANSFER = null, $SRC_ACCOUNT = null, $DST_ACCOUNT = null, $STATUS_PAY = null, $ID_SHOP = null, $ID_OPERATION = null, $ID_PAY = null)
    {
        if (null == $START_DATE)
            throw new Exception("Параметр START_DATE пустой", Exception::ERROR_PARAM);

        if (null == $END_DATE)
            throw new Exception("Параметр END_DATE пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "get_history.php";

//        В формировании хеша не участвуют переменные TYPE_TRANSFER, SRC_ACCOUNT, DST_ACCOUNT, SUCCESS_PAY, ID_SHOP, ID_OPERATION.

        $request = array(
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'START_DATE' => $START_DATE,
            'END_DATE' => $END_DATE,
        );

        $request['HASH'] = $this->sign($request);

        if (null !== $TYPE_TRANSFER) {
            /**
             * PAY_ZP
             */
            $request['TYPE_TRANSFER'] = $TYPE_TRANSFER;
        }
        if (null !== $SRC_ACCOUNT) {
            $request['SRC_ACCOUNT'] = $SRC_ACCOUNT;
        }
        if (null !== $DST_ACCOUNT) {
            $request['DST_ACCOUNT'] = $DST_ACCOUNT;
        }
        if (null !== $STATUS_PAY) {
            $request['STATUS_PAY'] = $STATUS_PAY;
        }
        if (null !== $ID_SHOP) {
            $request['ID_SHOP'] = $ID_SHOP;
        }
        if (null !== $ID_OPERATION) {
            $request['ID_OPERATION'] = $ID_OPERATION;
        }
        if (null !== $ID_PAY) {
            $request['ID_PAY'] = $ID_PAY;
        }

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP3 Получение истории операций по кошельку, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }

        return $response;
    }

    public function Zp4($HEAD_MESSAGE, $TEXT_MESSAGE, $DST_ACCOUNT)
    {
        if($DST_ACCOUNT === null)
            throw new Exception("Параметр DST_ACCOUNT пустой", Exception::ERROR_PARAM);

        if ($TEXT_MESSAGE === null)
            throw new Exception("Параметр TEXT_MESSAGE пустой", Exception::ERROR_PARAM);

        if($HEAD_MESSAGE === null)
            throw new Exception("Параметр HEAD_MESSAGE пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "sent_message.php";

        if ('UTF-8' === mb_detect_encoding($HEAD_MESSAGE, "UTF-8", true))
            $HEAD_MESSAGE = iconv("UTF-8", "windows-1251//IGNORE", $HEAD_MESSAGE);

        if ('UTF-8' === mb_detect_encoding($TEXT_MESSAGE, "UTF-8", true))
            $TEXT_MESSAGE= iconv("UTF-8", "windows-1251//IGNORE", $TEXT_MESSAGE);

        $request = array(
            "ID_INTERFACE" => $this->GetIdInterfece(),
            "ZP_ACCOUNT" => $this->GetZpAccount(),
            "DST_ACCOUNT" => $DST_ACCOUNT,
            "HEAD_MESSAGE" => $HEAD_MESSAGE,
            "TEXT_MESSAGE" => $TEXT_MESSAGE
        );

        $response = $this->doRequest($service_url, $request);
        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP4 Отправка сообщение пользователю, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']
                . "REQUEST: \n" . print_r($request, true) );
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    /**
     * @desc
     * FORMAT RETURN:
     *  ID_INTERFACE UNSIGNED INT Уникальный идентификатор интерфейса.
     *  ZP_ACCOUNT CHAR(10) ZP кошелек владельца интерфейса, баланс которого запрашивается
     *  DST_ACCOUNT CHAR(10) ZP кошелек информацию о котором нужно получить
     *
     * @param string $DST_ACCOUNT
     * @return array
     * @throws Exception
     */
    public function Zp5($DST_ACCOUNT)
    {
        if (null == $DST_ACCOUNT)
            throw new Exception("Параметр DST_ACCOUNT пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "get_info.php";
        $request = array(
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'DST_ACCOUNT' => $DST_ACCOUNT
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP5 Получение информации по кошельку пользователя, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    /**
     * @example REQUEST
     *  ID_PAY UNSIGNED INT (10) Внутренний учетный номер операции агента. Должен проверяться на уникальность на стороне агента и служит для идентификации операции на сервере агента. Номер счета должен быть уникален в пределах 7 дней.
     *  ID_INTERFACE UNSIGNED INT Уникальный идентификатор интерфейса.
     *  ZP_ACCOUNT CHAR(10) ZP кошелек владельца интерфейса. С этого кошелька выполняется перевод средств.
     *  NUM_INVOICE UNSIGNED INT Номер счета предварительно выписанного на сайте продавца
     *  SUMMA_INVOICE UNSIGNED FLOAT (8,2) Сумма счета на оплату. (полученная по запросу HTTPS/ZP6.1) SUMMA_IN=110.5 НЕ верно SUMMA_IN=110.50 верно
     *  SUMMA_PAY UNSIGNED FLOAT (8,2) Полная сумма оплаты клиентом, т.е. сумма внесенная Агенту для оплаты счета. (оплаченная клиентом) SUMMA_PAY=200 НЕ верно SUMMA_PAY=200.00 верно
     *  TYPE_TRANSFER VARCHAR(32) Код перевода. Назначается Агенту от системы
     *
     * @return array
     *  STATUS INT Код выполнения операции: STATUS=1 – Счет успешно найден, все остальные коды ошибка.
     *  MESSAGE VARCHAR Ответ сервера
     *  ID_PAY UNSIGNED INT (10) Внутренний учетный номер операции Агента
     *  NUM_INVOICE UNSIGNED INT Номер счета предварительно выписанного на сайте продавца
     *  SUMMA_INVOICE UNSIGNED FLOAT (8,2) Сумма счета на оплату в рублях РФ
     */
    public function Zp6($ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER, $SUMMA_PAY = null, $SUMMA_INVOICE = null, $step = 0)
    {
        if (null == $ID_PAY)
            throw new Exception("Параметр ID_PAY пустой", Exception::ERROR_PARAM);
        if (null == $NUM_INVOICE)
            throw new Exception("Параметр NUM_INVOICE пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "pay_invoice.php";

        switch ($step) {
            case 0:
                $response = $this->zp6Step1($service_url, $ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER);
                switch ($response["STATUS"]) {

                    case \Zp\Api\Https\ErrorCodeEnum::PROCESS:
                        /**
                         * @desc Производим операцию оплаты счёта
                         */
                        $response = $this->zp6Step2($service_url, $ID_PAY, $NUM_INVOICE, $SUMMA_PAY, $response['SUMMA_INVOICE'], $TYPE_TRANSFER);
                        break;
                    case \Zp\Api\Https\ErrorCodeEnum::SUCCESS:
                        break;
                    case \Zp\Api\Https\ErrorCodeEnum::INVOICE_NOT_FOUND:
                        throw new Exception("Счёт № {$NUM_INVOICE} не найден", \Zp\Api\Https\ErrorCodeEnum::INVOICE_NOT_FOUND);
                    default:
                        throw new Exception("Возникла не описанная ошибка, при подготовки платы счёта № {$NUM_INVOICE}");
                }
                break;
            case 1:
                $response = $this->zp6Step1($service_url, $ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER);
                break;
            case 2:
                if (null == $SUMMA_INVOICE)
                    throw new Exception("Параметр SUMMA_INVOICE не должны быть null", Exception::ERROR_PARAM);
                if (null == $SUMMA_PAY)
                    throw new Exception("Параметр SUMMA_PAY не должны быть null", Exception::ERROR_PARAM);
                /**
                 * Проверяем текущий статус
                 */
                $response = $this->zp6Step1($service_url, $ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER);

                switch ($response["STATUS"]) {
                    case \Zp\Api\Https\ErrorCodeEnum::PROCESS:
                        $response = $this->zp6Step2($service_url, $ID_PAY, $NUM_INVOICE, $SUMMA_PAY, $response['SUMMA_INVOICE'], $TYPE_TRANSFER);
                        break;
                    case \Zp\Api\Https\ErrorCodeEnum::SUCCESS:
                        break;
                    case \Zp\Api\Https\ErrorCodeEnum::INVOICE_NOT_FOUND:
                        throw new Exception("Счёт № {$NUM_INVOICE} не найден", \Zp\Api\Https\ErrorCodeEnum::INVOICE_NOT_FOUND);
                    default:
                        throw new Exception("Возникла не описанная ошибка, при подготовки платы счёта № {$NUM_INVOICE}");
                }

                break;
            default:
                throw new Exception("У запроса ZP6 нет действия " . $step, Exception::ERROR_REQUEST_ACTION);
        }
        return $response;
    }

    private function zp6Step1($service_url, $ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER)
    {
        $request = array(
            'ID_PAY' => $ID_PAY,
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'NUM_INVOICE' => $NUM_INVOICE,
            'TYPE_TRANSFER' => $TYPE_TRANSFER
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP6 Запрос счета и оплата счета продавца, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    private function zp6Step2($service_url, $ID_PAY, $NUM_INVOICE, $SUMMA_PAY, $SUMMA_INVOICE, $TYPE_TRANSFER)
    {
        if ($SUMMA_PAY < $SUMMA_INVOICE) {
            /**
             * @desc Сумма платежа счёта меньше выставленной суммы по счёту
             * Переводим её на кошелёк агента и прекращаем выполнение оплаты
             */
            $this->TransferMoneyOnAgentInvoice($NUM_INVOICE, $SUMMA_PAY);
            $message = "Номер платежа " . $ID_PAY . ". По номеру счёта " . $NUM_INVOICE . " Сумма платежа{" . $SUMMA_PAY . "} меньше, суммы счёта{" . $SUMMA_INVOICE . "}";
            $code = \Zp\Api\Https\PayException::PAY_LESS;

            /** выкидываем исключение throw \Zp\Api\Https\PayException */
            throw new \Zp\Api\Https\PayException($message, $code);
        } elseif ($SUMMA_PAY > $SUMMA_INVOICE) {
            /**
             * @desc Сумма платежа больше суммы выставленного счёта
             * В этом случае вычисляем сдачу и переводим её на кошелёк агента
             */
            $TAIL = $SUMMA_PAY - $SUMMA_INVOICE;
            $SUMMA_PAY = $SUMMA_INVOICE;
            $this->TransferMoneyOnAgentInvoice($NUM_INVOICE, $TAIL);
        }

        $request = array(
            'ID_PAY' => $ID_PAY,
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'NUM_INVOICE' => $NUM_INVOICE,
            'SUMMA_INVOICE' => $SUMMA_INVOICE,
            'SUMMA_PAY' => $SUMMA_PAY,
            'TYPE_TRANSFER' => $TYPE_TRANSFER
        );

        $response = $this->doRequest($service_url, $request);

        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP6 Запрос счета и оплата счета продавца, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    public function TransferMoneyOnAgentInvoice($INVOICE, $AMOUNT)
    {
        /**
         * @todo Переводим денежные седства на кошелёк пользователя.
         * @desc По номеру счёта проверяем наличие пользователя в системе
         * если пользователь еще не зарегистрирован, то создаём ему аккаунт
         * переводим средства ему на кошелёк
         */

        throw new \Exception();
//        throw new NotImplementsException();
    }

    /**
     * @desc
     *  ID_SHOP UNSIGNED INT Уникальный идентификатор магазина в системе
     *  ID_OPER UNSIGNED INT Уникальный номер счета Z-Payment, который передается в переменной LMI_SYS_TRANS_NO интерфейса Merchant
     *
     * @return array
     *  STATUS INT Код выполнения операции: STATUS=1 – Операция успешно выполнена, все остальные коды ошибка.
     *  MESSAGE VARCHAR Ответ сервера.
     *  STATUS UNSIGNED INT Код статуса платежа: 0 => Создана, 1 => Ожидает оплаты, 2 => Ожидает обработки, 3 => Обрабатывается, 4 => Завершена, 5 => Отменена, 6 => Заморожена, 99 => Ошибка!
     *  SUMMA_PAY UNSIGNED FLOAT (8,2) Сумма платежа в ZP
     *  EMAIL VARCHAR Электронная почта покупателя
     *
     * Примечание: Оплата считается успешно принятой, только если код статуса платежа STATUS = 4 или STATUS = 6.
     */
    public function Zp7($ID_SHOP, $ID_OPER, $MERCHANT_KEY)
    {
        if (null == $ID_SHOP)
            throw new Exception("Параметр ID_SHOP пустой", Exception::ERROR_PARAM);

        if (null == $ID_OPER)
            throw new Exception("Параметр ID_OPER пустой", Exception::ERROR_PARAM);

        $service_url = self::SERVICE_URL . "get_status_pay.php";
        $request = array(
            'ID_SHOP' => $ID_SHOP,
            'ID_OPER' => $ID_OPER,
            'HASH' => md5($ID_SHOP . $ID_OPER . $MERCHANT_KEY)
        );

        $response = $this->doRequest($service_url, $request);
        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZP7 Запрос статуса платежа, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    /**
     * @param $NUM_MOBILE
     * @param $TEXT_MESSAGE
     * @return array
     * @throws Exception
     */
    public function ZpSMS($NUM_MOBILE, $TEXT_MESSAGE)
    {
        if (null == $NUM_MOBILE)
            throw new Exception("Параметр NUM_MOBILE пустой", Exception::ERROR_PARAM);

        if (!strlen($NUM_MOBILE) == 11 && !substr($NUM_MOBILE, 0, 1) && !is_numeric($NUM_MOBILE))
            throw new Exception("Не верно задан NUM_MOBILE параметр", Exception::ERROR_PARAM);

        if (null == $TEXT_MESSAGE)
            throw new Exception("Параметр TEXT_MESSAGE пустой", Exception::ERROR_PARAM);

        if (!strlen($TEXT_MESSAGE) == 70)
            throw new Exception("Не верно задан TEXT_MESSAGE параметр", Exception::ERROR_PARAM);

        if ('UTF-8' === mb_detect_encoding($TEXT_MESSAGE, "UTF-8", true))
            $TEXT_MESSAGE = iconv("UTF-8", "windows-1251//IGNORE", $TEXT_MESSAGE);

        $request = array(
            'ID_INTERFACE' => $this->GetIdInterfece(),
            'ZP_ACCOUNT' => $this->GetZpAccount(),
            'NUM_MOBILE' => $NUM_MOBILE,
            'TEXT_MESSAGE' => $TEXT_MESSAGE
        );

        $service_url = self::SERVICE_URL . "sent_sms.php";

        $request['HASH'] = $this->sign($request);
        $response = $this->doRequest($service_url, $request);
        if (!$this->isValid($response)) {
            $message = sprintf("Ответ на запрос - HTTPS/ZPSMS Запрос статуса платежа, не валидный. Код: %s. Сообщение: %s.", $this->errors['CODE'], $this->errors['MESSAGE']);
            throw new Exception($message, Exception::REQUEST_DONT_VALID);
        }
        return $response;
    }

    public function SetError($code, $message)
    {
        $this->errors = array(
            'CODE' => $code,
            'MESSAGE' => $message
        );
    }

    public function GetError()
    {
        return $this->errors;
    }

    /**
     *
     * @param array $data
     * @return boolean
     */
    private function isValid(array $data)
    {
        if (!isset($data['STATUS']))
            return false;
        switch ($data['STATUS']) {
            case \Zp\Api\Https\ErrorCodeEnum::PROCESS:
                /** @desc Запрос находиться в стадии выполнения */
            case \Zp\Api\Https\ErrorCodeEnum::SUCCESS:
                /** @desc Запрос успешно выполнен */
                return true;
                break;
            case 13:
                /**
                 * @todo Выпонить действие
                 * @desc Не задан ZP кошелек Клиента
                 */
            default:
                $this->SetError($data['STATUS'], $data['MESSAGE']);
                return false;
        }
    }

    /**
     * Производим вызов
     * @param string $service_url
     * @param array $request
     * @return array
     */
    private function doRequest($service_url, array $request)
    {
        if (!isset($request['HASH']))
            $request['HASH'] = $this->sign($request);

        $buildtRequest = http_build_query($request);
        $getResponse = $this->curlSendForm($service_url, $buildtRequest);
        $ArResponse = $this->toArray($getResponse);

        if (!$this->verify($ArResponse)) {
            throw new Exception("Не верная подпись", Exception::DONT_VERIFY_SIGN);
        }

        if (isset($ArResponse['MESSAGE']) && !empty($ArResponse['MESSAGE'])) {
            $ArResponse['MESSAGE'] = win_utf8($ArResponse['MESSAGE']);
        }
        /**
         * Запоминаем последний ответ
         */
        $this->lastResponse = $ArResponse;
        return $ArResponse;
    }

    public function GetResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @param array $data
     * @return boolean
     */
    private function verify(array $data)
    {
        if (!isset($data['HASH']))
            return true;

        $HASH = $data['HASH'];
        unset($data['HASH']);
        return $HASH == md5(implode("", $data) . $this->GetPassword());
    }

    /**
     *
     * @param array $data
     * @return type
     */
    private function sign($data)
    {
        $concatValue = (is_array($data)) ? implode("", $data) : $data;
        $hash = md5($concatValue . $this->GetPassword());
        return $hash;
    }

    /**
     * Alias Global function CurlSendForm
     * @param type $service_url
     * @param type $buildtRequest
     * @return type
     */
    private function curlSendForm($service_url, $buildtRequest)
    {
        return CurlSendForm($service_url, $buildtRequest);
    }

}