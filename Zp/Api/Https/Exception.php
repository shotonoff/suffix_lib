<?php

/**
 * Description of Exception
 *
 * @author shotonoff
 */

namespace Zp\Api\Https;

class Exception extends \Exception {
    /**
     * @desc Код ошибки входящих параметров
     */
    const ERROR_PARAM = 0;
    /**
     * @desc Неверная подпись
     */
    const DONT_VERIFY_SIGN = 1;
    /**
     * @desc Ответ на запрос баланса не валидный
     */
    const REQUEST_DONT_VALID = 2;
    /**
     * @desc Прочая ошибка
     */
    const ERROR_ANOTHER = 3;
    /**
     * @desc У запроса ZP6 нет действия
     */
    const ERROR_REQUEST_ACTION = 4;

}

