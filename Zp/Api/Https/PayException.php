<?php

/**
 * Description of PayException
 *
 * @author shotonoff
 */

namespace Zp\Api\Https;

class PayException extends \Exception {
    
    const PAY_LESS = 1;

}