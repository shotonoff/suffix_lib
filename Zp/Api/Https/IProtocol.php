<?php

/**
 *
 * @author shotonoff
 */

namespace Zp\Api\Https;

interface IProtocol {

    const ZP1 = 0;
    const ZP2 = 1;
    const ZP3 = 2;
    const ZP4 = 3;
    const ZP5 = 4;
    const ZP6 = 5;
    const ZP7 = 6;
    const TYPE_TRANSFER = "PAY_ZP";

    /**
     * HTTPS/ZP1 Получение баланса по кошельку
     */
    public function Zp1();

    /**
     * HTTPS/ZP2 Запрос перевода и перевод средств с кошелька
     */
    public function Zp2($ID_PAY, $DST_ACCOUNT, $SUMMA_IN, $SUMMA_OUT, $COMMENT = "", $ID_OPERATION = null, $step = 0);

    /**
     * HTTPS/ZP3 Получение истории операций по кошельку
     */
    public function Zp3($START_DATE, $END_DATE, $TYPE_TRANSFER = null, $SRC_ACCOUNT = null, $DST_ACCOUNT = null, $STATUS_PAY = null, $ID_SHOP = null, $ID_OPERATION = null, $ID_PAY = null);

    /**
     * HTTPS/ZP4 Отправка сообщения по внутренней почте системы
     */
    public function Zp4($HEAD_MASAGE, $TEXT_MESSAGE, $DST_ACCOUNT);

    /**
     * @desc HTTPS/ZP5 Получение информации по кошельку пользователя
     * @var string $DST_ACCOUNT
     */
    public function Zp5($DST_ACCOUNT);

    /**
     * HTTPS/ZP6 Запрос счета и оплата счета продавца
     */
    public function Zp6($ID_PAY, $NUM_INVOICE, $TYPE_TRANSFER, $SUMMA_PAY = null, $SUMMA_INVOICE = null, $step = 0);

    /**
     * HTTPS/ZP7 Запрос статуса платежа
     */
    public function Zp7($ID_SHOP, $ID_OPER, $MERCHANT_KEY);
}