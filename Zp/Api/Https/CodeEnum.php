<?php

/**
 * Description of Enum
 *
 * @author shotonoff
 */

namespace Zp\Api\Https;

class CodeEnum {
    /**
     * @desc Операция инициализирована
     */

    const OPERATION_INITILIZE = 0;
    /**
     * @desc В процессе оплаты
     */
    const OPERATION_PROCESS = 1;
    /**
     * @desc Ожидает процессинга
     */
    const WAIT_PROCESSING = 2;
    /**
     * @desc Режим процессинга
     */
    const MODE_PROCESSING = 3;
    /**
     * @desc Успешно завершена
     */
    const OPERATION_SUCCESS = 4;
    /**
     * @desc Операция отменена
     */
    const OPERATION_CANCEL = 5;
    /**
     * @desc Ошибка в процессе перевода
     */
    const ERROR_PROCESS = 99;

}