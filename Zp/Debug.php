<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shotonoff
 * Date: 8/24/12
 * Time: 9:51 AM
 * To change this template use File | Settings | File Templates.
 */

class Debug
{
    private static $dump = array();

    public static function Add($d)
    {
        static::$dump[] = $d;
    }

    public static function Get()
    {
        return static::$dump;
    }

    public static function Pre()
    {
        echo '<pre>';
        print_r(static::$dump);
        echo '</pre>';
    }

    public static function VarDump()
    {
        var_dump(static::$dump);
    }

    public static function IsEmpty()
    {
        return empty(static::$dump);
    }
}