<?php

/**
 * Description of Request
 *
 * @author shotonoff
 */

namespace Zp\Http;

class Request implements IRequest {

    /**
     * @var \Zp\Route\IRoute
     */
    protected $currentRoute;


    protected $params = array();
    protected $method;

    public function __construct() {
        $this->Initialize();
    }

    public function Initialize() {
        $this->SetMethod($_SERVER['REQUEST_METHOD']);
    }

    protected function SetMethod($method) {
        if (!in_array($method, array('PUT', 'HEAD', 'POST', 'GET')))
        /** @todo exception */
            throw new \Exception();

        $this->method = $method;
    }

    public function GetMethod() {
        return $this->method;
    }

    public function IsPost() {
        return ($this->method == 'POST');
    }

    public function IsGet() {
        return ($this->method == 'GET');
    }

    public function GetAction() {
        return $this->currentRoute->GetAction();
    }

    public function GetController() {
        return $this->currentRoute->GetController();
    }

    public function Get($param) {
        $return = null;
        $params = $this->currentRoute->GetParams();
        if (isset($params[$param])) {
            $return = $params[$param];
        } elseif(isset($this->params[$param])){
            $return = $this->params[$param];
        }
        elseif(isset($_REQUEST[$param])){
            $return = $_REQUEST[$param];
        }
        return $return;
    }

    public function Set($param, $value) {
        $this->params[$param] = $value;
    }

    public function SetRoute(\Zp\Route\IRoute $route) {
        $this->currentRoute = $route;
    }

    public static function IsAjax() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    public static function IsHttps() {
        return ($_REQUEST['SERVER_PORT'] == 443);
    }

}