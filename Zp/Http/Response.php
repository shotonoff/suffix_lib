<?php

/**
 * Description of Response
 *
 * @author shotonoff
 */

namespace Zp\Http;

class Response implements IResponse {

    protected $headers = array();
    
    protected $buffer = array();
    
    public function AddBuffer($content) {
        $this->buffer[] = $content;
    }
    
    public function AddHeader($header) {
        $this->headers[] = $header;
    }
    
    public function Send() {
        if(!empty($this->headers)) {
            foreach ($this->headers as $header) {
                header($header);
            }
        }
        
        $obContent = ob_get_clean();
        $buffer = implode("", $this->buffer);
        echo $buffer . $obContent;
    }
    
    
    
}