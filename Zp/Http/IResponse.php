<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Zp\Http;

interface IResponse {
    
    public function AddHeader($header);
    
    public function AddBuffer($content);
    
    public function Send();
    
}
