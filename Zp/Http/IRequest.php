<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Zp\Http;

use Zp\Route\IRoute;

interface IRequest {

    public function Initialize();

    /**
     * @param string
     * @return string1
     */
    public function Get($param);

    public function GetAction();

    public function GetController();

    public function SetRoute(IRoute $route);
}