<?php

/**
 * Description of Base
 *
 * @author shotonoff
 */

namespace Zp\Http;

class Url
{

    static protected $baseUrl;

    protected $Uri;

    static public function SetBaseUrl($baseUrl)
    {
        static::$baseUrl = $baseUrl;
    }

    static public function BaseUrl()
    {
        return static::$baseUrl;
    }

    public function __construct()
    {
        $REQUEST_URI = trim($_SERVER['REQUEST_URI'], "/");
        if(false != strpos($REQUEST_URI, '?'))
            $REQUEST_URI = substr($REQUEST_URI, 0, strpos($REQUEST_URI, '?'));
        if (static::BaseUrl() != "") {
            if (static::BaseUrl() == $REQUEST_URI)
                $REQUEST_URI = "";
            else
                $REQUEST_URI = substr($REQUEST_URI, strlen(static::BaseUrl()) + 1);
        }
        $this->Uri = $REQUEST_URI;
    }

    public function Uri()
    {
        return $this->Uri;
    }
}